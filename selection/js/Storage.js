class Storage{
    gameData;
    curAccNum;
    curCharNum;
    constructor(){
        //this.removeGameDataInStorage();
        this.setting();
        this.printGameData();
    }

    getGameData(){
        return this.gameData;
    }

    setting(){
        this.gameData = JSON.parse(localStorage.getItem("game"));
        this.curAccNum = this.gameData.currentData.account;
    }

    selectedCharacter(num){
        this.gameData.currentData.character = num;
        this.curCharNum = num;
        this.printGameData();
    }
    start(){
        this.saveGameData();
    }

    newCharacter(){
        if(this.isCharacterFull()){
            return false;
        }
        this.gameData.currentData.character = this.gameData.accounts[this.curAccNum].characters.length;
        let character = this.newCharInfoSetting();
        this.insertCharacter(character);
        this.saveGameData();
        return true;
    }
    
    deleteCharacter(){
        this.gameData.accounts[this.curAccNum].characters.splice(this.curCharNum, 1);
        this.saveGameData();
    }

    insertCharacter(char){
        this.gameData.accounts[this.curAccNum].characters.push(char);
    }

    isCharacterFull(){
        if(this.gameData.accounts[this.curAccNum].characters.length >= 3){
            return true;
        }
        return false;
    }

    saveGameData(){
        localStorage.setItem("game", JSON.stringify(this.gameData));
    }

    printGameData(){
        console.log(this.gameData);
    }


    newCharInfoSetting(){
        let newChar = {
            information : {
                job : false,
                specialize : 0,
                code: false,
                stat : {
                    LV : 1,
                    EXP : 0,
                    curHP : 500,
                    curMP : 300,
                    maxHP : 500,
                    maxMP : 300,
                    AD : 500,
                    DEF: 100,
                    CP: 0,
                    CD: 100,
                    AS : 10,
                    moveSpeed : 3,
                    statpoints : {
                        HP : {cur: 0, max: 5},
                        MP : {cur: 0, max: 5},
                        AD : {cur: 0, max: 5},
                        CP : {cur: 0, max: 5},
                        CD : {cur: 0, max: 5},
                        DEF: {cur: 0, max: 5},
                        AS : {cur: 0, max: 1},
                    },
                    haveStatpoints : 0,
                },
                position : {
                    mapCode: 100,
                    x : 800,
                    y : 500
                },
                keyBoard : {
                    skillCode : {
                        Q : -1,
                        W : -1,
                        E : -1,
                        R : -1,
                        A : -1,
                        S : -1,
                        D : -1,
                        F : -1,
                    },
                    itemCode : {
                        delete: -1,
                        end: -1,
                    },
                },
                mapData: {
                    mapAccess: {
                        200: false,
                        300: false,
                        400: false,
                        500: false,
                    }
                }
            },
            inventory : {
                have: [
                {code: 2001, amount: 10, type: "consumption"},
                {code: 2002, amount: 10, type: "consumption"},
                ],
                fit: [],
                gold: 0,
            },
            quest : {
                inprocess: [],
                finished: [],
            }
        }
        return newChar;
    }
}
