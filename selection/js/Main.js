
let storage = new Storage();

$(document).ready(function(){
    showCharacterInfo();
})

function characterSelected(value){
    console.log(value);
    storage.selectedCharacter(value);
    startOn();
    deleteOn();
}

function startOn(){
    $("#start-button").addClass("on");
}
function deleteOn(){
    $("#delete-button").addClass("on");
}

function start(){
    storage.start();
}
/*
create 
*/
function createCharacter(){
    if(!storage.newCharacter()){
        alert("characters full");
        // action cancel.
    }
}


/*
delete 
*/
function deleteCharacter(){
    storage.deleteCharacter();
    window.location.reload();
}

        
function showCharacterInfo(){
    let ctx;
    let gameData = storage.getGameData();
    let characters = gameData.accounts[storage.curAccNum].characters;
    for(let i = 0; i < characters.length; i++){
        let charJob = characters[i].information.job;
        ctx = document.getElementById("char-button" + (i + 1));
        ctx.innerHTML = charJob;
        $("#char-button" + (i + 1)).addClass("on");
    }
}
