class Storage{
    gameData;
    curAccNum;
    curCharNum;
    curJobNum;
    constructor(){
        //this.removeGameDataInStorage();
        this.setting();
        this.printGameData();
    }

    setting(){
        this.gameData = JSON.parse(localStorage.getItem("game"));
        this.curAccNum = this.gameData.currentData.account;
        this.curCharNum = this.gameData.currentData.character;
    }

    selectedJob(value){
        console.log(value);
        this.curJobNum = value;
    }

    newJobSelect(){
        this.insertJobData();
        this.saveGameData();
    }

    insertJobData(){
        this.gameData.accounts[this.curAccNum].characters[this.curCharNum].information.job = this.curJobNum;
        this.gameData.accounts[this.curAccNum].characters[this.curCharNum].information.code = this.curJobNum * 100;
    }

    saveGameData(){
        localStorage.setItem("game", JSON.stringify(this.gameData));
    }

    printGameData(){
        console.log(this.gameData);
    }
    getGameData(){
        return this.gameData;
    }
}
