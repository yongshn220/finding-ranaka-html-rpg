class Quest{
    constructor(game, storageData, questData){
        this.game = game;
        this.activation = true;
        this.storageData = storageData;
        this.questData = questData;
        this.questDataSetting();
        if(storageData){
            this.storageDataSetting();
        }
        else{
            this.actionIndex = 0;
            this.process = 0;
        }
        this.npcQuestMarkCheck();
    }
    questDataSetting(){
        this.name = this.questData.name;
        this.id = this.questData.id;
        this.questType = this.questData.questType;
        this.steps = this.questData.steps;
        this.prerequiste = this.questData.prerequiste;
        this.requirement = this.questData.requirement;
        this.award = this.questData.award;
        this.info = this.questData.info;
        this.preInfo = this.questData.preInfo;
    }
    storageDataSetting(){
        this.actionIndex = this.storageData.actionIndex;
        if(this.requirement.type === "kill"){
            this.requirement.process = this.storageData.process;
        }
        else if(this.requirement.type === "collect"){
            
        }
        else{
        }
    }
    npcQuestMarkCheck(){
        if(this.actionIndex === 0){
            let id = this.steps[0].id;
            this.game.questManager.npcQuestMarkState[id] = 0;
        }
        else if(this.actionIndex === (this.steps.length - 1)){
            let id = this.steps[0].id;
            this.game.questManager.npcQuestMarkState[id]= -1;
            id = this.steps[this.actionIndex].id;
            this.game.questManager.npcQuestMarkState[id] = 1;
        }
        else{

            let id = this.steps[0].id;
            this.game.questManager.npcQuestMarkState[id] = -1;
            id = this.steps[this.steps.length - 1].id;
            this.game.questManager.npcQuestMarkState[id] = -1;
        }
    }
    check(type, id){
        if(this.steps[this.actionIndex].id !== id){
            return;
        }
        if(this.steps[this.actionIndex].type === type){
            if(type === "monolog"){
                this.actionIndex++;
            }
            else if(type === "talk"){
                if(this.steps[this.actionIndex].id === id){
                    this.actionIndex++;
                    this.specialQuestCheck();
                    this.connectedAction();
                }
                else{
                    return;
                }
            }
            else if(type === "collect"){
                let items = this.game.player.inventory.items;
                items.forEach(item => {
                    if(this.requirement.object === item.code){
                        this.requirement.process = item.amount;
                        if(this.requirement.process >= this.requirement.amount){
                            this.actionIndex++;
                        }
                        this.questTextEvent();
                    }
                })
            }
            else if(type === "kill"){
                if(this.requirement.object === id){
                    this.requirement.process++;
                    if(this.requirement.process >= this.requirement.amount){
                        this.actionIndex++;
                    }
                    this.questTextEvent();
                }
            }
            else if(type === "specialization"){
                this.actionIndex++;
            }
        }

        this.actionIndexCheck();
        this.npcQuestMarkCheck();
        
    }
    connectedAction(){
        if(this.actionIndex >= this.steps.length){
            return;
        }
        if(this.steps[this.actionIndex].type === "collect"){
            this.check("collect", this.steps[this.actionIndex].id);
        }
    }
    questTextEvent(){
        let objectName;
        if(this.requirement.type === "kill"){
            objectName = `'${this.game.monsterManager.monsterData.getMonster(this.requirement.object).name}' 처치`;
        }
        else if(this.requirement.type === "collect"){
            objectName = `'${this.game.itemManager.itemData.getItem(this.requirement.object).name}' 수집`;
        }
        let data =  `${objectName}  ${this.requirement.process}  /  ${this.requirement.amount}`;
        let msg = new Message(this, this.game.textManager, "newQuestInfo", data);
        this.game.messageQueue.add(msg);
    }
    actionIndexCheck(){
        if(this.actionIndex >= this.steps.length){
            this.awardEvent();
            this.activation = false;
        }
    }
    getStorageData(){
        return {id : this.id, actionIndex: this.actionIndex, process: this.requirement.process};
    }
    awardEvent(){
        this.award.forEach(eachAward => {
            let msg = "";
            if(eachAward.type === "EXP"){
                msg = new Message(this, this.game.player.stat, "getEXP", eachAward.amount);
            }
            else if(eachAward.type === "gold"){
                msg = new Message(this, this.game.player.inventory, "newGold", eachAward.amount);
            }
            else if(eachAward.type === "item"){
                msg = new Message(this, this.game.player.inventory, "newItem", {id: eachAward.id, amount: eachAward.amount})
            }
            else if(eachAward.type === "stone"){
                msg = new Message();
            }
            else if(eachAward.type === "access"){
                msg = new Message(this, this.game.mapManager, "newAccess", eachAward.mapId);
                backup.save();
            }
            this.game.messageQueue.add(msg);
        });
    }
    specialQuestCheck(){
        // 만약 퀘스트 종료 일 때;
        if(this.actionIndex >= this.steps.length){
            return;
        }
        // specialization 일 때
        if(this.steps[this.actionIndex].type === "specialization"){
            this.specializationEvent(this.steps[this.actionIndex].id);
        }
    }
    specializationEvent(id){
        let msg = new Message(this, this.game.spcManager, "specialization", id);
        this.game.messageQueue.add(msg);
    }
}
