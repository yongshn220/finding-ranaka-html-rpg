class ItemManager{
    itemList;
    itemData;
    constructor(game){
        this.game = game;
        this.itemList = [];
        this.itemData = new ItemData();
    }
    
    levelUpdate(){
        this.imageUpdate();
    }
    imageUpdate(){
        this.itemList.forEach(item => item.imageUpdate());
        this.itemList = this.itemList.filter(item => item.activation);
    }

    update(){
        this.itemList.forEach(item => { 
            item.update();
        });
    }

    newGold(mData){
        let gold = new Gold(this.game, mData);
        this.game.addEntity(gold);
        this.itemList.push(gold);
    }
    newItem(mData){
        let iData = this.itemData.getItem(mData.code);
        let item = new Item(this.game, mData, iData);
        this.game.addEntity(item);
        this.itemList.push(item);
    }

    itemsReset(){
        this.itemList = [];
    }

    getDown(){
        this.itemList.forEach(item => {
            
        })
    }

    isMoveValid(){
        let gx = item.gx;
        let gy = item.gy;
        let xw = gx + item.sw;
        let yh = gy + item.sh;

        let gridx;
        let gridxw;
        let gridy;
        let gridyh;

        gridx = Math.floor((gx) / 40);
        gridxw = Math.floor((xw) / 40);
        gridy = Math.floor((gy + 1) / 40);  //y direction +1 : items move down only. 
        gridyh = Math.floor((yh + 1) / 40);

        if(this.game.mapManager.curMap.isAxisValidInStructure(gridx,gridy,gridxw,gridyh)){
            return true;
        }
    }

    onMessage(msg){
        if(msg.type === "gold"){
            this.newGold(msg.data);
        }
        else if(msg.type === "item"){
            this.newItem(msg.data);
        }
        else if(msg.type === "reset"){
            this.itemsReset();
        }
    }
}