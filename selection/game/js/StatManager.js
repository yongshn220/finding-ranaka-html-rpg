class StatManager{
    game;
    statData;
    constructor(game){
        this.game = game;
        this.statData = new StatData(game);
    }

    update(){

    }

    statReset(by){
        this.LV = this.game.player.stat.LV;
        let newHP = this.statCalculation("HP");
        let newMP = this.statCalculation("MP");
        let newAD = this.statCalculation("AD");
        let newCP = this.statCalculation("CP");
        let newCD = this.statCalculation("CD");
        let newDEF = this.statCalculation("DEF");
        let newAS = this.statCalculation("AS");
        let newStat = {
            HP : newHP,
            MP : newMP,
            AD : newAD,
            CP : newCP,
            CD : newCD,
            DEF : newDEF,
            AS : newAS,
        }
        let msg = new Message(this, this.game.player.stat, "newStat", {stat: newStat, by: by});
        this.game.messageQueue.add(msg);
        msg = new Message(this, this.game.UI.UIInteraction.statInteraction, "statUIUpdate", {});
        this.game.messageQueue.add(msg);
    }

    statCalculation(type){
        let basicStat = this.statData.getCalculatedStat(this.LV, type);
        let statpoints = this.game.player.stat.getStatpoint(type);
        let equipments = this.game.player.inventory.calculateOption(type);
        let buff = this.game.player.skillManager.calculateOption(type);

        return Math.floor(((basicStat * (statpoints/100)) + equipments) * (buff/100));
    }

    statpointClicked(type){
        this.game.player.stat.haveStatpoints--;
        this.game.player.stat.statpoints[type].cur++;
        this.statReset();
    }

    onMessage(msg){
        if(msg.type === "statReset"){
            this.statReset(msg.data);
        }
        else if(msg.type === "statpointClicked"){
            this.statpointClicked(msg.data);
        }
    }
}

// 기본스텟 + 스텟포인트(퍼센트) + 장비 + 스톤 + 스킬(퍼센트) 
