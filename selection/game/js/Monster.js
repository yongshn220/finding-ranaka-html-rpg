class Monster{
    constructor(game, data, pos){
        this.game = game;
        this.data = data;
        this.pos = pos;
        this.timer = 0; 
        this.positionSetting();
        this.setting();
    }
    
    positionSetting(){
        this.dir = "left";
        this.sx = 0;
        this.sy = 0;
        this.sw = this.data.sw;
        this.sh = this.data.sh;
        this.dx = 0;
        this.dy = 0;
        this.dw = this.data.sw;
        this.dh = this.data.sh;
        this.randomGlobalPosition();
    }

    randomGlobalPosition(){
        let diff = this.pos.maxGx - this.pos.minGx;
        this.gx = this.pos.minGx + Math.floor(diff * Math.random());
        this.gy = this.pos.Gy;
    }

    setting(){
        this.id = this.data.id;
        this.name = this.data.name;
        this.LV = this.data.LV;
        this.AD = this.data.AD;
        this.maxHP = this.data.HP;
        this.curHP = this.maxHP;
        this.DEF = this.data.DEF;
        this.EXP = this.data.EXP;
        this.img = this.data.img;
        this.isAttack = this.data.isAttack;
        this.maxSourceX = this.sw * this.data.frame;
        this.maxSourceY = this.sh * 5;
        this.moveSpeed = this.data.moveSpeed;
        this.stands = this.data.stands;
        this.drops = this.data.drops;
        this.isMoving = true;
        this.state = "stand";
        this.activation = true;
        this.infoState = false;
        this.hpImg = new Image;
        this.hpImg.src = `./images/monsters/info/hp.png`;
        this.hpSx = 0;
        this.infoImg = this.data.infoImg;
        this.touchEffect = this.data.touchEffect;
        this.touchEffectOn = false;
    }

    move(){
        if(this.state === "move"){
            if(this.dir === "left"){
                if(this.isMonsterInBckgrnd(this.gx - this.moveSpeed, this.gy)){
                    this.gx -= this.moveSpeed;
                }
            }
            else{
                if(this.isMonsterInBckgrnd(this.gx + this.moveSpeed, this.gy)){
                    this.gx += this.moveSpeed;
                }
            }
        }
        this.moveAreaCheck();
    }

    moveAreaCheck(){
        if(this.state === "dead"){
            return;
        }
        let checkPointA = this.game.mapManager.curMap.isPointCollid(this.gx, this.gy + this.sh + 10, "wall");
        let checkPointB = this.game.mapManager.curMap.isPointCollid(this.gx + this.sw, this.gy + this.sh + 10, "wall");
        if(!checkPointA){
            this.gx += 10;
            this.directionChange();
        }
        if(!checkPointB){
            this.gx -= 10;
            this.directionChange();
        }
    }

    isMonsterInBckgrnd(gx, gy){
        let bw = this.game.mapManager.curMap.mainBackground.width;
        let bh = this.game.mapManager.curMap.mainBackground.height;
        if(gx > 100 &&  gy > 0 && gx < bw - this.sw && gy < bh - this.sh){
            return true;
        }
        return false;
    }

    imageUpdate(){
        this.timer += 0.2;
        if(this.sx < this.maxSourceX - this.sw){
            this.sx += this.sw;
        }
        else{
            if(this.state === "attacked" || this.state === "dead"){
                if(this.state === "dead"){
                    this.activation = false;
                }
            }
            else{
                this.sx = 0;
            }
        }

        this.motion();
    }

    motion(){
        if(this.state === "dead"){
            return;
        }
        let rNum = Math.random();

        if(rNum < 0.1 || (this.state === "attacked" && this.timer > 0.5)){
            rNum = Math.random();
            if(rNum < 0.3){
                this.standing();
            }
            else{
                this.directionChange();
            }
        }
    }

    standing(){
        this.state = "stand";
        if(this.sy < this.maxSourceY){
            this.sy = this.sh;
        }
        else{
            this.sy = this.maxSourceY + this.sh;
        }
        this.maxSourceX = this.sw * 2;
        this.sx = 0;
    }

    directionChange(){
        this.state = "move";
        this.maxSourceX = this.sw * this.data.frame;
        if(this.sy < this.maxSourceY){
            this.sy = this.maxSourceY;
            this.dir = "right"
        }
        else{
            this.sy = 0;
            this.dir = "left";
        }
    }

    positionAdjust(){
        let background = this.game.mapManager.curMap.mainBackground;
        this.dx = this.gx - background.sx;
        this.dy = this.gy - background.sy;
    }

    draw(){
        this.positionAdjust();
        canvas.context.drawImage(this.img,this.sx,this.sy,this.sw,this.sh,this.dx,this.dy,this.dw,this.dh);
    }
    
    drawHpBar(){
        let dx = this.dx + ((this.sw - 50) / 2);
        canvas.context.drawImage(this.hpImg,this.hpSx,0,50,10,dx,this.dy - 15,50,10);
    }

    drawInfo(){
        let dx = this.dx + ((this.sw - 100) / 2);
        let dy = this.dy + this.sh + 5
        canvas.context.drawImage(this.infoImg,0,0,100,15,dx,dy,100,15);
    }

    update(){
        this.draw();
        if(this.infoState){
            this.drawHpBar();
            this.drawInfo();
        } 
        this.move();
    }
    touchEffectEvent(){
        if(this.touchEffect){
            if(!this.touchEffectOn){
                this.touchEffectOn = true;
                this.touchEffect.types.forEach(type => {
                    this[type] = this.touchEffect[type];
                });
            }
        }   
    }
    hit(skill){
        if(this.state === "dead"){
            return;
        }
        this.touchEffectEvent();
        if(!this.stands){
            let cgx = this.game.player.character.gx;
            this.sx = 0;
            if(cgx < this.gx){
                this.sy = this.sh * 2;
            }
            else{
                this.sy = this.maxSourceY + (this.sh * 2)
            }
            this.timer = 0;
            this.maxSourceX = this.sw;
            this.state = "attacked";
        }
        this.damaged(skill);
    }

    damaged(damageData){
        damageData = this.damageUpdate(damageData);
        this.showDamage(damageData);
        this.infoState = true;
        damageData.damages.forEach(damage => {
            if(this.state === "dead"){
                return;
            }
            this.curHP -= damage;
            this.hpBarSetting();
            this.healthPointCheck();
        })
    }
    damageUpdate(damageData){
        let newDamages = damageData.damages.map((damage, i) => {
            let newDamage = damage - this.DEF;
            if(newDamage < 0){
                newDamage = 0;
            }

            if(damageData.criticals[i]){
                newDamage *= 2;
            }
            return newDamage;
        })

        damageData.damages = newDamages;
        return damageData;
    }

    hpBarSetting(){
        this.hpSx = (1150 * this.curHP / this.maxHP) - ((1150 * this.curHP / this.maxHP) % 50);
    }
    showDamage(damageData){
        let data = {nums: damageData.damages, criticals: damageData.criticals, gx: this.gx, gy: this.gy - 20,}
        let msg = new Message(this, this.game.damageManager, "newDamage", data);
        this.game.messageQueue.add(msg);
    }
    healthPointCheck(){
        if(this.curHP <= 0){
            this.state = "dead";
            this.dropEvent();
            this.dropEXP();
            this.deadMotion();
            let msg = new Message(this, this.game.questManager, "questCheck", {type: "kill", id: this.id});
            this.game.messageQueue.add(msg);
            msg = new Message(this, this.game.textManager, "newInfo", {type:"경험치 획득", value:this.EXP});
            this.game.messageQueue.add(msg);
        }
    }
    deadMotion(){
        this.maxSourceX = this.sw * this.data.frame;
        this.sx = 0;
        if(this.dir === "left"){
            this.sy = this.sh * 4;
        }
        else{
            this.sy = this.maxSourceY + (this.sh * 4);
        }
    }

    hitEffect(img){
        this.hitImg = img;
    }

    dropEXP(){
        let msg = new Message(this, this.game.player.stat, "getEXP", this.EXP);
        this.game.messageQueue.add(msg);
    }

    dropEvent(){
        this.dropGold();
        this.dropItems();
    }

    dropGold(){
        let rNum = Math.random() * 100;
        if(rNum < this.drops.gold.percent){
            let newGold = this.drops.gold.data + Math.floor(this.drops.gold.data * (Math.random() - 0.5));
            let data = {
                gx: this.gx,
                gy: this.gy,
                value: newGold,
            }
            let msg = new Message(this, this.game.itemManager, "gold", data);
            this.game.messageQueue.add(msg);
        }
    }
    dropItems(){
        let rNum;
        for(let i = 0; i < this.drops.items.length; i++){
            rNum = Math.random() * 100;
            if(rNum < this.drops.items[i].percent){
                let data = {
                    gx: this.gx,
                    gy: this.gy,
                    code: this.drops.items[i].data,
                }
                let msg = new Message(this, this.game.itemManager, "item", data);
                this.game.messageQueue.add(msg);
            }
        }
    }

    onMessage(msg){
        if(msg.type === "hit"){
            this.hit(msg.data);
        }
        else if(msg.type === "hitEffect"){
            this.hitEffect(msg.data);
        }
    }
}
