class SkillData{
    job;
    constructor(){
        this.job = Array(3).fill({});
        this.setting();
    }

    setting(){
        this.setSkillData();
        this.setSkillCodeList();
    }
    getSkillAll(code){
        return this.skillList[code];
    }
    getSkill(code){
        return this.skillData[code];
    }
    getSkillImg(job, spc, skill){
        let img = new Image();
        img.src = './images/skills/job' + job + '/spc' + spc + '/skill' + skill + '.png'; 
        return img;
    }
    getCharImg(job, spc, skill){
        let img = new Image();
        img.src = './images/character/job' + job + '/spc' + spc + '/skill-motion' + skill + '.png'; 
        return img;
    }
    getHitEffect(num){
        let img = new Image();
        img.src = './images/skills/hit/effect' + num + '.png';
        let sw;
        let sh;
        let frame;

        if(num === 1){
            sw = 40;
            sh = 40;
            frame = 5;
        }
        else if(num === 2){
            sw = 40;
            sh = 40;
            frame = 5;
        }
        else if(num === 3){
            sw = 40;
            sh = 40;
            frame = 3;
        }
        else if(true){

        }
        return {img: img, sw: sw, sh: sh, frame: frame};
    }
    setSkillCodeList(){
        this.skillList = {
            // 120 -> 1(직업)20(spc)
            100: [
                this.skillData[100], 
                this.skillData[101], 
                this.skillData[102],
            ],
            110: [
                this.skillData[100], 
                this.skillData[101], 
                this.skillData[102],
                this.skillData[103],
                this.skillData[104],
            ],
            111: [
                this.skillData[100], 
                this.skillData[101], 
                this.skillData[102],
            ],
            112: [
                this.skillData[100], 
                this.skillData[101], 
                this.skillData[102],
            ],
            120: [
                this.skillData[100], 
                this.skillData[101], 
                this.skillData[102], 
                this.skillData[103],
                this.skillData[104],
                this.skillData[105],
                this.skillData[106],
                this.skillData[107],
                this.skillData[108],
            ],
            121: [
                this.skillData[100], 
                this.skillData[101], 
                this.skillData[102],
            ],
            122: [
                this.skillData[100], 
                this.skillData[101], 
                this.skillData[102],
            ],
        }
    }
    setSkillData(){
        this.skillData = {
            // 100 -> 1(직업)00(스킬번호)
            100 : {
                code: 100, //keyboard 세팅용 코드 
                name: "슬래쉬",
                info: "전방의 적을 칼로 벤다",
                usage: "active",
                type: "attack",
                gapX: -40,
                gapY: 40, 
                dw: 200,
                dh: 160,
                delayTime: 0.4,
                charImg: this.getCharImg(1,0,1),
                charFrame: 2,
                skillImg: this.getSkillImg(1,0,1),
                skillFrame: 7,
                hitEffect: this.getHitEffect(1),
                attackOption: {
                    reload: {
                        frames: [0],
                    }     
                },
                option: {
                    type: "공격",
                    damage: 60,
                    maxHits: 2,
                    maxObjects: 5,
                    coolTime: 0,
                    MPDeduct: 50,
                    contains: ["type", "damage", "maxHits", "maxObjects", "coolTime", "MPDeduct"],
                }
            },
            101: {
                code: 101,
                name: "전사의 결의",
                info: "굳은 결의를 다져 공격력과 방어력을 강화한다.",
                usage: "active",
                type: "buff",
                gapX: -85,
                gapY: 60, 
                dw: 80,
                dh: 80,
                delayTime: 0.5,
                charImg: this.getCharImg(1,0,2),
                charFrame: 1,
                skillImg: this.getSkillImg(1,0,2),
                skillFrame: 8,

                option: {
                    type: "버프",
                    AD: 3,
                    DEF: 10,
                    coolTime: 0,
                    duration: 60,
                    MPDeduct: 50,
                    contains: ["type", "AD", "DEF", "coolTime", "duration", "MPDeduct"],
                },
            },
            102: {
                code: 102,
                name: "의지의 힘",
                info: "의지력으로 순간적인 힘을 증폭시킨다.",
                usage: "passive",
                type: "buff",
                option: {
                    type: "패시브",
                    CD:5,
                    contains: ["type", "CD"],
                }
            },
            103: {
                code: 103,
                name: "문 슬래쉬",
                info: "전방의 적을 쓸어버린다.",
                usage: "active",
                type: "attack",
                gapX: -160,
                gapY: 60, 
                dw: 400,
                dh: 160,
                delayTime: 0.7,
                charImg: this.getCharImg(1,10,1),
                charFrame: 2,
                skillImg: this.getSkillImg(1,10,1),
                skillFrame: 8,
                hitEffect: this.getHitEffect(1),
                attackOption: {
                    reload: {
                        frames: [0],
                    },     
                },
                option: {
                    type: "공격",
                    damage: 70,
                    maxHits: 4,
                    maxObjects: 5,
                    coolTime: 0,
                    MPDeduct: 70,
                    contains: ["type", "damage", "maxHits", "maxObjects", "coolTime", "MPDeduct"],
                },

            },
            104: {
                code: 104,
                name: "월아일체",
                info: "검과 하나가 되어 더욱 민첩해진다.",
                usage: "active",
                type: "buff",
                gapX: -120,
                gapY: 80, 
                dw: 160,
                dh: 160,
                delayTime: 0.4,
                charImg: this.getCharImg(1,10,2),
                charFrame: 1,
                skillImg: this.getSkillImg(1,10,2),
                skillFrame: 8,
                option: {
                    type: "버프",
                    AS: 10,
                    duration: 120,
                    MPDeduct: 300,
                    contains: ["type", "AS","MPDeduct", "duration"],
                },
            },
            105: {
                code: 105,
                name: "블레이드 스톰",
                info: "회전을 하면서 주위를 벤다.",
                usage: "active",
                type: "attack",
                gapX: -280,
                gapY: 30, 
                dw: 500,
                dh: 120,
                delayTime: 0.7,
                charImg: this.getCharImg(1,20,1),
                charFrame: 2,
                skillImg: this.getSkillImg(1,20,1),
                skillFrame: 6,
                hitEffect: this.getHitEffect(2),
                attackOption: {
                    reload: {
                        frames: [0,3],
                    }     
                },
                option: {
                    type: "공격",
                    damage: 60,
                    maxHits: 3,
                    maxObjects: 5,
                    coolTime: 0,
                    MPDeduct: 70,
                    contains: ["type", "damage", "maxHits", "maxObjects", "coolTime", "MPDeduct"],
                }
            },
            106: {
                code: 106,
                name: "크러스트",
                info: "전방의 적을 수차례 찌른다.",
                usage: "active",
                type: "attack",
                gapX: -180,
                gapY: 9, 
                dw: 300,
                dh: 120,
                delayTime: 0.7,
                charImg: this.getCharImg(1,20,3),
                charFrame: 3,
                skillImg: this.getSkillImg(1,20,3),
                skillFrame: 8,
                hitEffect: this.getHitEffect(1),
                attackOption: {
                    reload: {
                        frames: [3,4,5,6,7],
                    }     
                },
                option: {
                    type: "공격",
                    damage: 100,
                    maxHits: 3,
                    maxObjects: 1,
                    coolTime: 0,
                    MPDeduct: 260,
                    contains: ["type", "damage", "maxHits", "maxObjects", "coolTime", "MPDeduct"],
                }
            },
            107: {
                code: 107,
                name: "달빛진공참",
                info: "달빛의 힘을 빌려 전방으로 진공참을 날린다.",
                usage: "active",
                type: "attack",
                gapX: -250,
                gapY: 50, 
                dw: 400,
                dh: 150,
                delayTime: 1.0,
                charImg: this.getCharImg(1,20,4),
                charFrame: 1,
                skillImg: this.getSkillImg(1,20,4),
                skillFrame: 3,
                hitEffect: this.getHitEffect(3),
                attackOption: {
                    reload: {
                        frames: [0,1,2],
                    },
                    moving:{
                        distance: 800,
                        speed: 15,
                    }   
                },
                option: {
                    type: "공격",
                    damage: 360,
                    maxHits: 2,
                    maxObjects: 10,
                    coolTime: 10,
                    MPDeduct: 3000,
                    contains: ["type", "damage", "maxHits", "maxObjects", "coolTime", "MPDeduct"],
                }
            },
            108: {
                code: 108,
                name: "문 브레이크",
                info: "달을 소환하여 베어버린다. ",
                usage: "active",
                type: "attack",
                gapX: -250,
                gapY: 150, 
                dw: 600,
                dh: 320,
                delayTime: 0.8,
                charImg: this.getCharImg(1,20,5),
                charFrame: 4,
                skillImg: this.getSkillImg(1,20,5),
                skillFrame: 10,
                hitEffect: this.getHitEffect(3),
                attackOption: {
                    reload: {
                        frames: [4,5,6,7],
                    }  
                },
                option: {
                    type: "공격",
                    damage: 80,
                    maxHits: 3,
                    maxObjects: 10,
                    coolTime: 0,
                    MPDeduct: 350,
                    contains: ["type", "damage", "maxHits", "maxObjects", "coolTime", "MPDeduct"],
                }
            }
        }
    }
    
}