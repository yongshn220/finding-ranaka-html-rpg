class TimeManager{
    timers;
    now;
    constructor(game){
        this.game = game;
        this.timers = [];
        this.now = 0;
        this.create();
    }
    create(){
        let charTime = {
            name : "charTime",
            state : true,
            autoOff : false,
            start : 0,
            elapsed : 0,
            level : 200,
            dest : this.game.player.character,
        }
        this.timers.push(charTime);

        let monsterTime = {
            name : "monsterTime",
            state : true,
            autoOff : false,
            start : 0,
            elapsed : 0,
            level : 200,
            dest : this.game.monsterManager,
        }
        this.timers.push(monsterTime);
        
        let skillTime = {
            name : "skillTime",
            state : true,
            autoOff : false,
            start : 0,
            elapsed : 0,
            level : 100,
            dest : this.game.player.skillManager,
        }
        this.timers.push(skillTime);

        let gravityTime = {
            name : "gravityTime",
            state : true,
            autoOff : false,
            start : 0,
            elapsed : 0,
            level : 10,
            dest : this.game,
        }
        this.timers.push(gravityTime);
        
        let itemTime = {
            name : "itemTime",
            state : true,
            autoOff : false,
            start : 0,
            elapsed : 0,
            level : 200,
            dest : this.game.itemManager,
        }
        this.timers.push(itemTime);

        let charResting = {
            name : "charResting",
            state : true,
            autoOff : false,
            start : 0,
            elapsed : 0,
            level : 1000,
            dest : this.game.player.stat,
        }
        this.timers.push(charResting);
        
        let showUI = {
            name : "showUI",
            state : true,
            autoOff : false,
            start : 0,
            elapsed : 0,
            level : 200,
            dest : this.game.UI.showUI,
        }
        this.timers.push(showUI);

        let charAttacked = {
            name: "charAttacked",
            state: false,
            autoOff : true,
            start : 0,
            elapsed : 0,
            level : 2000,
            dest : this.game.player.character,
        }
        this.timers.push(charAttacked);

        let damageTime = {
            name: "damageTime",
            state: true,
            autoOff : false,
            start : 0,
            elapsed : 0,
            level : 100,
            dest : this.game.damageManager,
        }
        this.timers.push(damageTime);
        
        let textInfoTime = {
            name: "textTime",
            state: true,
            autoOff : false,
            start : 0,
            elapsed : 0,
            level : 1000,
            dest : this.game.textManager,
        }
        this.timers.push(textInfoTime);
    }

    levelUpdate(now){
        this.now = now;
        this.alwaysUpdate();

        this.timers.forEach(timer => {
            if(timer.state){
                timer.elapsed = now - timer.start;
                if(timer.elapsed >= timer.level){
                    timer.dest.levelUpdate();
                    timer.start = now;
                    if(timer.autoOff){
                        timer.state = false;
                    }
                }
            }
        })  
    }

    alwaysUpdate(){
        this.game.messageQueue.dispatch();
        this.game.mapManager.curMap.update();
        this.game.monsterManager.update();
        this.game.player.character.update();
        this.game.player.skillManager.update();
        this.game.backgroundCenterAdjust();
        this.game.itemManager.update();
        this.game.entityFilterling();
        this.game.damageManager.update();
    }

    timerReset(timer){
        timer.state = true;
        timer.elapsed = 0;
        timer.start = this.now;
    }

    timerOn(name){
        this.timers.forEach(timer => {
            if(timer.name === name){
                this.timerReset(timer);
            }
        })
    }

    timerOff(name){
        this.timers.forEach(timer => {
            if(timer.name === name){
                timer.state = false;
            }
        })
    }

    getTimer(name){
        for(let i = 0; i < this.timers.length; i++){
            if(this.timers[i].name === name){
                return this.timers[i];
            }
        }
    }

    onMessage(msg){
        if(msg.type === "timerOn"){
            this.timerOn(msg.data);
        }
        else if(msg.type === "timerOff"){
            this.timerOff(msg.data);
        }
    }
}