class Skill extends Entity{
    game;
    name;
    usage;
    type;
    maxSx;
    coolTime;
    duration;
    damage;
    MP;
    activation;
    char;
    hitImg;
    constructor(game, data, buffOption){
        super();
        this.game = game;
        this.data = data;
        this.char = this.game.player.character;
        this.activation = true;
        this.imgState = false;
        this.setting();
        //this.buffOptionSetting();
        
    }
    
    positionSetting(){
        this.sx = 0;
        this.sw = this.data.dw;
        this.sh = this.data.dh;
        this.dw = this.data.dw;
        this.dh = this.data.dh;
        this.maxSx = this.sw * this.data.skillFrame;
        this.viewPoint(true);
    }

    setting(){
        this.name = this.data.name;
        this.usage = this.data.usage;
        this.type = this.data.type;
        this.skillImg = this.data.skillImg;
        this.code = this.data.code;
        this.curFrame = 0;
        if(this.usage === "active" && this.type === "attack"){
            this.activeAttackSetting();
        }
        else if(this.usage === "active" && this.type === "buff"){
            this.activeBuffSetting();
        }
        else if(this.usage === "passive"){
            this.passiveBuffSetting();
        }
        else{
            this.duration = 1;
        }
    }

    activeAttackSetting(){
        this.positionSetting();
        this.coolTime = this.data.option.coolTime;
        this.damage = this.data.option.damage;
        this.MP = this.data.option.MPDeduct;
        this.maxHits = this.data.option.maxHits;
        this.maxObjects = this.data.option.maxObjects; // 최대 공격 가능 몬스터 수
        this.hitEffect = this.data.hitEffect;
        this.attackOptionSetting();
    }
    attackOptionSetting(){
        this.reloadFrames = this.data.attackOption.reload.frames;
        this.moving = this.data.attackOption.moving;
        if(this.moving){
            this.curDx = this.dx;
            this.movingState = true;
        }
        else{
            this.movingState = false;
        }
        this.hitState = false; // false: 공격 전, true: 공격 후
    }
    activeBuffSetting(){
        this.positionSetting();
        this.coolTime = this.data.option.coolTime;
        this.duration = this.data.option.duration;
        this.MP = this.data.option.MPDeduct;
        this.option = this.data.option;
    }
    passiveBuffSetting(){
        this.option = this.data.option;
    }
    hitStateCheck(){
        if(this.usage === "active" && this.type === "attack"){
            let state =  this.reloadFrames.some(i => {
                if(this.curFrame === i){
                    return true;
                }
            })
            if(state){
                this.hitState = true;
            }
        }
    }
    imageUpdate(){
        this.hitStateCheck();
        this.curFrame++;
        if(this.sx < (this.maxSx - this.sw)){
            this.sx += this.sw;
        } 
        else{
            if(!this.movingState){
                this.activation = false;
            }
            else{
                this.sx = 0;
                this.curFrame = 0;
            }
        }
        
    }

    durationUpdate(){
        if(this.usage === "passive"){
            return;
        }
        this.duration -= 0.1;
        if(this.duration < 0){
            this.activation = false;
            let msg = new Message(this, this.game.statManager, "statReset", "");
            this.game.messageQueue.add(msg);
            msg = new Message(this, this.game.UI.UIInteraction.skillInteraction, "castingBuffUpdate", {});
            this.game.messageQueue.add(msg);
        }
    }
    reactivate(){
        this.duration = this.data.duration;
    }

    viewPoint(ignore){
        if(!ignore){
            if(this.moving){
                let bg = this.game.mapManager.curMap.mainBackground;
                this.gx = bg.sx + this.dx;
                this.gy = bg.sy + this.dy;
                return;
            }
        }
        let v = this.char.view;
        this.view = v;
        if(v === "left"){
            this.sy = 0;
            this.gx = (this.char.gx + this.char.RX) - (this.sw + this.data.gapX);
            this.gy = (this.char.gy + this.char.RY) - (this.data.gapY);
            this.dx = (this.char.dx + this.char.RX) - (this.sw + this.data.gapX);
            this.dy = (this.char.dy + this.char.RY) - (this.data.gapY);
        }
        else if(v === "right"){
            this.sy = this.sh;
            this.gx = (this.char.gx + this.char.RX) + (this.char.RW + this.data.gapX);
            this.gy = (this.char.gy + this.char.RY) - (this.data.gapY);
            this.dx = (this.char.dx + this.char.RX) + (this.char.RW + this.data.gapX);
            this.dy = (this.char.dy + this.char.RY) - (this.data.gapY);
        }
    }
    
    movingCheck(){
        if(this.moving){
            if(this.sy === 0){
                this.dx -= this.moving.speed;
            }
            else{
                this.dx += this.moving.speed;
            }
            if(Math.sqrt((this.dx - this.curDx)**2) >= this.moving.distance){
                this.activation = false;
            }
        }
    }
    update(){
        this.movingCheck();
        this.draw();
    }

    draw(){
        this.viewPoint(false);
        canvas.context.drawImage(this.skillImg,this.sx,this.sy,this.sw,this.sh,this.dx,this.dy,this.dw,this.dh);
    }

    onMessage(){

    }

}

class HitEffect extends Entity{
    constructor(game, effect, monster){
        super();
        this.game = game;
        this.activation = true;
        this.img = effect.img;
        this.positionSetting(effect, monster);
    }

    positionSetting(e, m){
        this.sx = 0;
        this.sy = 0; 
        this.sw = e.sw;
        this.sh = e.sh;
        this.gx = m.gx;
        this.gy = m.gy;
        this.dx = 0;
        this.dy = 0;
        this.dw = m.sw;
        this.dh = m.sh;
        this.maxSx = this.sw * e.frame;
    }
    positionAdjust(){
        let background = this.game.mapManager.curMap.mainBackground;
        this.dx = this.gx - background.sx;
        this.dy = this.gy - background.sy;
    }
    imageUpdate(){
        if(this.sx < (this.maxSx - this.sw)){
            this.sx += this.sw;
        } 
        else{
            this.activation = false;
        }
    }

    update(){
        this.draw();
    }

    draw(){
        this.positionAdjust()
        canvas.context.drawImage(this.img,this.sx,this.sy,this.sw,this.sh,this.dx,this.dy,this.dw,this.dh);
    }

}