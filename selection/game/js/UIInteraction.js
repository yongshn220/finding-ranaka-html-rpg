class UIInteraction{
    game;
    constructor(game){
        this.game = game;
        this.inventoryInteraction = new UIInteractionInventory(this.game);
        this.skillInteraction = new UIInteractionSkill(this.game);
        this.questInteraction = new UIInteractionQuest(this.game);
        this.statInteraction = new UIInteractionStat(this.game);
        this.textInteraction = new UIInteractionText(this.game);
        this.storeInteraction = new UIInteractionStore(this.game);
        this.spcInteraction =  new UIInteractionSpc(this.game);
        this.miniMapInteraction = new UIInteractionMiniMap(this.game);
        this.states = {
            stat: false,
            skill: false,
            inventory: false,
            quest: false,
            contents: false,
            stone: false,
            map: false,
        }
        this.stateNames = ["stat", "skill", "inventory", "quest", "contents", "stone", "map"];
    }

    clicked(val){
        this.UIButtonOn(val);
        if(val === "quest"){
            let msg = new Message(this, this.questInteraction, "questUpdate", {});
            this.game.messageQueue.add(msg);
            $('#quest-detail').removeClass('on');
        }
        else if(val === "stat"){
            $('#stat-button').removeClass('hover');
        }
    }

    UIButtonOn(UI){
        if(this.states[UI]){
            this.states[UI] = false;
            $(`#${UI}`).removeClass();
            $(`#${UI}-button`).removeClass();
        }
        else{
            this.stateNames.forEach(name => {
                this.states[name] = false;
                $(`#${name}`).removeClass();
                $(`#${name}-button`).removeClass();
            })
            this.states[UI] = true;
            $(`#${UI}`).addClass("on");
            $(`#${UI}-button`).addClass("on");
        }
    }

    talkStart(text, name){
        $('#talk').addClass('on');
        let element = document.getElementById('talk-name');
        element.innerHTML = name;
        element = document.getElementById('talk-text');
        element.innerHTML = text;
    }
    talkEnd(){
        $('#talk').removeClass();
    }
    
    onMessage(msg){
        if(msg.type === "clicked"){
            this.clicked(msg.data);
        }
        else if(msg.type === "talkStart"){
            this.talkStart(msg.data.text, msg.data.name);
        }
        else if(msg.type === "talkEnd"){
            this.talkEnd();
        }
    }
}

class UIInteractionInventory{
    constructor(game){
        this.game = game;
        this.setting();
    }

    setting(){
        //this.update();
        this.keyImgSetting();
    }
    keyImgSetting(){
        let elements = document.querySelectorAll('.consum-key');
        elements.forEach(element => {
            let id = element.getAttribute('id');
            let img = `./images/items/ui/${id}.png`;
            $(`#${id}`).css('background', `url(${img})`);
        });
    }
    keyBackup(){
        let deleteKeyItemCode = this.game.player.keyBoard.delete;
        let endKeyItemCode = this.game.player.keyBoard.end;
        if(deleteKeyItemCode > 0){
            this.haveToKey("consumption-key1", deleteKeyItemCode);
        }
        if(endKeyItemCode > 0){
            this.haveToKey("consumption-key2", endKeyItemCode);
        }
    }
    
    screenKeyUpdate(){
        let element = document.getElementById('consumption-key1-amount');
        let innerElement = document.getElementById('consumption-key1');
        let value = innerElement.getAttribute('value') * 1;
        if(value > 0){
            element.innerHTML = this.game.player.inventory.getItemAmount(value);
        }
      
        element = document.getElementById('consumption-key2-amount');
        innerElement = document.getElementById('consumption-key2');
        value = innerElement.getAttribute('value') * 1;
        if(value > 0){
            element.innerHTML = this.game.player.inventory.getItemAmount(value);
        }
    }
    itemTypeChange(id, type){
        let types = ["equipment", "consumption", "general", "other"];
        for(let t = 0; t < types.length; t++){
            if(types[t] === type){
                $(`#d-${types[t]}`).addClass('on');
                $(`#i-${types[t]}`).addClass('on');
            }
            else{
                $(`#d-${types[t]}`).removeClass('on');
                $(`#i-${types[t]}`).removeClass('on');
            }
        }
    }

    update(){
        this.inventoryReset();
        this.inventoryHaveItemsUpdate();
        this.inventoryEquipmentUpdate();
        this.screenKeyUpdate();

        //gold
        let gold = this.game.player.inventory.gold;
        let element = document.getElementById('gold-text');
        element.innerHTML = gold;
    }
    inventoryHaveItemsUpdate(){
        let img;
        let iNum = [0,0,0,0];
        let items = this.game.player.inventory.items;
        let element;
        let t;
        for(let i = 0; i < items.length; i++){
            t = Math.floor(items[i].code / 1000) - 1;
            img = `./images/items/innerItem/${items[i].code}.png`
            $(`#itemType${t}-data${iNum[t]}`).css("background", `url(${img})`);
            element = document.getElementById(`itemType${t}-data${iNum[t]}`);
            element.setAttribute('draggable', true);
            element.setAttribute('drag-type', items[i].type);
            element.innerHTML = items[i].amount;
            element.value = items[i].code;
            iNum[t]++;
        }
    }
    inventoryEquipmentUpdate(){
        let img;
        let fitItems = this.game.player.inventory.fitItems;
        let elements = document.querySelectorAll(".equip");
        for(let i = 0; i < elements.length; i++){
            for(let j = 0; j < fitItems.length; j++){
                if(elements[i].getAttribute('drag-type') === fitItems[j].type){
                    img = `./images/items/innerItem/${fitItems[j].code}.png`
                    $(`#${elements[i].getAttribute('id')}`).css("background", `url(${img})`);
                    elements[i].setAttribute('draggable', true);
                    elements[i].setAttribute('value', fitItems[j].code);
                }
            }
        }
    }

    inventoryReset(){
        let img = './images/items/innerItem/empty.png';
        let element;
        for(let t = 0; t < 4; t++){
            for(let i = 0; i < 35; i++){
                $(`#itemType${t}-data${i}`).css("background", `url(${img})`)
                element = document.getElementById(`itemType${t}-data${i}`);
                element.setAttribute("value", -1);
                element.innerHTML = "";
                element.setAttribute("draggable", false);
                element.setAttribute("drag-type", "blank");
            }
        }
        let elements = document.querySelectorAll(".equip");
        for(let i = 0; i < elements.length; i++){
             $(`#${elements[i].getAttribute('id')}`).css("background", `url(${img})`);
             elements[i].setAttribute('value', -1); 
             elements[i].setAttribute('draggable', false);
        }
    }
    
    //show info
    itemClicked(code){ 
        if(code === -1){
            return;
        }
        $(`#inventory-detail`).removeClass().addClass("on");
        let item = this.game.itemManager.itemData.getItem(code);
        let img = `./images/items/innerItem/${code}.png`
        $(`#inventory-detail-img`).css("background", `url(${img})`);
        let element = document.getElementById(`inventory-detail-name`);
        element.innerHTML = item.name;
        element = document.getElementById(`item-info`);
        element.innerHTML = item.info;
        if(item.option){
            this.setItemOption(item);
        }
    }
    setItemOption(item){
        let elementLeft = document.getElementById(`item-effect-name`);
        let elementRight = document.getElementById(`item-effect-value`);
        elementLeft.innerHTML = "";
        elementRight.innerHTML = "";
        item.option.contains.forEach(name => {
            let optionName = name;
            let optionValue = item.option[name];
            if(item.type === "consumption" && ((optionName === "HP") || (optionName === "MP"))){
                optionValue = "+" + optionValue + "%";
            }
            let elementLeft = document.getElementById(`item-effect-name`);
            let elementRight = document.getElementById(`item-effect-value`);
            elementLeft.innerHTML += `<li>${optionName}</li>`;
            elementRight.innerHTML += `<li>${optionValue}</li>`;
        })
    }
    haveToFit(putValue, takeValue){
        if(putValue > 0){
            let msg = new Message("DragEvent", game.player.inventory, "fitToHave", putValue);
            game.messageQueue.add(msg);
        }
        let msg = new Message("DragEvent", game.player.inventory, "haveToFit", takeValue);
        game.messageQueue.add(msg);
        msg = new Message(this, game.UI.UIInteraction.inventoryInteraction, "update", {});
        game.messageQueue.add(msg);
        
    }
    fitToHave(takeValue){
        let msg = new Message("DragEvent", game.player.inventory, "fitToHave", takeValue);
        game.messageQueue.add(msg);
        msg = new Message(this, game.UI.UIInteraction.inventoryInteraction, "update", {});
        game.messageQueue.add(msg);
    }
    haveToKey(id, value){
        //inner
        let element = document.getElementById(id);
        let img = `./images/items/consumption/${value}.png`;
        element.setAttribute('value', value);
        element.setAttribute('draggable', true);
        $(`#${id}`).css('background', `url(${img})`);
        
        //keyboard
        let key = element.getAttribute('keyType');
        this.game.player.keyBoard[key] = value;
        
        //screen
        element = document.getElementById(`${id}-amount`);
        element.innerHTML = this.game.player.inventory.getItemAmount(value);
        $(`#${id}-image`).css('background', `url(${img})`);
    }
    keyToHave(id){
        //inner
        let element = document.getElementById(id);
        let img = `./images/items/ui/${id}.png`;
        element.setAttribute('value', -1);
        element.setAttribute('draggable', false);
        $(`#${id}`).css('background', `url(${img})`);

        //keyboard
        let key = element.getAttribute('keyType');
        this.game.player.keyBoard[key] = -1;

        //screen
        element = document.getElementById(`${id}-amount`);
        element.innerHTML = "";
        $(`#${id}-image`).css('background', "");
    }
    onMessage(msg){
        if(msg.type === "update"){
            this.update();
        }
        else if(msg.type === "itemClicked"){
            this.itemClicked(msg.data);
        }
        else if(msg.type === "itemTypeChange"){
            this.itemTypeChange(msg.data.id, msg.data.type);
        }
        else if(msg.type === "haveToFit"){
            this.haveToFit(msg.data.putValue, msg.data.takeValue);
        }
        else if(msg.type === "fitToHave"){
            this.fitToHave(msg.data.takeValue);
        }
        else if(msg.type === "haveToKey"){
            this.haveToKey(msg.data.id, msg.data.value);
        }
        else if(msg.type === "keyToHave"){
            this.keyToHave(msg.data);
        }
    }
}
class UIInteractionSkill{
    constructor(game){
        this.game = game;
    }
    update(){
        this.skillBlockSetUp();
        this.skillKeyBackup();
    }
    skillBlockSetUp(){
        let skillCodeList = this.game.player.skillManager.skillList;
        let skillKey = ['Q','W','E','R','A','S','D','F'];
        for(let i = 0; i < skillCodeList.length; i++){
            if(skillCodeList[i].code > 0){
                $(`#skill-block${i+1}`).css('background', `url(./images/skills/ui/skill${skillCodeList[i].code}.png)`);
                let element = document.getElementById(`skill-block${i+1}`);
                element.setAttribute('value', skillCodeList[i].code);
                if(skillCodeList[i].usage === "active"){
                    element.setAttribute('draggable', true);
                }
            }
        }
    }
    skillKeyBackup(){
        let skillCodeList = this.game.player.keyBoard.skillCodeList;
        skillCodeList.forEach((code, i) => {
            if(code > 0){
                let id = "skill-key" + (i+1);
                this.haveToKey(id, code);
            }  
        })
    }
    skillBlockClicked(code){
        if(code === -1){
            return;
        }
        $(`#skill-detail`).removeClass().addClass("on");
        let skill = this.game.player.skillManager.skillData.getSkill(code);
        let img = `./images/skills/ui/skill${code}.png`
        $(`#skill-detail-img`).css("background", `url(${img})`);
        let element = document.getElementById(`skill-detail-name`);
        element.innerHTML = skill.name;
        element = document.getElementById(`skill-info-top`);
        element.innerHTML = skill.info;
        this.setSkillOption(skill);
    }
    setSkillOption(skill){
        let elementLeft = document.getElementById(`skill-effect-name`);
        let elementRight = document.getElementById(`skill-effect-value`);
        elementLeft.innerHTML = "";
        elementRight.innerHTML = "";
        skill.option.contains.forEach(option => {
            let optionName = option;
            let optionValue = skill.option[option];
            if(optionName === "damage"){
                optionValue = optionValue + "%";
            }
            let elementLeft = document.getElementById(`skill-effect-name`);
            let elementRight = document.getElementById(`skill-effect-value`);
            elementLeft.innerHTML += `<li>${optionName}</li>`;
            elementRight.innerHTML += `<li>${optionValue}</li>`;
        })
    }
    haveToKey(id, value){
        //inner
        let element = document.getElementById(id);
        let img = `./images/skills/ui/key${value}.png`;
        element.setAttribute('value', value);
        element.setAttribute('draggable', true);
        $(`#${id}`).css('background', `url(${img})`);
        
        //keyboard
        let key = element.getAttribute('keyType');
        this.game.player.keyBoard[key] = value;
        
        //screen
        $(`#${id}-image`).css('background', `url(${img})`);
    }
    keyToHave(id){
        //inner
        let element = document.getElementById(id);
        let img = `./images/skills/ui/keys/${this.getKeyById(id)}.png`;
        element.setAttribute('value', -1);
        element.setAttribute('draggable', false);
        $(`#${id}`).css('background', `url(${img})`);

        //keyboard
        let key = element.getAttribute('keyType');
        this.game.player.keyBoard[key] = -1;

        //screen
        $(`#${id}-image`).css('background', "");
    }
    getKeyById(id){
        if(id === "skill-key1"){
            return "Q";
        }
        if(id === "skill-key2"){
            return "W";
        }
        if(id === "skill-key3"){
            return "E";
        }
        if(id === "skill-key4"){
            return "R";
        }
        if(id === "skill-key5"){
            return "A";
        }
        if(id === "skill-key6"){
            return "S";
        }
        if(id === "skill-key7"){
            return "D";
        }
        if(id === "skill-key8"){
            return "F";
        }
        return false;
    }
    castingBuffUpdate(){
        document.getElementById('skill-casting-area').innerHTML = "";
        let skills = this.game.player.skillManager.castingBuffList;
        skills.forEach((skill,i) => {
            let element = document.getElementById('skill-casting-area');
            element.innerHTML += `<div id="casting-${skill.code}"></div>`;
            let img = `./images/skills/ui/castingBuff/${skill.code}.png`;
            $(`#casting-${skill.code}`).css("left", ((10*i) + (40*i))).css("position", "absolute");
            $(`#casting-${skill.code}`).css("width", 40).css("height", 40).css("background", `url("${img}")`);
        })
    }
    onMessage(msg){
        if(msg.type === "skillBlockClicked"){
            this.skillBlockClicked(msg.data);
        }
        else if(msg.type === "haveToKey"){
            this.haveToKey(msg.data.id, msg.data.value);
        }
        else if(msg.type === "keyToHave"){
            this.keyToHave(msg.data);
        }
        else if(msg.type === "castingBuffUpdate"){
            this.castingBuffUpdate();
        }
        else if(msg.type === "update"){
            this.update();
        }
    }
}


class UIInteractionStat{
    constructor(game){
        this.game = game;
    }
    
    statUIUpdate(){
        this.statCurPointCheck();
        this.statHavePointsCheck();
        this.statTotalValueCheck();
        this.statButtonCheck();
    }
    statTotalValueCheck(){
        let inCssTypes = ["HP", "MP", "AD", "CP", "CD", "DEF", "AS"];
        let inStatTypes = ["maxHP", "maxMP", "AD", "CP", "CD", "DEF", "AS"];
        let element;
        for(let i = 0; i < inCssTypes.length; i++){
            element = document.getElementById(`final-${inCssTypes[i]}-value`);
            element.innerHTML = this.game.player.stat[inStatTypes[i]];
        }
    }
    statHavePointsCheck(){
        let haveStatpoints = this.game.player.stat.haveStatpoints;
        document.getElementById('have-stat-point-num').innerHTML = haveStatpoints;
    }
    statCurPointCheck(){
        let statpoints = this.game.player.stat.statpoints;
        let types = ["HP", "MP", "AD", "CP", "CD", "DEF", "AS"];
        let element;
        for(let i = 0; i < types.length; i++){
            element = document.getElementById(`${types[i]}-point-cur`);
            element.innerHTML = statpoints[types[i]].cur;
        }
    }
    statButtonCheck(){
        let stat = this.game.player.stat;
        let haveStatpoints = stat.haveStatpoints;
        let types = ["HP", "MP", "AD", "CP", "CD", "DEF", "AS"];
        for(let i = 0; i < types.length; i++){
            $(`#${types[i]}-point-button`).removeClass('on');
            $(`#${types[i]}-point-button`).addClass('no-click');
        }
        if(haveStatpoints > 0){
            for(let i = 0; i < types.length; i++){
                if(stat.statpoints[types[i]].cur < stat.statpoints[types[i]].max){
                    $(`#${types[i]}-point-button`).addClass('on');
                    $(`#${types[i]}-point-button`).removeClass('no-click');
                }   
            }
        }         
    }
    getStatPoint(){
        $('#stat-button').addClass('hover');
    }
    onMessage(msg){
        if(msg.type === "statUIUpdate"){
            this.statUIUpdate();
        }
        else if(msg.type === "getStatPoint"){
            this.getStatPoint();
        }
    }
}

class UIInteractionQuest{
    constructor(game){
        this.game = game;
    }
    questListSetting(){

        //시작가능 quest first list setting;
        let element = document.getElementById('quest-first-list');
        for(let i = 0; i < 12; i++){
            element.innerHTML += `
            <button id="quest-first-line${i}" class="quest-line no-click" ltype="new" value="-1" onClick="questListClicked(this.id, this.value)"></button>
            `
            $(`#quest-first-line${i}`).css("left", 0).css("top",(i * 40) + (i*3));
        }

        //진행중 quest second list setting;
        element = document.getElementById('quest-second-list');
        for(let i = 0; i < 12; i++){
            element.innerHTML += `
            <button id="quest-second-line${i}" class="quest-line no-click" ltype="inprocess" value="-1" onClick="questListClicked(this.id, this.value)"></button>
            `
            $(`#quest-second-line${i}`).css("left", 0).css("top", (i * 40) + (i*3));
        }
    }
    questUpdate(){
        this.questListReset();
        let totalQuests = this.game.questManager.inprocessQuests;
        let newQuests = totalQuests.filter(q => (q.actionIndex === 0));
        let inprocessQuests = totalQuests.filter(q => (q.actionIndex != 0));
        inprocessQuests.forEach((quest, i)=> {
            let element = document.getElementById(`quest-second-line${i}`);
            $(`#quest-second-line${i}`).removeClass('no-click');
            element.innerHTML = `[${quest.questType}]${quest.name}`;
            element.setAttribute("value", quest.id);
        })

        newQuests.forEach((quest, i)=> {
            let element = document.getElementById(`quest-first-line${i}`);
            $(`#quest-first-line${i}`).removeClass('no-click');
            element.innerHTML = `[${quest.questType}]${quest.name}`;
            element.setAttribute("value", quest.id);
        })
    }
    questListReset(){
        for(let i = 0; i < 8; i++){
            let element = document.getElementById(`quest-second-line${i}`);
            $(`#quest-second-line${i}`).addClass('no-click');
            element.innerHTML = "";
            element.setAttribute("value", -1);

            element = document.getElementById(`quest-first-line${i}`);
            $(`#quest-first-line${i}`).addClass('no-click');
            element.innerHTML = "";
            element.setAttribute("value", -1);
        }
    }
    
    questInfoClicked(id, value){
        $('#quest-detail').addClass('on');
        let questData = this.game.questManager.getInprocessQuest(value);

        $('#quest-detail-img').css('background', `url('./images/UI/quest/npcInfo/${questData.steps[0].id}.png')`);
        let element = document.getElementById('quest-detail-questTitle');
        element.innerHTML = questData.name;  
        let lineType = document.getElementById(id).getAttribute('ltype')
        element = document.getElementById('quest-detail-info');
        if(lineType === "new"){
            element.innerHTML = questData.preInfo;
        }
        else if(lineType === "inprocess"){
            element.innerHTML = questData.info;
            if(questData.requirement){
                //npc이미지
                //오브젝트 이미지
                $('#quest-request-objectImage').css("background", `url(./images/UI/quest/infoImages/${questData.requirement.object}.png)`)
                //오브젝트이름
                element = document.getElementById('quest-request-object');
                if(questData.requirement.type === "kill"){
                    element.innerHTML = this.game.monsterManager.monsterData.monsters[questData.requirement.object].name;
                }
                else if(questData.requirement.type === "collect"){
                    element.innerHTML = this.game.itemManager.itemData.getItem([questData.requirement.object]).name;
                }
                //오브젝트 amount
                element = document.getElementById('quest-request-amount');
                element.innerHTML = `${questData.requirement.process}/${questData.requirement.amount}`;
                //보상
                for(let i = 0; i < questData.award.length; i++){
                    element = document.getElementById(`quest-award-line${i+1}-name`);
                    element.innerHTML = questData.award[i].type;
                    element = document.getElementById(`quest-award-line${i+1}-value`);
                    element.innerHTML = `+${questData.award[i].amount}`;
                }
            }
        }
    }
    
    questInfoUpdate(text){
        let element = document.getElementById('quest-text-area');
        element.innerHTML = text;
    }
    onMessage(msg){
        if(msg.type === "questListSetting"){
            this.questListSetting();
        }
        else if(msg.type === "questUpdate"){
            this.questUpdate();
        }
        else if(msg.type === "questInfoClicked"){
            this.questInfoClicked(msg.data.id, msg.data.value);
        }
        else if(msg.type === "questInfoUpdate"){
            this.questInfoUpdate(msg.data);
        }
    }
}
class UIInteractionText{
    constructor(game){
        this.game = game;
    }

    textListSetting(){
        let nameElement = document.getElementById('info-text-name');
        let valueElement = document.getElementById('info-text-value');

        for(let i = 0; i < 5; i++){
            nameElement.innerHTML += `<div id="text-line-name${i}" class="text-line"></div>`
            $(`#text-line-name${i}`).css('left', 0).css('top', ((4-i)*20) + ((4-i)*2));
            valueElement.innerHTML += `<div id="text-line-value${i}" class="text-line"></div>`
            $(`#text-line-value${i}`).css('left', 0).css('top', ((4-i)*20) + ((4-i)*2));
        }
         
    }
    UIUpdate(infoList){
        for(let i = 0; i < 5; i++){
            if(infoList[i]){
                document.getElementById(`text-line-name${i}`).innerHTML = infoList[i].type;
                document.getElementById(`text-line-value${i}`).innerHTML = infoList[i].value;
            }
            else{
                document.getElementById(`text-line-name${i}`).innerHTML = "";
                document.getElementById(`text-line-value${i}`).innerHTML = "";
            }
        }
    }
    onMessage(msg){
        if(msg.type === "UIUpdate"){
            this.UIUpdate(msg.data);
        }
    }
}

class UIInteractionStore{
    constructor(game){
        this.game = game;
    }
    
    itemListSetting(){
        let element = document.getElementById('store-item-inner-area');
        let num = 0;
        for(let i = 0; i < 8; i++){
            for(let j = 0; j < 6; j++){
                element.innerHTML += `
                <button id="store-item${num}" class="store-item" value="-1" onClick="storeItemClicked(this.id, this.value)">
                </button>
                `
                $(`#store-item${num}`).css('left', (j*70)).css('top', (i*70));
                num++; 
            }
        }
        this.itemListReset();
    }
    itemListReset(){
        let element;
        let img = './images/UI/store/item/empty.png';
        let num = 0;
        for(let i = 0; i < 8; i++){
            for(let j = 0; j < 6; j++){
                element = document.getElementById(`store-item${num}`);
                element.setAttribute('value',-1);
                $(`#store-item${num}`).css('background', `url(${img})`);
                $(`#store-item${num}`).addClass('no-click');
                num++;
            }
        }
    }
    itemListUpdate(items){
        this.itemListReset();
        console.log(items);
        let element;
        let img;
        items.forEach((code, i) => {
            img = `./images/UI/store/item/${code}.png`;
            element = document.getElementById(`store-item${i}`);
            element.setAttribute('value', code);
            $(`#store-item${i}`).css('background', `url(${img})`);
            $(`#store-item${i}`).removeClass('no-click');
        })
    }
    storeIn(items){
        $('#store-detail-buy').addClass('no-click');
        $('#store').addClass('on');
        this.itemListUpdate(items);
    }
    storeOut(){
        $('#store-detail-area').removeClass('on');
        $('#store-detail-buy').addClass('no-click');
        $('#store').removeClass('on');
    }
    itemClicked(id, value){
        document.getElementById('store-detail-top').setAttribute('value', value); // 아이템 id 저장
        $('#store-detail-area').addClass('on');
        $('#store-detail-buy').removeClass('no-click');
        let item = this.game.itemManager.itemData.getItem(value);
        $('#store-detail-image').css('background', `url('./images/UI/store/item/${value}.png')`);
        document.getElementById('store-detail-name').innerHTML = item.name;
        document.getElementById('store-detail-info').innerHTML = item.info;
        document.getElementById('store-detail-price-value').innerHTML = item.sell;
        document.getElementById('store-detail-price-value').setAttribute('value', item.sell);
        //document.getElementById('store-detail-amount-value').setAttribute('value', 1);
        //this.setTotalPrice();
    }
    setTotalPrice(){
        let amount = document.getElementById('store-detail-amount-value').value;
        if(amount > 999){
            $('#store-detail-buy').addClass('no-click');
        }
        else{
            $('#store-detail-buy').removeClass('no-click');
        }
        let price = document.getElementById('store-detail-price-value').getAttribute('value');
        document.getElementById('store-detail-totalPrice-value').innerHTML = amount * price;
        document.getElementById('store-detail-totalPrice-value').setAttribute('value', amount * price);
    }
    itemAmountSet(){
        this.setTotalPrice();
    }
    buyButtonClicked(){
        let price = document.getElementById('store-detail-totalPrice-value').getAttribute('value') * 1;
        let amount = document.getElementById('store-detail-amount-value').value * 1;
        let itemCode = document.getElementById('store-detail-top').getAttribute('value')*1;
        let item = this.game.itemManager.itemData.getItem(itemCode);
        let invenGold = this.game.player.inventory.gold;
        if(invenGold < price){
            let msg = new Message(this, this.game.textManager, "newInfo", {type:"결재 실패", value: "골드부족"});
            this.game.messageQueue.add(msg);
            return;
        }
        let msg = new Message(this, this.game.player.inventory, "buyItem", {id: item.code, amount:amount, price:price});
        this.game.messageQueue.add(msg);
        msg = new Message(this, this.game.textManager, "newInfo", {type:"아이템구매", value: item.name});
        this.game.messageQueue.add(msg);
    }
    onMessage(msg){
        if(msg.type === "storeIn"){
            this.storeIn(msg.data);
        }
        else if(msg.type === "storeOut"){
            this.storeOut(msg.data);
        }
        else if(msg.type === "itemClicked"){
            this.itemClicked(msg.data.id, msg.data.value);
        }
        else if(msg.type === "itemAmountSet"){
            this.itemAmountSet();
        }
        else if(msg.type === "buyButtonClicked"){
            this.buyButtonClicked();
        }
    }
}

class UIInteractionSpc{
    constructor(game){
        this.game = game;
    }
    setting(){
        let element = document.getElementById('spc');
        element.innerHTML = 
        `
        <div id="spc-name">직업선택</div>
        <div id="spc1-name">달무사</div> 
        <div id="spc2-name">듀얼소드마스터</div>    
        <input id="spc1-input" class="spc" name="spc" checked="true" type="radio" value=""/>
        <input id="spc2-input" class="spc" name="spc" type="radio" value=""/>
        <button id="spc-submit" onClick="spcClicked()">
            선택
        </button>

        `
    }
    specialization(spcNames){
        $('#spc').addClass('on');
        document.getElementById('spc1-name').innerHTML = spcNames[0];
        document.getElementById('spc2-name').innerHTML = spcNames[1];
    }
    spcClicked(){
        $('#spc').removeClass('on');
        let elements = document.getElementsByName('spc');      
        let id = -1
        if(elements[0].checked){
            id = 0;
        }
        else{
            id = 1
        }
        let msg = new Message(this, this.game.spcManager, "spcSubmit", id);
        this.game.messageQueue.add(msg);

    }
    onMessage(msg){
        if(msg.type === "spcClicked"){
            this.spcClicked();
        }
        else if(msg.type === "specialization"){
            this.specialization(msg.data);
        }
    }
}

class UIInteractionMiniMap{
    constructor(game){
        this.game = game;
    }
    
    update(name, id){
        let element = document.getElementById('miniMap-name');
        element.innerHTML = name;
        $('#miniMap-image').css('background', `url('./images/UI/map/images/${id}.png')`);
    }
    onMessage(msg){
        if(msg.type === "update"){
            this.update(msg.data.name, msg.data.id);
        }
    }
}