class Storage{
    gameData;
    currentData;
    account;
    character;
    constructor(){
        this.setting();
    }

    setting(){
        this.gameData = JSON.parse(localStorage.getItem("game"));
        console.log(this.gameData);
        this.currentData = this.gameData.currentData;
        this.account = this.gameData.accounts[this.currentData.account];
        this.character = this.account.characters[this.currentData.character];
    }

    saveGameData(){
        localStorage.setItem("game", JSON.stringify(this.gameData));
    }

    printGameData(){
        console.log(this.gameData);
    }
    
    getAccount(){
        return this.account;
    }
    getCharacter(){
        return this.character;
    }
    getInformation(){
        return this.character.information;
    }
    setInformation(info){
        this.character.information = info;
    }
    getMyItem(){
        return this.character.myItem;
    }
    getStone(){
        return this.character.stone;
    }
    getInventory(){
        return this.character.inventory;
    }
    getQuest(){
        return this.character.quest;
    }
    getNPC(){
        return this.character.npc;
    }
   
}