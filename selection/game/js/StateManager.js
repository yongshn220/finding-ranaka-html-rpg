class StateManager{
    stateList
    constructor(game){
        this.game = game;
        this.stateList = {};
        this.setting();
    }

    setting(){
        this.stateList = {
            character : {
                isMoving : false,
                isJumping : false,
                isDelaying : false,
                isLadderOn : false,
                checkStructure : true,
                motion : 0,
            },
            item : {
                checkStructure : true,
            },
            talkManager : {
                isTalking : false,
            },
            monster : {
                info: false,
            },
            store: {
                isOpen: false,
            }
        }
    }

    setState(object, name, state){
        this.stateList[object][name] = state;
    }
    getState(object, name){
        return this.stateList[object][name];
    }


    onMessage(msg){
        if(msg.type === ""){

        }
    }
}