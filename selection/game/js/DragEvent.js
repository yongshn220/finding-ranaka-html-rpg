







function dragEventListener(){
    const dragElements = document.querySelectorAll(".drag");
    dragElements.forEach(element => {
        element.addEventListener("dragstart", dragStart);
        element.addEventListener("dragenter", dragEnter);
        element.addEventListener("dragover", dragOver);
        element.addEventListener("dragleave", dragLeave);
        element.addEventListener("drop", drop);
    });
}

function dragStart(event){
    event.dataTransfer.setData("id", event.target.getAttribute("id"));
    event.dataTransfer.setData("value", event.target.getAttribute("value"));
    event.dataTransfer.setData("drag-type", event.target.getAttribute("drag-type"));
    event.dataTransfer.setData("block-type", event.target.getAttribute("block-type"));
}
function dragEnter(event){
    event.target.classList.add("drop-hover");
}
function dragOver(event){
    event.preventDefault();
}
function dragLeave(event){
    event.target.classList.remove("drop-hover");
}
function drop(event){
    event.preventDefault();
    event.target.classList.remove("drop-hover");
    const takeValue = event.dataTransfer.getData("value");
    const putValue = event.target.getAttribute("value");
    const takeId = event.dataTransfer.getData("id");
    let dropMode = dropModeCheck(event);
    if(dropMode === "haveToFit"){
        msg = new Message("DragEvent", game.UI.UIInteraction.inventoryInteraction, "haveToFit", {putValue: putValue * 1, takeValue: takeValue * 1});
        game.messageQueue.add(msg);
    }
    else if(dropMode === "fitToHave"){
        msg = new Message("DragEvent", game.UI.UIInteraction.inventoryInteraction, "fitToHave", {takeValue: takeValue * 1});
        game.messageQueue.add(msg);
    }
    else if(dropMode === "haveToKey"){
        let putId = event.target.getAttribute('id');
        let msg = new Message("DragEvent", game.UI.UIInteraction.inventoryInteraction, "haveToKey", {id: putId, value: takeValue * 1});
        game.messageQueue.add(msg);
    }
    else if(dropMode === "keyToHave"){
        let msg = new Message("DragEvent", game.UI.UIInteraction.inventoryInteraction, "keyToHave", takeId);
        game.messageQueue.add(msg);
    }
    else if(dropMode === "skillHaveToKey"){
        let putId = event.target.getAttribute('id');
        let msg = new Message("DragEvent", game.UI.UIInteraction.skillInteraction, "haveToKey", {id: putId, value: takeValue * 1});
        game.messageQueue.add(msg);
    }
    else if(dropMode === "skillKeyToHave"){
        let msg = new Message("DragEvent", game.UI.UIInteraction.skillInteraction, "keyToHave", takeId);
        game.messageQueue.add(msg);
    }
}

function dropModeCheck(event){
    const takeDragType = event.dataTransfer.getData("drag-type");
    const takeBlockType = event.dataTransfer.getData("block-type");
    const putDragType = event.target.getAttribute("drag-type");
    const putBlockType = event.target.getAttribute("block-type");
    
    if(takeDragType === putDragType || putDragType === "blank"){
        if(takeDragType === "skill"){
            if(takeBlockType === "have"){
                if(putBlockType === "key"){
                    return "skillHaveToKey";
                }
            }
            else if(takeBlockType === "key"){
                if(putBlockType === "have"){
                    return "skillKeyToHave";
                }
            }
        }
        else{
            if(takeBlockType === "have"){
                if(putBlockType === "fit"){
                    return "haveToFit";
                }
                else if(putBlockType === "key"){
                    return "haveToKey";
                }
            }
            else if(takeBlockType === "fit"){
                if(putBlockType === "have"){
                    return "fitToHave";
                }
            }
            else if(takeBlockType === "key"){
                if(putBlockType === "have"){
                    return "keyToHave";
                }
            }
        }
    }
    else{
        return false;
    }
}
