class UI{
    game;
    states;
    constructor(game){
        this.game =  game;
        this.UIInteraction = new UIInteraction(this.game);
        this.setting();
        this.showUI = new ShowUI(this.game);
    }

    setting(){
        this.UIInteraction.textInteraction.textListSetting();
        this.UIInteraction.spcInteraction.setting();
        this.storeSetting();
        this.questUISetting();
        this.statUISetting();
        this.skillUISetting();
        this.inventoryUISetting();
    }
    storeSetting(){
        let element = document.getElementById('store');
        element.innerHTML = `
        <div id="store-bar">
        </div>
        <div id="store-item-area">
            <div id="store-item-inner-area">
            </div>
        </div>
        <div id="store-detail-area">
            <div id="store-detail-top">
                <div id="store-detail-image">
                </div>
                <div id="store-detail-name">
                </div>
                <div id="store-detail-info">
                </div>
            </div>
            <div id="store-detail-bottom">
                <div id="store-detail-price">
                    <div id="store-detail-price-name">
                    가격
                    </div>
                    <div id="store-detail-price-value">
                    </div>
                </div>
                <div id="store-detail-amount">
                    <div id="store-detail-amount-name">
                    개수 
                    </div>
                    <input id="store-detail-amount-value" type="number" value="0" min="0" max="999" onChange="storeItemAmountSet()" />
                </div>
                <div id="store-detail-totalPrice">
                    <div id="store-detail-totalPrice-name">
                    총가격
                    </div>
                    <div id="store-detail-totalPrice-value">
                    0
                    </div>
                </div>
                <button id="store-detail-buy" onClick="storeBuyButtonClicked()">
                최종결재
                </button>

            </div>
        </div>
        `
        this.UIInteraction.storeInteraction.itemListSetting();
    }

    questUISetting(){
        let element = document.getElementById('quest');
        element.innerHTML = 
        `
        <div id="quest-bar">
        </div>
        <div id="quest-first-section">
            <div id="quest-first-name">
            시작가능
            </div>
            <div id="quest-first-list">
            </div>
        </div>
        <div id="quest-second-section">
            <div id="quest-second-name">
            진행중
            </div>
            <div id="quest-second-list">
            </div>
        </div>
        <div id="quest-detail">
            <div id="quest-detail-name">
            정보
            </div>
            <div id="quest-detail-first">
                <div id="quest-detail-img">
                </div>
                <div id="quest-detail-questTitle">
                </div>
            </div>
            <div id="quest-detail-second">
                <div id="quest-request">
                    <div id="quest-request-name">
                    요구:
                    </div>
                    <div id="quest-request-objectImage">
                    </div>
                    <div id="quest-request-object">
                    </div>
                    <div id="quest-request-amount">
                    </div>
                </div>
                <div id="quest-award">
                    <div id="quest-award-name">
                     보상:
                    </div>
                    <div id="quest-award-line1">
                        <div id="quest-award-line1-name">
                        </div>
                        <div id="quest-award-line1-value">
                        </div>
                    </div>
                    <div id="quest-award-line2">
                        <div id="quest-award-line2-name">
                        </div>
                        <div id="quest-award-line2-value">
                        </div>
                    </div>
                    <div id="quest-award-line3">
                        <div id="quest-award-line3-name">
                        </div>
                        <div id="quest-award-line3-value">
                        </div>
                    </div>
                    <div id="quest-award-line4">
                        <div id="quest-award-line4-name">
                        </div>
                        <div id="quest-award-line4-value">
                        </div>
                    </div>
                </div>
            </div>
            <div id="quest-detail-third">  
                <div id="quest-detail-info">
                </div>
            </div>
            
        </div>
        `
        let msg = new Message(this, this.UIInteraction.questInteraction, "questListSetting", {});
        this.game.messageQueue.add(msg);
    }
    statUISetting(){
        let element = document.getElementById('stat');
        element.innerHTML = 
        `
        <div id="stat-bar">
        </div>
        <div id="stat-left">
            <div id="final-HP">
                <div id="final-HP-name">
                HP
                </div>
                <div id="final-HP-value">
                1321654200
                </div>
            </div>
            <div id="final-MP">
                <div id="final-MP-name">
                MP
                </div>
                <div id="final-MP-value">
                1321654200
                </div>
            </div>
            <div id="final-AD">
                <div id="final-AD-name">
                AD
                </div>
                <div id="final-AD-value">
                1321654200
                </div>
            </div>
            <div id="final-CP">
                <div id="final-CP-name">
                CP
                </div>
                <div id="final-CP-value">
                1321654200
                </div>
            </div>
            <div id="final-CD">
                <div id="final-CD-name">
                CD
                </div>
                <div id="final-CD-value">
                1321654200
                </div>
            </div>
            <div id="final-DEF">
                <div id="final-DEF-name">
                DEF
                </div>
                <div id="final-DEF-value">
                1321654200
                </div>
            </div>
            <div id="final-AS">
                <div id="final-AS-name">
                AS
                </div>
                <div id="final-AS-value">
                1321654200
                </div>
            </div>
        </div>
        <div id="stat-right">
            <div id="stat-right-upper">
            </div>   
            <div id="stat-right-lower">
                <div id="HP-point">
                    <div id="HP-point-name">
                    HP
                    </div>
                    <div id="HP-point-cur">
                    </div>
                    <div id="HP-point-max">
                    5
                    </div>
                    <button id="HP-point-button" value="HP" onClick="statpointClicked(this.value)">
                    </button>
                </div>
                <div id="MP-point">
                    <div id="MP-point-name">
                    MP
                    </div>
                    <div id="MP-point-cur">
                    </div>
                    <div id="MP-point-max">
                    5
                    </div>
                    <button id="MP-point-button" value="MP" onClick="statpointClicked(this.value)">
                    </button>
                </div>
                <div id="AD-point">
                    <div id="AD-point-name">
                    AD
                    </div>
                    <div id="AD-point-cur">
                    </div>
                    <div id="AD-point-max">
                    5
                    </div>
                    <button id="AD-point-button" value="AD" onClick="statpointClicked(this.value)">
                    </button>
                </div>
                <div id="CP-point">
                    <div id="CP-point-name">
                    CP
                    </div>
                    <div id="CP-point-cur">
                    </div>
                    <div id="CP-point-max">
                    5
                    </div>
                    <button id="CP-point-button" value="CP" onClick="statpointClicked(this.value)">
                    </button>
                </div>
                <div id="CD-point">
                    <div id="CD-point-name">
                    CD
                    </div>
                    <div id="CD-point-cur">
                    </div>
                    <div id="CD-point-max">
                    5
                    </div>
                    <button id="CD-point-button" value="CD" onClick="statpointClicked(this.value)">
                    </button>
                </div>
                <div id="DEF-point">
                    <div id="DEF-point-name">
                    DEF
                    </div>
                    <div id="DEF-point-cur">
                    </div>
                    <div id="DEF-point-max">
                    5
                    </div>
                    <button id="DEF-point-button" value="DEF" onClick="statpointClicked(this.value)">
                    </button>
                </div>
                <div id="AS-point">
                    <div id="AS-point-name">
                    AS
                    </div>
                    <div id="AS-point-cur">
                    </div>
                    <div id="AS-point-max">
                    1
                    </div>
                    <button id="AS-point-button" value="AS" onClick="statpointClicked(this.value)">
                    </button>
                </div>

                <div id="have-stat-point">
                    <div id="have-stat-point-num">
                    0
                    </div>
                </div>
            </div>
        </div>
        `
    }
    skillUISetting(){
        let element = document.getElementById('skill');
        element.innerHTML = 
        `
        <div id="skill-bar">
        </div>
        <div id="skill-key">
            <button id="skill-key1" class="drag skill-key" keyType="Q" drag-type="skill" block-type="key" value="-1" draggable="false" onClick="">
            </button>
            <button id="skill-key2" class="drag skill-key" keyType="W" drag-type="skill" block-type="key" value="-1" draggable="false" onClick="">
            </button>
            <button id="skill-key3" class="drag skill-key" keyType="E" drag-type="skill" block-type="key" value="-1" draggable="false" onClick="">
            </button>
            <button id="skill-key4" class="drag skill-key" keyType="R" drag-type="skill" block-type="key" value="-1" draggable="false" onClick="">
            </button>
            <button id="skill-key5" class="drag skill-key" keyType="A" drag-type="skill" block-type="key" value="-1" draggable="false" onClick="">
            </button>
            <button id="skill-key6" class="drag skill-key" keyType="S" drag-type="skill" block-type="key" value="-1" draggable="false" onClick="">
            </button>
            <button id="skill-key7" class="drag skill-key" keyType="D" drag-type="skill" block-type="key" value="-1" draggable="false" onClick="">
            </button>
            <button id="skill-key8" class="drag skill-key" keyType="F" drag-type="skill" block-type="key" value="-1" draggable="false" onClick="">
            </button>
        </div>
        <div id="skill-block">
            <button id="skill-block1" class="drag skill" drag-type="skill" block-type="have" value="-1" draggable="false" onClick="skillBlockClicked(this.value)">
            </button>
            <button id="skill-block2" class="drag skill" drag-type="skill" block-type="have" value="-1" draggable="false" onClick="skillBlockClicked(this.value)">
            </button>
            <button id="skill-block3" class="drag skill" drag-type="skill" block-type="have" value="-1" draggable="false" onClick="skillBlockClicked(this.value)">
            </button>
            <button id="skill-block4" class="drag skill" drag-type="skill" block-type="have" value="-1" draggable="false" onClick="skillBlockClicked(this.value)">
            </button>
            <button id="skill-block5" class="drag skill" drag-type="skill" block-type="have" value="-1" draggable="false" onClick="skillBlockClicked(this.value)">
            </button>
            <button id="skill-block6" class="drag skill" drag-type="skill" block-type="have" value="-1" draggable="false" onClick="skillBlockClicked(this.value)">
            </button>
            <button id="skill-block7" class="drag skill" drag-type="skill" block-type="have" value="-1" draggable="false" onClick="skillBlockClicked(this.value)">
            </button>
            <button id="skill-block8" class="drag skill" drag-type="skill" block-type="have" value="-1" draggable="false" onClick="skillBlockClicked(this.value)">
            </button>
            <button id="skill-block9" class="drag skill" drag-type="skill" block-type="have" value="-1" draggable="false" onClick="skillBlockClicked(this.value)">
            </button>
            <button id="skill-block10" class="drag skill" drag-type="skill" block-type="have" value="-1" draggable="false" onClick="skillBlockClicked(this.value)">
            </button>
        </div>
        <div id="skill-detail">
            <div id="skill-detail-img">
            </div>
            <div id="skill-detail-name">
            </div>
            <div id="skill-detail-info">
                <div id="skill-info-top"> 
                </div>
                <div id="skill-effect">
                    <div id="skill-effect-name">
                    </div>
                    <div id="skill-effect-value">
                    </div>
                </div>
            </div>
        </div>
        `

        element = document.getElementById('skill-casting-area');
        element.innerHTML = 
        `
        <div>
        </div>
        `
    }

    inventoryUISetting(){
        let element = document.getElementById('inventory');
        element.innerHTML =  
        `
        <div id="inventory-bar">
        </div>
        <div id="inventory-key">
            <button id="consumption-key1" class="drag consum-key" keyType="delete" drag-type="consumption" block-type="key" value="-1" draggable="false" onClick="itemClicked(this.value)">
            </button>
            <button id="consumption-key2" class="drag consum-key" keyType="end" drag-type="consumption" block-type="key" value="-1" draggable="false" onClick="itemClicked(this.value)">
            </button>
        </div>
        <div id="equipments">
            <button id="weapon" class="drag equip" drag-type="weapon" block-type="fit" draggable="false" onClick="itemClicked(this.value)">
            </button>
            <button id="helmet" class="drag equip" drag-type="helmet" block-type="fit" draggable="false" onClick="itemClicked(this.value)">
            </button>
            <button id="armor" class="drag equip" drag-type="armor" block-type="fit" draggable="false" onClick="itemClicked(this.value)">
            </button>
            <button id="gloves" class="drag equip" drag-type="gloves" block-type="fit" draggable="false" onClick="itemClicked(this.value)">
            </button>
            <button id="shoes" class="drag equip" drag-type="shoes" block-type="fit" draggable="false" onClick="itemClicked(this.value)">
            </button>
            <button id="belt" class="drag equip" drag-type="belt" block-type="fit" draggable="false" onClick="itemClicked(this.value)">
            </button>
        </div>
        <div id="items">
            <div id="item-type">
                <button id="i-equipment" value="equipment" onClick="itemTypeChange(this.id, this.value)">
                </button>

                <button id="i-consumption" value="consumption" onClick="itemTypeChange(this.id, this.value)">
                
                </button>

                <button id="i-general" value="general" onClick="itemTypeChange(this.id, this.value)">
                
                </button>

                <button id="i-other" value="other" onClick="itemTypeChange(this.id, this.value)">
                
                </button>
            </div>
            <div id="item-data">
                <div id="d-equipment" class="on">
                </div>
                <div id="d-consumption">
                </div>
                <div id="d-general">
                </div>
                <div id="d-other">
                </div>
            </div>
            <div id="gold">
                <a id="gold-text"></a>
            </div>
        </div>
        <div id="inventory-detail">
            <div id="inventory-detail-img">
            </div>
            <div id="inventory-detail-name">
            </div>
            <div id="inventory-detail-info">
                <div id="item-info"> 
                </div>
                <div id="item-effect">
                    <div id="item-effect-name">
                    </div>
                    <div id="item-effect-value">
                    </div>
                </div>
            </div>

        </div>
        `
        this.itemsSetGrid();   
    }
    itemsSetGrid(){
        let element;
        let num;
        let types = ["equipment", "consumption", "general", "other"];
        for(let t = 0; t < types.length; t++){
            num = 0;
            element = document.getElementById(`d-${types[t]}`);
            for(let y = 0; y < 7; y++){
                for(let x = 0; x < 5; x++){
                    element.innerHTML += `<button id="itemType${t}-data${num}" class="drag item-piece" drag-type="blank" block-type="have" value="-1" draggable="false" onClick="itemClicked(this.value)"></button>`
                    $(`#itemType${t}-data${num}`).css("left", x * 70).css("top", y * 70);
                    num++;
                }
            }
        }
    }

    onMessage(msg){
        if(msg.type === "clicked"){
            this.clicked(msg.data);
        }
    }
}


class ShowUI{
    constructor(game){
        this.game = game;
    }

    levelUpdate(){
        this.showStat();
    }

    showStat(){
        let element;
        let stat = this.game.player.stat;
        //HP
        let hp = 400 * (stat.curHP / stat.maxHP);
        $("#hp").css("width", hp);
        element = document.querySelector('#hp');
        element.innerHTML = `${stat.curHP} / ${stat.maxHP}`;
        //MP
        let mp = 400 * (stat.curMP / stat.maxMP);
        $("#mp").css("width", mp);
        element = document.querySelector('#mp');
        element.innerHTML = `${stat.curMP} / ${stat.maxMP}`;
        //LV
        element = document.getElementById('lv');
        element.innerHTML = "LV : " + stat.LV;
        //EXP
        let exp = 1480 * (stat.curEXP / stat.maxEXP);
        $("#exp").css("width", exp);
        element = document.querySelector('#exp a');
        element.innerHTML = `${stat.curEXP} / ${stat.maxEXP}`;
    }
}