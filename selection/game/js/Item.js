class Item extends Entity{
    constructor(game, mData, iData){
        super();
        this.game = game;
        this.mData = mData;
        this.iData = iData;
        this.activation = true;
        this.gravityActivation = true;
        this.checkStructure = true;
        this.setting();
    }

    setting(){
        this.sx = 0;
        this.sy = 0;
        this.sw = 40;
        this.sh = 40;
        this.dx = 0;
        this.dy = 0;
        this.dw = 40;
        this.dh = 40;
        this.RX = 0;
        this.RY = 0;
        this.RW = 40;
        this.RH = 40;
        this.velocity = {x : 0, y: 15};
        this.gx = this.mData.gx;
        this.gy = this.mData.gy;
        this.code = this.mData.code;
        this.img = this.iData.img;
    }

    positionAdjust(){
        let background = this.game.mapManager.curMap.mainBackground;
        this.dx = this.gx - background.sx;
        this.dy = this.gy - background.sy;
    }
    move(x, y){
        this.gx += x;
        this.gy += y;
    }
    gravityDeactivation(){
        this.gravityActivation = false;
    }

    update(){
        this.positionAdjust();
        this.draw();
    }
    imageUpdate(){
        
    }

    eaten(){
        this.activation = false;
    }

    draw(){
        canvas.context.drawImage(this.img,this.sx,this.sy,this.sw,this.sh,this.dx,this.dy,this.dw,this.dh); 
    }
    
    onMessage(msg){
        if(msg.type === "move"){
            this.move(msg.data.x, msg.data.y);
        }
        else if(msg.type === "gravityDeactivate"){
            this.gravityDeactivation();
        }
        else if(msg.type === "eaten"){
            this.eaten();
        }
    }
}

class Gold extends Entity{
    constructor(game, mData){
        super();
        this.game = game;
        this.mData = mData;
        this.value = mData.value;
        this.activation = true;
        this.gravityActivation = true;
        this.checkStructure = true;
        this.setting();
    }

    setting(){
        this.sx = 0;
        this.sy = 0;
        this.sw = 40;
        this.sh = 40;
        this.dx = 0;
        this.dy = 0;
        this.dw = 40;
        this.dh = 40;
        this.RX = 0;
        this.RY = 0;
        this.RW = 40;
        this.RH = 40;
        this.velocity = {x : 0, y: 15};
        this.gx = this.mData.gx;
        this.gy = this.mData.gy;
        this.img = new Image();
        this.img.src = './images/items/other/gold.png'
    }

    positionAdjust(){
        let background = this.game.mapManager.curMap.mainBackground;
        this.dx = this.gx - background.sx;
        this.dy = this.gy - background.sy;
    }
    move(x, y){
        this.gx += x;
        this.gy += y;
    }
    gravityDeactivation(){
        this.gravityActivation = false;
    }

    update(){
        this.positionAdjust();
        this.draw();
    }
    imageUpdate(){
        
    }

    eaten(){
        this.activation = false;
    }

    draw(){
        canvas.context.drawImage(this.img,this.sx,this.sy,this.sw,this.sh,this.dx,this.dy,this.dw,this.dh); 
    }
    
    onMessage(msg){
        if(msg.type === "move"){
            this.move(msg.data.x, msg.data.y);
        }
        else if(msg.type === "gravityDeactivate"){
            this.gravityDeactivation();
        }
        else if(msg.type === "eaten"){
            this.eaten();
        }
    }
}