class DamageManager{
    damageList;
    constructor(game){
        this.game = game;
        this.damageList = [];
    }
    
    levelUpdate(){
        this.damageList.forEach(damage => damage.levelUpdate());
    }
    update(){
        this.damageList.forEach(damage => damage.update());
        this.damageList = this.damageList.filter(damage => damage.activation);
    }

    newDamage(data){
        for(let i = 0; i < data.nums.length; i++){
            let newDamage = new Damage(this.game, data.nums[i], data.criticals[i], data.gx, data.gy);
            newDamage.getUp(i * 30);
            this.damageList.push(newDamage);
        }
    }
    charDamaged(data){
        let newDamage = new Damage(this.game, data.damage, "character", data.gx, data.gy);
        this.damageList.push(newDamage);
    }
    onMessage(msg){
        if(msg.type === "newDamage"){
            this.newDamage(msg.data);
        }
        else if(msg.type === "charDamaged"){
            this.charDamaged(msg.data);
        }
    }
}

class Damage{
    constructor(game, num, critical, gx, gy){
        this.game = game;
        this.timer = 0;
        this.activation = true;
        this.isCritical = critical;
        this.setting(num, gx, gy);
    }
    setting(num, gx, gy){
        this.num = num;
        if(this.isCritical === "character"){
            if(this.num === 0){
                this.num = "miss"
            }
            else{
                this.num = "-" + this.num;
            }
        }
        this.gx = gx;
        this.gy = gy;
        this.rawGy = gy;
    }
    move(){
        this.gy--;
    }
    timeCheck(){
        this.timer += 100;
        if(this.timer >= 500){
            this.activation = false;
        }
    }
    getUp(num){
        this.gy = this.gy - num;
    }
    levelUpdate(){
        this.timeCheck();
    }
    update(){
        this.move();
        this.draw();
    }
    positionAdjust(){
        let background = this.game.mapManager.curMap.mainBackground;
        this.dx = this.gx - background.sx;
        this.dy = this.gy - background.sy;
    }
    draw(){
        this.positionAdjust();
        if(this.isCritical === "character"){
            canvas.context.fillStyle = "purple";
            canvas.context.font = "35px Arial";
        }
        else if(this.isCritical){
            canvas.context.fillStyle = "#ff6e6e";
            canvas.context.font = "40px Arial";
        }
        else{
            canvas.context.fillStyle = "#ffc800";
            canvas.context.font = "30px Arial";
        }
        canvas.context.textAlign = "center";
        canvas.context.fillText(this.num, this.dx, this.dy);
    }
}

