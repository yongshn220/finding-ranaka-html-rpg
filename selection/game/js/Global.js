const screen_width = 1600;
const screen_height = 800;
const char_width = 160;
const char_height = 160;


function getCharacterImage(num){
    let movingImg = new Image();
    let standingImg = new Image();
    let job = Math.floor(num / 100);
    let spc = num % 100;
    movingImg.src = `images/character/job${job}/spc${spc}/char-moving.png`;
    standingImg.src = `images/character/job${job}/spc${spc}/char-standing.png`;
    return [movingImg, standingImg];
}


const characterImages = {
    100: {
        moving: getCharacterImage(100)[0],
        standing: getCharacterImage(100)[1],
    },
    110: {
        moving: getCharacterImage(110)[0],
        standing: getCharacterImage(110)[1],
    },
    111: {
        moving: getCharacterImage(111)[0],
        standing: getCharacterImage(111)[1],
    },
    112: {
        moving: getCharacterImage(112)[0],
        standing: getCharacterImage(112)[1],
    },
    120: {
        moving: getCharacterImage(120)[0],
        standing: getCharacterImage(120)[1],
    },
    121: {
        moving: getCharacterImage(121)[0],
        standing: getCharacterImage(121)[1],
    },
    122: {
        moving: getCharacterImage(122)[0],
        standing: getCharacterImage(122)[1],
    },
}


