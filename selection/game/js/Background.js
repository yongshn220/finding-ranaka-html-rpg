class Background{
    sx;
    sy;
    sw;
    sh;
    dx;
    dy;
    dw;
    dh;
    constructor(width, height, image){
        this.width = width;
        this.height = height;
        this.image = image;
        this.setPosition();
    }

    setPosition(){
        this.sx = 0;
        this.sy = 0;
        this.sw = screen_width;
        this.sh = screen_height;
        this.dx = 0; // always
        this.dy = 0; // always
        this.dw = screen_width;
        this.dh = screen_height;
    }

    move(x, y){
        this.sx += x;
        this.sy += y;
    }

    isEntityInBackground(gx, gy, sw, sh){
        if(gx > 0 &&  gy > 0 && gx < this.width - sw && gy < this.height - sh){
            return true;
        }
        return false;

    }

    onMessage(msg){
        if(msg.type == "move"){
            this.move(msg.data.x, msg.data.y);
        }
        else if(msg.type = ""){

        }
        else{

        }
    }

    update(){
        this.draw();
    }

    draw(){
        canvas.context.drawImage(this.image, this.sx, this.sy, this.sw, this.sh, this.dx, this.dy, this.dw, this.dh);
    }
}