class SkillManager{
    game;
    job;
    specialize;
    skillData;
    castingList;
    delayTime;
    delayState;
    hitEffectList;

    constructor(game){
        this.game = game;
        this.delayTime = 0;
        this.delayState = false;
        this.skillData = new SkillData();
        this.skillList = [];
        this.castingList = [];
        this.castingBuffList = [];
        this.hitEffectList = [];
    }

    backup(info){
        this.skillListUpdate();
    }
    skillListUpdate(){
        this.charCode = this.game.player.information.code;
        this.skillList = this.skillData.getSkillAll(this.charCode);
        this.skillList.forEach(skill => {
            if(skill.usage === "passive"){
                let newSkill = new Skill(this.game, skill);
                this.castingBuffList.push(newSkill);
            }
        })
        let msg = new Message(this, this.game.statManager, "statReset", "");
        this.game.messageQueue.add(msg);
        msg = new Message(this, this.game.UI.UIInteraction.skillInteraction, "castingBuffUpdate", {});
        this.game.messageQueue.add(msg);
        msg = new Message(this, this.game.UI.UIInteraction.skillInteraction, "update", {});
        this.game.messageQueue.add(msg);
    }
    levelUpdate(){
        this.imageUpdate();
        
    }
    imageUpdate(){
        this.castingList.forEach(skill => skill.imageUpdate());
        this.castingList = this.castingList.filter(skill => skill.activation);
        this.castingBuffList.forEach(skill => skill.durationUpdate());
        this.castingBuffList = this.castingBuffList.filter(skill => skill.activation);
        this.hitEffectList.forEach(effect => effect.imageUpdate());
        this.hitEffectList = this.hitEffectList.filter(effect => effect.activation);
        
        if(this.delayState){
            this.delayTimeDecreaseEvent()
        }
    }
    calculateOption(type){
        let totalNum = 100;
        for(let i = 0; i < this.castingBuffList.length; i++){
            let num = this.castingBuffList[i].option[type];
            if(num){
                totalNum += num;
            }
        }
        return totalNum;
    }
    calculateOptionForSkill(){
        let totalAS = 0;
        this.castingBuffList.forEach(buff => {
            if(buff.option.AS){
                totalAS += buff.option.AS;
            }
        })

        return {AS: totalAS};

    }
    delayTimeDecreaseEvent(){
        if(this.delayTime <= 0){
            this.delayState = false;
            let msg = new Message(this, this.game.player.character, "delay", false);
            this.game.messageQueue.add(msg);
        }
        else{
            this.delayTime -= 0.1;
        }
    }

    skill(code){
        let curBuffOption = this.calculateOptionForSkill();
        let curSkillData = this.skillData.getSkill(code);
        if(curSkillData){
            if(this.useSkillValidCheck(curSkillData)){
                let skill = new Skill(this.game, curSkillData);
                this.castingList.push(skill);
                if(skill.type === "buff"){
                    this.addBuff(curSkillData);
                    let msg = new Message(this, this.game.statManager, "statReset", "");
                    this.game.messageQueue.add(msg);
                    msg = new Message(this, this.game.UI.UIInteraction.skillInteraction, "castingBuffUpdate", {});
                    this.game.messageQueue.add(msg);
                }
            }
            else{
                return;
            }
        }
        this.delayTime = curSkillData.delayTime - (curSkillData.delayTime*(curBuffOption.AS/100));
        this.delayState = true;
        this.charImgChange(curSkillData);
    }
    addBuff(skillData){
        for(let i = 0; i < this.castingBuffList.length; i++){
            if(this.castingBuffList[i].code === skillData.code){
                this.castingBuffList[i].reactivate();
                return true;
            }
        }
        let newBuff = new Skill(this.game, skillData);
        this.castingBuffList.push(newBuff);
    }
    
    useSkillValidCheck(data){
        if(!this.MPDecrease(data)){
            return false;
        }
        if(this.game.stateManager.getState("character", "isLadderOn")){
            return false;
        }
        return true;
    }
    MPDecrease(data){
        if(this.game.player.stat.curMP < data.option.MPDeduct){
            return false;
        }
        else{
            let msg = new Message(this, this.game.player.stat, "MPDecrease", data.option.MPDeduct);
            this.game.messageQueue.add(msg);
            return true;
        } 
    }
    charImgChange(data){
        let img = data.charImg;
        let frame = data.charFrame
        let msg = new Message(this, this.game.player.character, "delay", {state: true, frame: frame, img: img});
        this.game.messageQueue.add(msg);
    }
    movingSkillHit(){
        this.castingList.some(skill => {
            if(skill.movingState){
                if(skill.hitState){
                    this.hitEvent(skill); 
                }
                return true;
            }
        })
    }
    skillHit(){
        this.castingList.forEach(skill => {
            if(skill.type === "attack"){
                if(skill.hitState){
                    this.hitEvent(skill);
                }
            }
        })
    }
    hitEvent(skill){
        let monsters = this.game.monsterManager.monsterList;
        monsters = monsters.filter(m => this.isCollid(skill, m)); // 충돌되지 않은 몬스터들 filter
        monsters = monsters.splice(0,skill.maxObjects); // 최대 공격 가능 몬스터 수 만큼 splice
        monsters.forEach(monster => {
            skill.hitState = false;
            let damageData = this.getDamage(skill);
            let msg = new Message(this, monster, "hit", damageData);
            this.game.messageQueue.add(msg);
            this.hitEffect(skill.hitEffect, monster);
        })
    }
    getDamage(skill){
        let damages = [];
        let criticals = [];
        for(let i = 0; i < skill.maxHits; i++){
            let rNum = (Math.random() * 10) - 5
            let damage = this.game.player.stat.AD * (skill.damage / 100);
            let newDamage = damage + (damage * (rNum / 100));
            rNum = Math.random();
            if(this.game.player.stat.CP >= rNum * 100){
                criticals.push(true);
            }
            else{
                criticals.push(false);
            }
            damages.push(Math.floor(newDamage));
        }
        return {damages: damages, criticals: criticals};
    }

    hitEffect(effect, monster){
        let newEffect = new HitEffect(this.game, effect, monster);
        this.hitEffectList.push(newEffect);
    }
    
    isCollid(a, b){
        if((a.gx <= (b.gx + b.sw) && (a.gx + a.sw) >= b.gx) && a.gy <= (b.gy + b.sh) && (a.gy + a.sh) >= b.gy){ 
            return true;
        }
        return false;
    }

    onMessage(msg){
        if(msg.type === "skill"){
            this.skill(msg.data);
        }
    }
    update(){
        this.castingList.forEach(skill => skill.update());
        this.hitEffectList.forEach(effect => effect.update());
        this.skillHit();
        this.movingSkillHit();
    }
}