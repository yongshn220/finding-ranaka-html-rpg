class Stat{
    constructor(game){
        this.game = game;
        this.timer = 0;
        this.state = "normal";
    }

    backup(stat){
        this.LV = stat.LV;
        this.curEXP = stat.EXP;
        this.maxEXP = this.game.statManager.statData.dataList[this.LV].EXP;
        this.curHP = stat.curHP;
        this.curMP = stat.curMP;
        this.maxHP = stat.maxHP;
        this.maxMP = stat.maxMP;
        this.AD = stat.AD;
        this.DEF = stat.DEF;
        this.CP = stat.CP;
        this.CD = stat.CD;
        this.AS = stat.AS;
        this.moveSpeed = stat.moveSpeed;
        this.statpoints = stat.statpoints;
        this.haveStatpoints = stat.haveStatpoints;
        this.statReset("backup");
    }
    
    getStatpoint(type){
        let num = 5;
        if(type === "CP"){
            num = 100;
        }
        return 100 + (this.statpoints[type].cur * num);
    }

    save(stat){
        stat.LV = this.LV;
        stat.EXP = this.curEXP;
        stat.curHP = this.curHP;
        stat.curMP = this.curMP;
        stat.maxHP = this.maxHP;
        stat.maxMP = this.maxMP;
        stat.AD = this.AD;
        stat.DEF = this.DEF;
        stat.CP = this.CP;
        stat.CD = this.CD;
        stat.AS = this.AS;
        stat.statpoints = this.statpoints;
        stat.haveStatpoints = this.haveStatpoints;
    }

    update(){
        //console.log("AD : " +this.AD);
    }
    levelUpdate(){
        this.resting();
    }

    resting(){
         //this.HPIncrease(100);
         //this.MPIncrease(100);
    }

    HPIncrease(num){
        this.curHP += num;
        if(this.curHP > this.maxHP){
            this.curHP = this.maxHP;
        }
    }
    MPIncrease(num){
        this.curMP += num;
        if(this.curMP > this.maxMP){
            this.curMP = this.maxMP;
        }
    }

    damaged(AD){ 
        if(!this.game.timeManager.getTimer("charAttacked").state){
            this.timer = 0;
            let newDamage = AD - this.DEF
            if(newDamage <= 0){
                newDamage = 0;   
            }
            else{
                this.curHP -= newDamage;
                this.HPCheck();
            }
            let data = {damage: newDamage, gx: this.game.player.character.gx+80, gy: this.game.player.character.gy+20,}
            let msg = new Message(this, this.game.damageManager, "charDamaged", data);
            this.game.messageQueue.add(msg);
        }
        let msg = new Message(this, this.game.timeManager, "timerOn", "charAttacked");
        this.game.timeManager.onMessage(msg);         
    }

    HPCheck(){
        if(this.curHP <= 0){
            this.curHP = this.maxHP;
            alert("noHP");
        }
    }

    MPDecrease(MP){
        this.curMP -= MP;
        if(this.curMP < 0){
            this.curMP = 0;
            this.MPCheck();
        }
    }
    getEXP(amount){
        this.curEXP += amount;
        if(this.curEXP >= this.maxEXP){
            while(this.curEXP >= this.maxEXP){
                this.LV++;
                this.curEXP = this.curEXP - this.maxEXP;
                this.maxEXP = this.game.statManager.statData.dataList[this.LV].EXP;
                if(this.LV % 5 === 0){
                    this.haveStatpoints++;
                    let msg = new Message(this, this.game.UI.UIInteraction.statInteraction, "getStatPoint", {});
                    this.game.messageQueue.add(msg);
                }
            }
            this.statReset("levelUP");
            let msg = new Message(this, this.game.questManager, "newQuestCheck", {});
            this.game.messageQueue.add(msg);
        }
    }
    statReset(by){
        let msg = new Message(this, this.game.statManager, "statReset", by);
        this.game.messageQueue.add(msg);
    }

    newStat(newStat, by){
        this.maxHP = newStat.HP;
        this.maxMP = newStat.MP;
        this.AD = newStat.AD;
        this.CP = newStat.CP;
        this.CD = newStat.CD;
        this.DEF = newStat.DEF;
        this.AS = newStat.AS;
        if(by === "levelUP"){
            this.curHP = this.maxHP;
            this.curMP = this.maxMP;
        }
    }

    consumeItem(item){
        this.HPIncrease(Math.floor(this.maxHP * (item.option.HP/100)));
        this.MPIncrease(Math.floor(this.maxMP * (item.option.MP/100)));
    }
    onMessage(msg){
        if(msg.type === "damaged"){
            this.damaged(msg.data);
        }
        else if(msg.type === "MPDecrease"){
            this.MPDecrease(msg.data);
        }
        else if(msg.type === "getEXP"){
            this.getEXP(msg.data);
        }
        else if(msg.type === "newStat"){
            this.newStat(msg.data.stat, msg.data.by);
        }
        else if(msg.type === "consumeItem"){
            this.consumeItem(msg.data);
        }
    }
}