class Inventory{
    items;
    fitItems;
    stones;
    gold;
    constructor(game){
        this.game = game;
        this.gold = 0;
        this.items = [];
        this.fitItems = [];
        this.stones;
    }
    
    newItem(code, amount){
        let alreadyHave = false;
        for(let i = 0; i < this.items.length; i++){
            if(this.items[i].code === code){
                this.items[i].amount += amount;
                alreadyHave = true;
            }
        }
        if(!alreadyHave){
            this.items.push({code: code, amount: 1, type: this.getType(code)});
        }
        let msg = new Message(this, this.game.questManager, "questCheck", {type: "collect", id: code});
        this.game.messageQueue.add(msg);
        this.doUpdate();
    }
    getType(code){
        let newCode = code - (code % 100);
        if(newCode === 1100){
            return "weapon";
        }
        else if(newCode === 1200){
            return "helmet";
        }
        else if(newCode === 1300){
            return "armor"
        }
        else if(newCode === 1400){
            return "gloves"
        }
        else if(newCode === 1500){
            return "shoes"
        }
        else if(newCode === 1600){
            return "belt"
        }
        else if(newCode === 2000){
            return "consumption";
        }
        else if(newCode === 3000){
            return "general";
        }
        else{
            return "other";
        }
    }
    newGold(value){
        this.gold += value;
        this.doUpdate();
    }
    haveToFit(id){
        this.items.forEach((item, i) => {
            if(item.code === id){
                this.fitItems.push(item);
                this.items.splice(i, 1);
            }
        })
        let msg = new Message(this, this.game.statManager, "statReset", {});
        this.game.messageQueue.add(msg);
    }
    fitToHave(id){
        this.fitItems.forEach((fitItem, i) => {
            if(fitItem.code === id){
                this.items.push(fitItem);
                this.fitItems.splice(i, 1);
            }
        })
        let msg = new Message(this, this.game.statManager, "statReset", {});
        this.game.messageQueue.add(msg);
    }

    consume(code){
        this.items.forEach((item, i) => {
            if(item.code === code){
                item.amount--;
                if(item.amount <= 0){
                    this.items.splice(i, 1);
                }
                let consumeItem = this.game.itemManager.itemData.getItem(code);
                let msg = new Message(this, this.game.player.stat, "consumeItem", consumeItem);
                this.game.messageQueue.add(msg);
                msg = new Message(this, this.game.UI.UIInteraction.inventoryInteraction, "update",  {});
                this.game.messageQueue.add(msg);
                return;
            }
        })
    }
    getItemAmount(code){
        for(let i = 0 ; i < this.items.length; i++){
            if(this.items[i].code === code){
                return this.items[i].amount;
            }
        }
        return false;
    }

    calculateOption(type){
        let totalNum = 0;
        for(let i = 0; i < this.fitItems.length; i++){
            let itemData = this.game.itemManager.itemData.getItem(this.fitItems[i].code);
            let num = itemData.option[type];
            if(num){
                totalNum += num; 
            }
        }
        return totalNum;
    }
    buyItem(id, amount, price){
        this.gold -= price;
        this.newItem(id, amount);
    }

    onMessage(msg){
        if(msg.type === "newItem"){
            this.newItem(msg.data.id, msg.data.amount);
        }
        else if(msg.type === "newGold"){
            this.newGold(msg.data);
        }
        else if(msg.type === "haveToFit"){
            this.haveToFit(msg.data);
        }
        else if(msg.type === "fitToHave"){
            this.fitToHave(msg.data);
        }
        else if(msg.type === "consume"){
            this.consume(msg.data);
        }
        else if(msg.type === "buyItem"){
            this.buyItem(msg.data.id, msg.data.amount, msg.data.price);
        }
    }
    doUpdate(){
        let msg = new Message(this, this.game.UI.UIInteraction.inventoryInteraction, "update", {});
        this.game.messageQueue.add(msg);
    }

    backup(inventory){
        this.items = inventory.have;
        this.fitItems = inventory.fit;
        this.gold = inventory.gold
        this.doUpdate();
    }

    save(inventory){
        inventory.have = this.items;
        inventory.fit = this.fitItems;
        inventory.gold = this.gold;
    }
}

class OwnItem{
    constructor(game, data){
        this.game = game;
        this.data = data;
        this.setting();
    }

    setting(){
        this.code = this.data.code;
        this.type = this.data.type;
        this.info = this.data.info;
        this.wearable = this.data.wearable;
        this.useable = this.data.useable;
        this.sell = this.data.sell;
        this.option = this.data.option; 
        this.img = this.data.img;
        this.sortType(); 
    }

    sortType(){
        let n = Math.floor(this.code / 1000);
        if(n === 1){
            this.sortType = "equipment";
        }
        else if(n === 2){
            this.sortType = "consumption";
        }
        else if(n === 3){
            this.sortType = "general";
        }
    }
}
