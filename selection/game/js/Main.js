

const game = new Game();
const backup = new Backup(game);
const canvas = new Canvas("");

$(document).ready(function(){

    addEventListener(game);
    dragEventListener(game);
    animate();
})

saveTime = {
    start : 0,
    elapsed : 0,
    level : 5000,
}


function animate(now = 0){
    canvas.clearCanvas();
    saveTime.elapsed = now - saveTime.start;
    save(now);
    game.update(now);
    requestId = requestAnimationFrame(animate);
}

function save(now){
    if(saveTime.elapsed >= saveTime.level){
        backup.save();
        saveTime.start = now;
    }
}

function UIButtonEvents(val){
    let msg = new Message("main", game.UI.UIInteraction, "clicked", val)
    game.messageQueue.add(msg);
}

function itemTypeChange(id, type){
    let msg = new Message("main", game.UI.UIInteraction.inventoryInteraction, "itemTypeChange", {id: id, type: type});
    game.messageQueue.add(msg);
}

function itemClicked(val){
    val = val * 1;
    let msg = new Message("main", game.UI.UIInteraction.inventoryInteraction, "itemClicked", val)
    game.messageQueue.add(msg);
}

function skillBlockClicked(val){
    val = val * 1;
    let msg = new Message("main", game.UI.UIInteraction.skillInteraction, "skillBlockClicked", val)
    game.messageQueue.add(msg);
}
function statpointClicked(val){
    let msg = new Message("main", game.statManager, "statpointClicked", val);
    game.messageQueue.add(msg);
}
function questListClicked(id, val){
    if(val*1 < 0){
        return;
    }
    let msg = new Message("main", game.UI.UIInteraction.questInteraction, "questInfoClicked", {id: id, value: val*1});
    game.messageQueue.add(msg);
}

function storeItemClicked(id, val){
    let msg = new Message("main", game.UI.UIInteraction.storeInteraction, "itemClicked", {id: id, value: val*1})
    game.messageQueue.add(msg);
}
function storeItemAmountSet(){
    let msg = new Message("main", game.UI.UIInteraction.storeInteraction, "itemAmountSet", {});
    game.messageQueue.add(msg);
}
function storeBuyButtonClicked(){
    let msg = new Message("main", game.UI.UIInteraction.storeInteraction, "buyButtonClicked", {});
    game.messageQueue.add(msg);
}
function spcClicked(){
    let msg = new Message("main", game.UI.UIInteraction.spcInteraction, "spcClicked", {});
    game.messageQueue.add(msg);
}