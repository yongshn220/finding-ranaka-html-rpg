class Player{
    game;
    character;
    information;
    stat;
    inventory;
    quest;
    skillManager;
    keyBoard;

    constructor(game){
        this.game = game; 
        this.information = {
            job : false,
            specialize : false,
            code: false,
        }
        this.character = new Character(game);
        this.stat = new Stat(game);
        this.skillManager = new SkillManager(game);
        this.inventory = new Inventory(game);
        this.keyBoard = new KeyBoard();
    }
    
    backup(info){
        this.information.job = info.job;
        this.information.specialize = info.specialize;
        this.information.code = info.code;
    }
    save(info){
        info.job = this.information.job;
        info.specialize = this.information.specialize;
        info.code = this.information.code;
    }
    
}