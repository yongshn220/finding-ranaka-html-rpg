class MapData{
    infoList;
    structureList;
    baseImageList;
    mainImageList;
    constructor(game){
        this.game = game;
        this.infoList =  {};
        this.structureList = {};
        this.baseImageList = {};
        this.mainImageList = {};
        this.structureSetting();
        this.informationSetting();
        this.imageSetting();
    }
    imageSetting(){
        let numOfMaps = [14,5,7,8,9]; // num of maps in each chapter;
        let baseImg;
        let middleImg;
        let mainImg;
        let mapCode;
        for(let chapN = 0; chapN < 2; chapN++){
            for(let mapN = 0; mapN < numOfMaps[chapN]; mapN++){
                baseImg = new Image();
                //middleImg = new Image();
                mainImg = new Image();
                mapCode = this.getMapCode(chapN+1, mapN)

                baseImg.src = `./images/maps/chapter${chapN+1}/base/${mapCode}.png`;
                mainImg.src = `./images/maps/chapter${chapN+1}/main/${mapCode}.png`;

                this.baseImageList[mapCode] = baseImg;
                this.mainImageList[mapCode] = mainImg;
            }
        }
        
    }

    getMapCode(chapter, map){
        let key = ""
        if(map < 10){
            key = chapter + "0" + map;
        }
        else{
            key = chapter + "" + map;
        } 
        return key;
    }

    getStructureImg(id){
        let img = new Image();
        img.src = `./images/structures/${id}.png`;
        return img;
    }

    informationSetting(){
        this.infoList[100] = {
                name: "나의 집",
                width: 1600,
                height: 1000,
                structure: this.structureList[100],
                monster : false,
        };
        this.infoList[101] = {
                name: "스콜라스 학교 가는길",
                width: 1600,
                height: 800,
                structure: this.structureList[101],
                monster : false,
        };
        this.infoList[102] = {
                code: 102,
                name: "스콜라스 학교",
                width: 5000,
                height: 1000,
                structure: this.structureList[102],
                monster : false,
        };
        this.infoList[103] = {
                code: 103,
                name: "학교 거리1",
                width: 2400,
                height: 1000,
                structure: this.structureList[103],
                monster: {
                    code: [1],
                    maxNum : 20,
                    pos : [
                        {minGx: 200, maxGx: 1000, Gy: 200},
                        {minGx: 200, maxGx: 1000, Gy: 360},
                        {minGx: 1300, maxGx: 1700, Gy: 480},
                        {minGx: 1300, maxGx: 1700, Gy: 40},
                        {minGx: 10, maxGx: 2200, Gy: 719},
                    ],
                }
        };
        this.infoList[104] = {
                code: 104,
                name: "학교 거리2",
                width: 2400,
                height: 1000,
                structure: this.structureList[104],
                monster : {
                    code: [2],
                    maxNum : 20,
                    pos : [
                        {minGx: 810, maxGx: 1924, Gy: 499},
                        {minGx: 810, maxGx: 1924, Gy: 299},
                        {minGx: 810, maxGx: 1924, Gy: 79},
                        {minGx: 10, maxGx: 2200, Gy: 739},
                    ],
                }
        };
        this.infoList[105] = {
                code: 105,
                name: "대강당",
                width: 1600,
                height: 800,
                structure: this.structureList[105],
                monster : false,
        };
        this.infoList[106] = {
                code: 106,
                name: "학교 복도",
                width: 5000,
                height: 1000,
                structure: this.structureList[106],
                monster : false,
        };
        this.infoList[107] = {
                code: 107,
                name: "C등급 반",
                width: 1600,
                height: 800,
                structure: this.structureList[107],
                monster : {
                    code: [3],
                    maxNum : 10,
                    pos : [{minGx: 10, maxGx: 1500, Gy: 0}],
                }
        };
        this.infoList[108] = {
                code: 108,
                name: "B등급 반",
                width: 1600,
                height: 800,
                structure: this.structureList[108],
                monster : {
                    code: [4],
                    maxNum : 10,
                    pos : [{minGx: 10, maxGx: 1500, Gy: 0}],
                }
        };
        this.infoList[109] = {
                code: 109,
                name: "A등급 반",
                width: 1600,
                height: 800,
                structure: this.structureList[109],
                monster : {
                    code: [7],
                    maxNum : 1,
                    pos : [{minGx: 10, maxGx: 1500, Gy: 0}],
                }
        };
        this.infoList[[110]] = {
                code: 110,
                name: "학교 뒷마당1",
                width: 2400,
                height: 1000,
                structure: this.structureList[110],
                monster : {
                    code: [5],
                    maxNum : 20,
                    pos : [
                        {minGx: 610, maxGx: 1600, Gy: 519},
                        {minGx: 780, maxGx: 1500, Gy: 239},
                        {minGx: 10, maxGx: 2200, Gy: 739},
                    ],
                } 
        };
        this.infoList[111] = {
                code: 111,
                name: "학교 뒷마당2",
                width: 2400,
                height: 1000,
                structure: this.structureList[111],
                monster : {
                    code: [6],
                    maxNum : 20,
                    pos : [
                        {minGx: 170, maxGx: 630, Gy: 359},
                        {minGx: 856, maxGx: 1300, Gy: 359},
                        {minGx: 1590, maxGx: 2000, Gy: 359},
                        {minGx: 170, maxGx: 2000, Gy: 79},
                        {minGx: 10, maxGx: 2200, Gy: 719},
                    ],
                }
        };
        this.infoList[112] = {
                code: 112,
                name: "계약의 성소",
                width: 1600,
                height: 800,
                structure: this.structureList[112],
                monster : false,
        };
        this.infoList[113] = {
                code: 113,
                name: "새내기 반",
                width: 1600,
                height: 800,
                structure: this.structureList[113],
                monster : false,
        };
        this.infoList[200] = {
                code: 200,
                name: "엘린버드캠프 가는 길1",
                width: 2400,
                height: 1000,
                structure: this.structureList[200],
                monster : {
                    code: [8],
                    maxNum : 10,
                    pos : [{minGx: 10, maxGx: 1400, Gy: 0}],
                }
        };
        this.infoList[201] = {
                code: 201,
                name: "엘린버드캠프 가는 길2",
                width: 2400,
                height: 1000,
                structure: this.structureList[201],
                monster : {
                    code: [9],
                    maxNum : 10,
                    pos : [{minGx: 10, maxGx: 1400, Gy: 0}],
                }
        };
        this.infoList[202] = {
                code: 202,
                name: "엘린버드캠프",
                width: 3000,
                height: 1000,
                structure: this.structureList[202],
                monster : false,
        };
        this.infoList[203] = {
                code: 203,
                name: "울창한숲1",
                width: 2400,
                height: 1000,
                structure: this.structureList[203],
                monster : {
                    code: [10],
                    maxNum : 10,
                    pos : [{minGx: 10, maxGx: 1400, Gy: 0}],
                }
        };
        this.infoList[204] = {
                code: 204,
                name: "울창한숲2",
                width: 2400,
                height: 1000,
                structure: this.structureList[204],
                monster : false,
        };
        
 
    }
    structureSetting(){
        //100 나의 집
        let chap1map0 = [
            { 
                type: "wall",
                sw: 1600,
                sh: 120,
                gx: 0,
                gy: 880,
                portalData: false,
            },
            { 
                type: "wall",
                sw: 400,
                sh: 1000,
                gx: 0,
                gy: 0,
                portalData: false,
            },
            { 
                type: "wall",
                sw: 400,
                sh: 1000,
                gx: 1200,
                gy: 680,
                portalData: false,
            },
            {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 440,
               gy: 800,
               portalData: {
                   to: 101, //학교가는길
                   pos: {x: 1400,y: 519},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
        ]

        //101 스콜라스 학교 가는 길
       let chap1map1 = [
            {
               type: "wall",
               sw: 1600,
               sh: 120,
               gx: 0,
               gy: 680,
               portalData: false,
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 40,
               gy: 600,
               portalData: {
                   to: 102, // 학교
                   pos: {x: 4814, y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 1480,
               gy: 600,
               portalData: {
                   to: 100,
                   pos: {x: 480, y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
       ]

       //102 스콜라스 학교
       let chap1map2 = [
            {
               type: "wall",
               sw: 5000,
               sh: 120,
               gx: 0,
               gy: 880,
           },
           {   
               name: "나즈리엘", 
               type: "npc",
               id: 102000,
               sw: 160,
               sh: 160,
               gx: 4200,
               gy: 761,
               img: this.getStructureImg(102000),      
           },
           
           {   
               name: "게시판", 
               type: "npc",
               id: 108000,
               sw: 120,
               sh: 200,
               gx: 1580,
               gy: 708,
               img: this.getStructureImg(108000),      
           },
           {   
               name: "소비상점", 
               type: "store",
               subType: "consumption",
               items:[2001,2002],
               id: 598000,
               sw: 160,
               sh: 160,
               gx: 400,
               gy: 720,
               img: this.getStructureImg(598000),      
           },
           {   
               name: "장비상점", 
               type: "store",
               subType: "equipment",
               items: [],
               id: 599000,
               sw: 160,
               sh: 160,
               gx: 600,
               gy: 720,
               img: this.getStructureImg(599000),      
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 40,
               gy: 800,
               portalData: {
                   to: 103, // 학교 거리1
                   pos: {x: 2240, y: 719},
                   access: true,
               },
               img: this.getStructureImg(0),  
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 760,
               gy: 800,
               portalData: {
                   to: 110, // 뒷마당1
                   pos: {x: 0,y: 719 },
                   access: true,
               },
               img: this.getStructureImg(0),  
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 2440,
               gy: 800,
               portalData: {
                   to: 106, //학교 복도
                   pos: {x: 0, y: 719},
                   access: true,
               },
               img: this.getStructureImg(0),  
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 4880,
               gy: 800,
               portalData: {
                   to: 101, // 학교가는 길
                   pos: {x: 10, y: 519},
                   access: true,
               },
               img: this.getStructureImg(0),  
           },
       ]
      
        
        //학교거리1
       let chap1map3 = [
            {
               type: "wall",
               sw: 2400,
               sh: 120,
               gx: 0,
               gy: 880,
           },
           {
               type: "wall",
               sw: 1000,
               sh: 40,
               gx: 240,
               gy: 320,
           },
           {
               type: "wall",
               sw: 1000,
               sh: 40,
               gx: 240,
               gy: 520,
           },
           {
               type: "wall",
               sw: 520,
               sh: 40,
               gx: 1360,
               gy: 200,
           },
           {
               type: "wall",
               sw: 520,
               sh: 40,
               gx: 1360,
               gy: 640,
           },
           {
               type: "ladder",
               sw: 10,
               sh: 200,
               gx: 1760,
               gy: 640,
           },
           {
               type: "ladder",
               sw: 10,
               sh: 150,
               gx: 360,
               gy: 320,
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 40,
               gy: 800,
               portalData: {
                   to: 104, // 학교거리2
                   pos: {x: 2240, y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 2280,
               gy: 800,
               portalData: {
                   to: 102, //학교
                   pos: {x: 10, y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
       ]

       //학교거리2
       let chap1map4 = [
            {
               type: "wall",
               sw: 2400,
               sh: 120,
               gx: 0,
               gy: 880,
               portalData: false,
           },
           {
               type: "wall",
               sw: 1240,
               sh: 40,
               gx: 800,
               gy: 240,
               portalData: false,
           },
           {
               type: "wall",
               sw: 1240,
               sh: 40,
               gx: 800,
               gy: 440,
               portalData: false,
           },
           {
               type: "wall",
               sw: 1240,
               sh: 80,
               gx: 800,
               gy: 640,
               portalData: false,
           },
           {   
               name: "분리수거함", 
               type: "npc",
               id: 103000,
               sw: 160,
               sh: 160,
               gx: 1200,
               gy: 720,
               img: this.getStructureImg(103000),      
           },
           {
               type: "ladder",
               sw: 10,
               sh: 200,
               gx: 1960,
               gy: 640,
           },
           {
               type: "ladder",
               sw: 10,
               sh: 150,
               gx: 880,
               gy: 440,
           },
           {
               type: "ladder",
               sw: 10,
               sh: 150,
               gx: 1960,
               gy: 240,
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 40,
               gy: 800,
               portalData: {
                   to: 105, // 대강당
                   pos: {x: 1480,y: 519},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 2280,
               gy: 800,
               portalData: {
                   to: 103, //학교 거리 1
                   pos: {x: 10, y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
       ]

       //대강당
       let chap1map5 = [
            {
               type: "wall",
               sw: 1600,
               sh: 120,
               gx: 0,
               gy: 680,
               portalData: false,
           },
           {   
               name: "아바나", 
               type: "npc",
               id: 104000,
               sw: 160,
               sh: 160,
               gx: 400,
               gy: 559,
               img: this.getStructureImg(104000),      
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 1480,
               gy: 600,
               portalData: {
                   to: 104, //학교 거리 1
                   pos: {x: 10, y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
       ]

       //학교복도
       let chap1map6 = [
            {
               type: "wall",
               sw: 5000,
               sh: 120,
               gx: 0,
               gy: 880,
               portalData: false,
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 40,
               gy: 800,
               portalData: {
                   to: 102, //학교
                   pos: {x: 2400, y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
           {
               type: "portal",
               sw: 120,
               sh: 80,
               gx: 620,
               gy: 800,
               portalData: {
                   to: 113, //새내기
                   pos: {x: 10, y: 519},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
           {
               type: "portal",
               sw: 120,
               sh: 80,
               gx: 1600,
               gy: 800,
               portalData: {
                   to: 107, //c등급반
                   pos: {x: 10, y: 519},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
           {
               type: "portal",
               sw: 120,
               sh: 80,
               gx: 2540,
               gy: 800,
               portalData: {
                   to: 108, //B등급
                   pos: {x: 10, y: 519},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
           {
               type: "portal",
               sw: 120,
               sh: 80,
               gx: 4750,
               gy: 800,
               portalData: {
                   to: 109, // A등급
                   pos: {x: 10, y: 519},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
       ]

       //C등급반
       let chap1map7 = [
            {
               type: "wall",
               sw: 1600,
               sh: 120,
               gx: 0,
               gy: 680,
               portalData: false,
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 40,
               gy: 600,
               portalData: {
                   to: 106, //학교 복도
                   pos: {x: 1600, y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
       ]
       //B등급반
       let chap1map8 = [
            {
               type: "wall",
               sw: 1600,
               sh: 120,
               gx: 0,
               gy: 680,
               portalData: false,
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 40,
               gy: 600,
               portalData: {
                   to: 106, //학교 복도
                   pos: {x: 2500, y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
       ]

       //A등급반
       let chap1map9 = [
            {
               type: "wall",
               sw: 1600,
               sh: 120,
               gx: 0,
               gy: 680,
               portalData: false,
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 40,
               gy: 600,
               portalData: {
                   to: 106, //학교 복도
                   pos: {x: 4700, y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
       ]

       //학교 뒷마당1
       let chap1map10 = [
            {
               type: "wall",
               sw: 2400,
               sh: 120,
               gx: 0,
               gy: 880,
               portalData: false,
           },
           {
               type: "wall",
               sw: 1200,
               sh: 40,
               gx: 600,
               gy: 680,
               portalData: false,
           },
           {
               type: "wall",
               sw: 800,
               sh: 40,
               gx: 800,
               gy: 400,
               portalData: false,
           },
           {
               type: "wall",
               sw: 400,
               sh: 40,
               gx: 1000,
               gy: 160,
               portalData: false,
           },
           {
               type: "ladder",
               sw: 10,
               sh: 170,
               gx: 740,
               gy: 680,
           },
           {
               type: "ladder",
               sw: 10,
               sh: 200,
               gx: 1250,
               gy: 400,
           },
           {
               type: "ladder",
               sw: 10,
               sh: 200,
               gx: 1050,
               gy: 160,
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 40,
               gy: 800,
               portalData: {
                   to: 102, //학교
                   pos: {x: 720, y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 1160,
               gy: 80,
               portalData: {
                   to: 111, //학교 뒷마당2
                   pos: {x: 10, y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 2280,
               gy: 800,
               portalData: {
                   to: 112, //계약의성소
                   pos: {x: 10, y: 519},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
       ]

       //학교 뒷마당2
       let chap1map11 = [
            {
               type: "wall",
               sw: 2400,
               sh: 120,
               gx: 0,
               gy: 880,
               portalData: false,
           },
           {
               type: "wall",
               sw: 2000,
               sh: 40,
               gx: 200,
               gy: 240,
               portalData: false,
           },
           {
               type: "wall",
               sw: 560,
               sh: 80,
               gx: 200,
               gy: 520,
               portalData: false,
           },
           {
               type: "wall",
               sw: 560,
               sh: 80,
               gx: 920,
               gy: 520,
               portalData: false,
           },
           {
               type: "wall",
               sw: 560,
               sh: 80,
               gx: 1640,
               gy: 520,
               portalData: false,
           },
           {
               type: "ladder",
               sw: 10,
               sh: 300,
               gx: 320,
               gy: 420,
           },
           {
               type: "ladder",
               sw: 10,
               sh: 300,
               gx: 2080,
               gy: 520,
           },
           {
               type: "ladder",
               sw: 10,
               sh: 200,
               gx: 840,
               gy: 240,
           },
           {
               type: "ladder",
               sw: 10,
               sh: 200,
               gx: 1560,
               gy: 240,
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 40,
               gy: 800,
               portalData: {
                   to: 110, //뒷마당 1
                   pos: {x: 1120, y: -3},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },

       ]

       //계약의 성소
       let chap1map12 = [
            {
               type: "wall",
               sw: 1600,
               sh: 120,
               gx: 0,
               gy: 680,
               portalData: false,
           },
           {   
               name: "석상", 
               type: "npc",
               id: 107000,
               sw: 200,
               sh: 360,
               gx: 1260,
               gy: 359,
               img: this.getStructureImg(107000),      
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 40,
               gy: 600,
               portalData: {
                   to: 110, //뒷마당 2
                   pos: {x: 2280, y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 1480,
               gy: 600,
               portalData: {
                   to: 200, //엘린버드캠프 가는길1
                   pos: {x: 10, y: 719},
                   access: false,
               },
               img: this.getStructureImg(0), 
           },
       ]

       //새내기 반
       let chap1map13 = [
            {
               type: "wall",
               sw: 1600,
               sh: 120,
               gx: 0,
               gy: 680,
               portalData: false,
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 10,
               gy: 600,
               portalData: {
                   to: 106, //학교 복도
                   pos: {x: 680, y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
           {   
               name: "클론", 
               type: "npc",
               id: 105000,
               sw: 160,
               sh: 160,
               gx: 1300,
               gy: 552,
               img: this.getStructureImg(105000),      
           },
           {   
               name: "바스티온", 
               type: "npc",
               id: 106000,
               sw: 160,
               sh: 160,
               gx: 1000,
               gy: 552,
               img: this.getStructureImg(106000),      
           },
       ]


       //엘린버드캠프가는길1
       let chap2map0 = [
            {
               type: "wall",
               sw: 2400,
               sh: 120,
               gx: 0,
               gy: 880,
               portalData: false,
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 10,
               gy: 800,
               portalData: {
                   to: 112, //대강당
                   pos: {x: 1480,y: 519},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 2280,
               gy: 800,
               portalData: {
                   to: 201, //계약의성소
                   pos: {x: 10, y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
       ]
       //엘린버드캠프가는길2
       let chap2map1 = [
            {
               type: "wall",
               sw: 2400,
               sh: 120,
               gx: 0,
               gy: 880,
               portalData: false,
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 10,
               gy: 800,
               portalData: {
                   to: 200, //대강당
                   pos: {x: 2280,y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 2280,
               gy: 800,
               portalData: {
                   to: 202, //엘린버드캠프
                   pos: {x: 10, y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
       ]
        //엘린버드캠프
       let chap2map2 = [
            {
               type: "wall",
               sw: 3000,
               sh: 120,
               gx: 0,
               gy: 880,
               portalData: false,
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 10,
               gy: 800,
               portalData: {
                   to: 201, //엘린버드캠프가는길2
                   pos: {x: 2280,y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 1000,
               gy: 800,
               portalData: {
                   to: 203, //울창한숲1
                   pos: {x: 2280,y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
       ]

        //울창한숲1
       let chap2map3 = [
            {
               type: "wall",
               sw: 2400,
               sh: 120,
               gx: 0,
               gy: 880,
               portalData: false,
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 10,
               gy: 800,
               portalData: {
                   to: 204, //울창한숲2
                   pos: {x: 2280,y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 2280,
               gy: 800,
               portalData: {
                   to: 202, //앨린버드캠프
                   pos: {x: 1000,y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
       ]

        //울창한숲2
       let chap2map4 = [
            {
               type: "wall",
               sw: 2400,
               sh: 120,
               gx: 0,
               gy: 880,
               portalData: false,
           },
           {
               type: "portal",
               sw: 80,
               sh: 80,
               gx: 2280,
               gy: 800,
               portalData: {
                   to: 203, //울창한숲1
                   pos: {x: 10,y: 719},
                   access: true,
               },
               img: this.getStructureImg(0), 
           },
       ]

        this.structureList[100] = chap1map0;
        this.structureList[101] = chap1map1;
        this.structureList[102] = chap1map2;
        this.structureList[103] = chap1map3;
        this.structureList[104] = chap1map4;
        this.structureList[105] = chap1map5;
        this.structureList[106] = chap1map6;
        this.structureList[107] = chap1map7;
        this.structureList[108] = chap1map8;
        this.structureList[109] = chap1map9;
        this.structureList[110] = chap1map10;
        this.structureList[111] = chap1map11;
        this.structureList[112] = chap1map12;
        this.structureList[113] = chap1map13;
        this.structureList[200] = chap2map0;
        this.structureList[201] = chap2map1;
        this.structureList[202] = chap2map2;
        this.structureList[203] = chap2map3;
        this.structureList[204] = chap2map4;

    }

}

