class QuestData{
    questList;
    constructor(game){
        this.game = game;
        this.questList = {};
        this.setting();
    }

    getQuestInfo(id){
        return this.questList[id];
    }
    
    getNewQuest(){
        let newQuestList = [];
        let questIds = this.questList.questIds;
        for(let i = 0; i < questIds.length; i++){
            if(this.prereqCheck(this.questList[questIds[i]])){
                newQuestList.push(this.questList[questIds[i]]);
            }
        }
        return newQuestList;
    }
    prereqCheck(quest){
        if(quest.state !== "notStarted"){
            return;
        }
        let prereq = quest.prerequisite;
        let LV = this.game.player.stat.LV;
        let check = false;
        for(let j = 0; j < prereq.length; j++){
            if(prereq[j].type === "LV"){
                if(prereq[j].data > LV){
                    check = false;
                    break;
                }
                else{
                    check = true;
                }
            }
            else if(prereq[j].type === "quest"){
                if(this.questList[prereq[j].data].state != "finished"){
                    check = false;
                    break;
                }
                else{
                    check = true;
                }
            }
        }
        return check;
    }
    setQuestState(inprocessQuests, finishedQuests){
        this.setFinishedQuestList(finishedQuests);
        this.setInprocessQuestList(inprocessQuests);
    }
    setInprocessQuestList(quests){
        quests.forEach(quest => {
            this.questList[quest.id].state = "inprocess";
        })
    }
    setInprocessQuest(id){
        this.questList[id].state = "inprocess";
    }
    setFinishedQuestList(idList){
        idList.forEach(id => {
            this.questList[id].state = "finished";
        })
    }
    setFinishedQuest(id){
        this.questList[id].state = "finished";
    }
    setting(){
        this.questList = {
            questIds : [10, 20, 30, 40,50,60,70,80,90,100,110,120,130,140,150,160,170],

            10 : {
                state: "notStarted",
                name: "여정의 시작",
                questType: "main",
                id: 10,
                steps: [
                    {type: "monolog", id: 0},
                ],
                prerequisite: [
                    {type: "LV", data: 0}
                ],
                requirement: false,
                award: [
                    {type: "EXP", amount: 100},
                ],
                preInfo: "새로운 여정을 시작하자.",
                info: ""
            },
            20 : {
                state: "notStarted",
                name: "학생회장과의 첫 만남",
                questType: "main",
                id: 20,
                steps: [
                    {type: "talk", id: 102000},
                    {type: "kill", id: 1},
                    {type: "talk", id: 102000},
                ],
                prerequisite: [
                    {type: "LV", data: 0},
                    {type: "quest", data: 10},
                ],
                requirement: {
                    type: "kill",
                    object: 1,
                    amount: 50,
                    process: 0,
                },
                award: [
                    {type: "EXP", amount: 500},
                    {type: "gold", amount: 3000},
                    {type: "item", id: 1101, amount: 1},
                    {type: "item", id: 1201, amount: 1},
                ],
                preInfo: "학생회장 나즈리엘과 대화를 나눠보자.",
                info: "학생회장 나즈리엘이 학교 주변이 더러워 공기가 탁하다고 한다. 그녀의 고민거리를 해결해주자."
            },
            30 : {
                state: "notStarted",
                name: "솔선수범",
                questType: "main",
                id: 30,
                steps: [
                    {type: "talk", id: 103000},
                    {type: "collect", id: 3001},
                    {type: "talk", id: 103000},
                ],
                prerequisite: [
                    {type: "LV", data: 5},
                    {type: "quest", data: 20},
                ],
                requirement: {
                    type: "collect",
                    object: 3001, //찌그러진 캔
                    amount: 30,
                    process: 0,
                },
                award: [
                    {type: "EXP", amount: 1000},
                ],
                preInfo: "분리수거함과 대화해보자.",
                info: "주변에 쓰레기가 많이 떨어져있다. 치워주자."
            },
            40 : {
                state: "notStarted",
                name: "교장 아바나와의 만남",
                questType: "main",
                id: 40,
                steps: [
                    {type: "talk", id: 104000},
                    {type: "talk", id: 104000},
                ],
                prerequisite: [
                    {type: "LV", data: 0},
                    {type: "quest", data: 30},
                ],
                requirement: false,
                award: [
                    {type: "EXP", amount: 1000},
                    {type: "item", id: 1301, amount: 1},
                    {type: "item", id: 1401, amount: 1},
                    {type: "item", id: 1501, amount: 1},
                ],
                preInfo: "교장 아바나가 할 말이 있어보인다.",
                info: "다시 한번 말을 걸어 배정된 반을 알아보자."
            },
            50 : {
                state: "notStarted",
                name: "등급 측정",
                questType: "main",
                id: 50,
                steps: [
                    {type: "talk", id: 105000},
                    {type: "kill", id: 3},
                    {type: "talk", id: 105000},
                ],
                prerequisite: [
                    {type: "LV", data: 0},
                    {type: "quest", data: 40},
                ],
                requirement: {
                    type: "kill",
                    object: 3, //찌그러진 캔
                    amount: 50,
                    process: 0,
                },
                award: [
                    {type: "EXP", amount: 2500},
                ],
                preInfo: "새로 배정된 반으로 가서 등급 측정을 해보자.",
                info: "등급 측정을 위해 C등급 표적부터 차근차근 잡아보자."
            },
            60 : {
                state: "notStarted",
                name: "도전 정신",
                questType: "main",
                id: 60,
                steps: [
                    {type: "talk", id: 105000},
                    {type: "kill", id: 4},
                    {type: "talk", id: 105000},
                ],
                prerequisite: [
                    {type: "LV", data: 0},
                    {type: "quest", data: 50},
                ],
                requirement: {
                    type: "kill",
                    object: 4, //찌그러진 캔
                    amount: 70,
                    process: 0,
                },
                award: [
                    {type: "EXP", amount: 5000},
                ],
                preInfo: "다음 등급 측정을 한번 해볼까?",
                info: "등급 측정을 위해 B등급 표적을 잡아보자."
            },
            70 : {
                state: "notStarted",
                name: "뛰어난 재능",
                questType: "main",
                id: 70,
                steps: [
                    {type: "talk", id: 105000},
                    {type: "kill", id: 7},
                    {type: "talk", id: 105000},
                ],
                prerequisite: [
                    {type: "LV", data: 0},
                    {type: "quest", data: 60},
                ],
                requirement: {
                    type: "kill",
                    object: 7, //찌그러진 캔
                    amount: 1,
                    process: 0,
                },
                award: [
                    {type: "EXP", amount: 10000},
                ],
                preInfo: "마지막 등급 측정이다. 신중히 선택하자 위험할 수 있다.",
                info: "이왕 등급 측정하는거 확실하게 하고가자. A등급 몬스터를 처치하는거야."
            },
            80 : {
                state: "notStarted",
                name: "비겁한 부탁",
                questType: "main",
                id: 80,
                steps: [
                    {type: "talk", id: 106000},
                    {type: "kill", id: 4},
                    {type: "talk", id: 106000},
                ],
                prerequisite: [
                    {type: "LV", data: 0},
                    {type: "quest", data: 70},
                ],
                requirement: {
                    type: "kill",
                    object: 4, //찌그러진 캔
                    amount: 70,
                    process: 0,
                },
                award: [
                    {type: "EXP", amount: 10000},
                ],
                preInfo: "바스티온이 부탁이 있다는데..",
                info: "라나카의 행방을 알기 위해서라면 모든지 할 수 있어. B등급 몬스터를 대신 잡아주자."
            },
            90 : {
                state: "notStarted",
                name: "또 시작된 나즈리엘의 부탁",
                questType: "main",
                id: 90,
                steps: [
                    {type: "talk", id: 102000},
                    {type: "collect", id: 3002},
                    {type: "talk", id: 102000},
                ],
                prerequisite: [
                    {type: "LV", data: 0},
                    {type: "quest", data: 80},
                ],
                requirement: {
                    type: "collect",
                    object: 3002, //빗자루
                    amount: 1,
                    process: 0,
                },
                award: [
                    {type: "EXP", amount: 15000},
                ],
                preInfo: "나즈리엘의 또 시작된 부탁을 들어줄까..",
                info: "입학식으로 인해 학교가 많이 더러워졌다며 빗자루 하나만 들고 와달라고 한다."
            },
            100 : {
                state: "notStarted",
                name: "바스티온의 정보",
                questType: "main",
                id: 100,
                steps: [
                    {type: "monolog", id: 0},
                    {type: "talk", id: 106000},
                ],
                prerequisite: [
                    {type: "LV", data: 0},
                    {type: "quest", data: 90},
                ],
                requirement: false,
                award: [
                    {type: "EXP", amount: 10000},
                ],
                preInfo: "바스티온에게 연락이왔다.",
                info: "바스티온이 저번에 부탁했던 정보를 알아왔다고한다. 어서 가보자."
            },
            110 : {
                state: "notStarted",
                name: "전직가능",
                questType: "important",
                id: 110,
                steps: [
                    {type: "monolog", id: 0},
                    {type: "talk", id: 107000},
                ],
                prerequisite: [
                    {type: "LV", data: 20},
                    //{type: "quest", data: 100},
                    
                ],
                requirement: false,
                award: [
                ],
                preInfo: "어디선가 나를 부르는 목소리가 들린다.",
                info: "목소리가 들리는 곳으로 가보자. 뒷마당 너머에서 들린 것 같았는데.."
            },
            120 : {
                state: "notStarted",
                name: "전직",
                questType: "important",
                id: 120,
                steps: [
                    {type: "talk", id: 107000},
                    {type: "specialization", id: 1},
                    {type: "talk", id: 107000},
                ],
                prerequisite: [
                    {type: "LV", data: 20},
                    {type: "quest", data: 110},
                ],
                requirement: false,
                award: [
                    {type:"access", mapId: 200},
                ],
                preInfo: "한 층더 강해질 수 있을 것 같은 느낌이야.",
                info: "석상에게 말을 걸어 원하는 직업을 선택하자.."
            },
            130 : {
                state: "notStarted",
                name: "쓰레기 더미 수배 공고",
                questType: "sub",
                id: 130,
                steps: [
                    {type: "talk", id: 108000},
                    {type: "kill", id: 1},
                    {type: "talk", id: 108000},
                ],
                prerequisite: [
                    {type: "LV", data: 5},
                ],
                requirement: {
                    type: "kill",
                    object: 1, //찌그러진 캔
                    amount: 222,
                    process: 0,
                },
                award: [
                    {type: "EXP", amount: 2000},
                    {type: "gold", amount: 100000}
                ],
                preInfo: "게시판에 공고가 올라왔다.",
                info: "쓰레기 더미를 처치하자."
            },
            140 : {
                state: "notStarted",
                name: "깡통 수배 공고",
                questType: "sub",
                id: 140,
                steps: [
                    {type: "talk", id: 108000},
                    {type: "kill", id: 2},
                    {type: "talk", id: 108000},
                ],
                prerequisite: [
                    {type: "LV", data: 5},
                    {type: "quest", data: 130},
                ],
                requirement: {
                    type: "kill",
                    object: 2, //찌그러진 캔
                    amount: 222,
                    process: 0,
                },
                award: [
                    {type: "EXP", amount: 3000},
                    {type: "gold", amount: 120000}
                ],
                preInfo: "게시판에 공고가 올라왔다.",
                info: "깡통을 처치하자."
            },
            150 : {
                state: "notStarted",
                name: "C등급표적물 수배 공고",
                questType: "sub",
                id: 150,
                steps: [
                    {type: "talk", id: 108000},
                    {type: "kill", id: 3},
                    {type: "talk", id: 108000},
                ],
                prerequisite: [
                    {type: "LV", data: 5},
                    {type: "quest", data: 140},
                ],
                requirement: {
                    type: "kill",
                    object: 3, //찌그러진 캔
                    amount: 222,
                    process: 0,
                },
                award: [
                    {type: "EXP", amount: 5000},
                    {type: "gold", amount: 150000}
                ],
                preInfo: "게시판에 공고가 올라왔다.",
                info: "C등급표적물를 처치하자."
            },
            160 : {
                state: "notStarted",
                name: "B등급표적물 수배 공고",
                questType: "sub",
                id: 160,
                steps: [
                    {type: "talk", id: 108000},
                    {type: "kill", id: 4},
                    {type: "talk", id: 108000},
                ],
                prerequisite: [
                    {type: "LV", data: 5},
                    {type: "quest", data: 150},
                ],
                requirement: {
                    type: "kill",
                    object: 4, //찌그러진 캔
                    amount: 222,
                    process: 0,
                },
                award: [
                    {type: "EXP", amount: 6000},
                    {type: "gold", amount: 160000}
                ],
                preInfo: "게시판에 공고가 올라왔다.",
                info: "B등급표적물를 처치하자."
            },
            170 : {
                state: "notStarted",
                name: "마법빗자루 수배 공고",
                questType: "sub",
                id: 170,
                steps: [
                    {type: "talk", id: 108000},
                    {type: "kill", id: 6},
                    {type: "talk", id: 108000},
                ],
                prerequisite: [
                    {type: "LV", data: 5},
                    {type: "quest", data: 160},
                ],
                requirement: {
                    type: "kill",
                    object: 6, //찌그러진 캔
                    amount: 222,
                    process: 0,
                },
                award: [
                    {type: "EXP", amount: 8000},
                    {type: "gold", amount: 200000}

                ],
                preInfo: "게시판에 공고가 올라왔다.",
                info: "마법빗자루를 처치하자."
            }
       }
   }
}