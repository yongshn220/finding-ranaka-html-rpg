class MessageQueue {
    messages;
    constructor(){
        this.messages = [];
    }

    add(msg){
        this.messages.push(msg);
    }

    addDelay(msg, time){
        
    }

    dispatch(){
        let entity, msg;
        for(let i = 0; i < this.messages.length; i++){
            msg = this.messages[i];
            if(msg){
                entity = msg.to;
                if(entity){
                    entity.onMessage(msg);
                }
            }
            this.messages.splice(i, 1);
            i--;
        }
    }
}

class Message {
    constructor(from, to, type, data){
        this.from = from;
        this.to = to;
        this.type = type;
        this.data = data;
    }
}