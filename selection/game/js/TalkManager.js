class TalkManager{
    talkIndex;
    talkData;
    constructor(game){
        this.game = game;
        this.talkIndex = 0;
        this.talkData = new TalkData(this.game);
    }

    talkEvent(id, name, type){
        this.rawId = id;
        this.name = name;
        this.type = type;
        if(this.idSetting()){
            this.talk();
        }        
    }

    talk(){
        let text = this.talkData.getText(this.finId, this.talkIndex);
        this.talkStart(text);
    }

    idSetting(){
        let inprocessQuest = this.game.questManager.getInprocessQuests();
        for(let i = 0; i < inprocessQuest.length; i++){
            let newId = this.rawId + inprocessQuest[i].id + inprocessQuest[i].actionIndex;
            if(this.talkData.contains(newId)){
                this.finId = newId;
                return true;
            }
        }
        for(let i = 0; i < inprocessQuest.length; i++){
            let newId = this.rawId + inprocessQuest[i].id;
            if(this.talkData.contains(newId)){
                this.finId = newId;
                return true;
            }
        }
        if(this.talkData.contains(this.rawId)){
            this.finId = this.rawId;
            return true;
        }
        return false;
    }
    nextTalk(){
        this.talkIndex++;
        if(this.talkIndex >= this.talkData.getTalk(this.finId).length){
            this.talkEnd();
        }
        else{
            this.talk();
        }
    }

    talkStart(text, name){
        this.game.stateManager.setState("talkManager", "isTalking", true);
        let msg = new Message(this, this.game.UI.UIInteraction, "talkStart", {text:text, name:this.name});
        this.game.messageQueue.add(msg);
    }

    talkEnd(){
        this.talkIndex = 0;
        this.game.stateManager.setState("talkManager", "isTalking", false);
        let data = {type: "talk", id: this.rawId};
        let msg = new Message(this, this.game.UI.UIInteraction, "talkEnd", {});
        this.game.messageQueue.add(msg);
    }
    
    onMessage(msg){
        if(msg.type === "talkEvent"){
            this.talkEvent(msg.data.id, msg.data.name, msg.data.type);
        }
        else if(msg.type === "nextTalk"){
            this.nextTalk();
        }
    }
}