class Map{
    game;
    name;
    mapData;
    baseBackground;
    mainBackground;
    structureList;


    constructor(game, code, mapData){
        this.game = game;
        this.name = false;
        this.code = code;
        this.mapData = mapData;
        this.structureList = [];
        this.setting();
        this.structureSetting();
    }

    setting(){
        let info = this.mapData.infoList[this.code];
        let baseImg = this.mapData.baseImageList[this.code];
        let mainImg = this.mapData.mainImageList[this.code];
        this.name = info.name;
        this.baseBackground = new Background(screen_width, screen_height, baseImg);
        this.mainBackground = new Background(info.width, info.height, mainImg);
    }

    structureSetting(){
        let stuData = this.mapData.structureList[this.code];
        stuData.forEach(stu => {
            this.structureList.push(new Structure(game, stu));
        })
    }
    
    isCollid(gx, gy, sw, sh, type){
        for(let i = 0; i < this.structureList.length; i++){
            if(this.structureList[i].isCollid(gx,gy,sw,sh, type)){
                return true;
            }
        }
        return false;
    }
    isPointCollid(gx, gy, type){
        for(let i = 0; i < this.structureList.length; i++){
            if(this.structureList[i].isPointCollid(gx,gy,type)){
                return true;
            }
        }
        return false;
    }
    isLineCollid(gx, gy, sw, type){
        for(let i = 0; i < this.structureList.length; i++){
            if(this.structureList[i].isLineCollid(gx,gy,sw,type)){
                return true;
            }
        }
        return false;
    }

    getIn(){
        for(let i = 0; i < this.structureList.length; i++){
            let char = this.game.player.character;
            if(this.structureList[i].type === "portal"){
                if(this.structureList[i].isCollid(char.gx, char.gy, char.sw, char.sh, "portal")){
                    this.structureList[i].getIn();
                    return "mapIn";
                }
            }
        }
        for(let i = 0; i < this.structureList.length; i++){
            let char = this.game.player.character;
            if(this.structureList[i].type === "ladder"){
                if(this.structureList[i].isCollid(char.gx + 80, char.gy + 80, 5, 80, "ladder")){
                    this.game.player.character.ladderUp();
                    return "ladderOn";
                }
            }
        }
        this.game.stateManager.setState("character", "isLadderOn", false);
    }
    getDown(){
        for(let i = 0; i < this.structureList.length; i++){
            let char = this.game.player.character;
            if(this.structureList[i].type === "ladder"){
                if(this.structureList[i].isLineCollid(char.gx + 80, char.gy + char.sh + 1, 5, "ladder")){
                    this.game.player.character.ladderDown();
                    return;
                }
            }
        }
        this.game.stateManager.setState("character", "isLadderOn", false);
    }
    getTalk(){
        for(let i = 0; i < this.structureList.length; i++){
            let char = this.game.player.character;
            let type = this.structureList[i].type;
            if(type === "npc" || type === "store"){
                if(this.structureList[i].isCollid(char.gx, char.gy, char.sw, char.sh, type)){
                    this.structureList[i].getTalk();
                    return;
                }
            }
        }

        //monolog
        let msg = new Message(this, this.game.talkManager, "talkEvent", {id: 0, name: "레이", type: "monolog"});
        this.game.messageQueue.add(msg);
        msg = new Message(this, this.game.questManager, "questCheck", {type: "monolog", id: 0});
        this.game.messageQueue.add(msg);
    }

    structureUpdate(){
        this.structureList.forEach(structure => {
            if(structure.isDrawable){
                structure.update();
            }
        })
    }

    onMessage(msg){
        if(msg.type === "getIn"){
            this.getIn();
        }
        else if(msg.type === "getDown"){
            this.getDown();
        }
        else if(msg.type === "getTalk"){
            this.getTalk();
        }
    }

    update(){
        this.baseBackground.update();
        this.mainBackground.update();
        this.structureUpdate();
    }
}