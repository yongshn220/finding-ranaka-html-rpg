class Character extends Entity{
    game;
    moveSpeed;
    isMoving;
    maxSourceX;
    delayState;
    gravityActivation;

    constructor(game){
        super();
        this.game = game;
        
        this.moveSpeed = 2;
        this.jumpPower = 18;
        this.delayState = false;
        this.motionState = 0;
        this.gravityActivation = true;
        this.setting();
        this.create();
        this.questState = -1;
        this.newQuestImg = new Image();
        this.newQuestImg.src = './images/UI/quest/newQuestMark.png'
        this.finishedQuestImg = new Image();
        this.finishedQuestImg.src = './images/UI/quest/finishedQuestMark.png'
        this.questImg = [this.newQuestImg, this.finishedQuestImg];
    }

    create(){
        this.game.addEntity(this);
    }
    
    charImageSetting(){
        let info = this.game.player.information;
        let charCode = info.job * 100 + info.specialize;
        this.movingImg = characterImages[charCode].moving;
        this.standingImg = characterImages[charCode].standing;
        this.image = this.standingImg;
    }
    backup(pos){
        this.dx = pos.x;
        this.dy = pos.y;
        this.gx = pos.x;
        this.gy = pos.y;
        this.charImageSetting();
    }

    save(pos){
        pos.x = this.gx;
        pos.y = this.gy;
    }

    setting(){
        this.dx = 800;
        this.dy = 0;
        this.gx = 800;
        this.gy = 0;
        this.sw = char_width;
        this.sh = char_height;
        this.dw = char_width;
        this.dh = char_height;
        this.maxSourceX = char_width * 4;
        this.RX = 40;
        this.RY = 80;
        this.RW = 80;
        this.RH = 80;
        this.view = "left";
    }
    rePosition(pos){
        this.dx = pos.x;
        this.dy = pos.y;
        this.gx = pos.x;
        this.gy = pos.y;
    }
    
    standing(){
        if(!this.game.stateManager.getState("character", "isDelaying")){
            this.motionChange(this.standingImg, 0, 4);
        }
        this.game.stateManager.setState("character", "isMoving", false);
    }

    levelUpdate(){
        this.imageUpdate();
    }
    imageUpdate(){
        if(this.sx < (this.maxSourceX - this.sw)){
            this.sx += this.sw;
        }
        else if(!this.game.stateManager.getState("character", "isDelaying")){
            this.sx = 0;
        }
    }

    updateViewPoint(view){
        if(this.view === view){
            return;
        }
        //console.log([this.gx, this.gy]);
        let isDelaying = this.game.stateManager.getState("character", "isDelaying")
        let isLadderOn = this.game.stateManager.getState("character", "isLadderOn")
        if(!isDelaying && !isLadderOn){
            this.view = view;
            if(view === "left"){
                this.sy = 0;
            }
            else if(view === "right"){
                this.sy = this.sh;
            }
        }
    }

    positionAdjust(){
        let background = this.game.mapManager.curMap.mainBackground;
        this.dx = this.gx - background.sx;
        this.dy = this.gy - background.sy;
    }

    move(x, y){
        if(this.moveValidCheck()){
            this.gx += x;
            this.gy += y;
        }  
    }
    moveValidCheck(){
        let isDelaying = this.game.stateManager.getState("character", "isDelaying");
        let isJumping = this.game.stateManager.getState("character", "isJumping");
        let isLadderOn = this.game.stateManager.getState("character", "isLadderOn");
        if(isLadderOn){
            return false;
        }
        if(isDelaying){
            if(!isJumping){
                return false;
            }
        }
        else{
            this.motionChange(this.movingImg, 1, 4);
        }
        return true;
    }
    ladderUp(){
        this.gy -= 2;
        this.game.stateManager.setState("character", "isLadderOn", true);
        this.game.stateManager.setState("character", "isJumping", false);
    }
    ladderDown(){
        this.gy +=2;
        this.game.stateManager.setState("character", "isLadderOn", true);
        this.game.stateManager.setState("character", "isJumping", false);
    }
    
    jumpStart(){
        if(this.game.stateManager.getState("character", "isJumping")){
            return;
        }
        this.game.stateManager.setState("character", "checkStructure", false);
        this.game.stateManager.setState("character", "isLadderOn", false);
        this.game.stateManager.setState("character", "isJumping", true);
        this.velocity.y = this.jumpPower;
    }
    jumpEnd(){
        this.game.stateManager.setState("character", "isJumping", false);
    }

    delay(data){
        this.game.stateManager.setState("character", "isDelaying", data.state);
        if(!data.state){
            this.motionChange(this.standingImg, 0, 4);
        }
        else{
            this.motionChange(data.img, 3, data.frame);
        }
    }

    eat(){
        let items = this.game.itemManager.itemList;
        items.forEach(item => {
            if(this.eatable(this, item) && item.activation){
                let msg = new Message(this, item, "eaten", {});
                this.game.messageQueue.add(msg);
                if(item instanceof Item){
                    msg = new Message(this, this.game.player.inventory, "newItem",{id: item.code, amount: 1});
                    this.game.messageQueue.add(msg);
                    let data = {type: "아이템 획득", value: this.game.itemManager.itemData.getItem(item.code).name};
                    msg = new Message(this, this.game.textManager, "newInfo", data);
                    this.game.messageQueue.add(msg);
                }
                else{
                    msg = new Message(this, this.game.player.inventory, "newGold", item.value);
                    this.game.messageQueue.add(msg);
                    msg = new Message(this, this.game.textManager, "newInfo", {type:"골드 획득", value:item.value});
                    this.game.messageQueue.add(msg);
                }
            }
        });
    }

    eatable(a, b){
        if((a.gx <= (b.gx + b.sw) && (a.gx + a.sw) >= b.gx) && a.gy <= (b.gy + b.sh) && (a.gy + a.sh) >= b.gy){ 
            return true;
        }
        return false;
    }

    onMessage(msg){
        if(msg.type === "move"){
            this.move(msg.data.x, msg.data.y);
        }
        else if(msg.type === "standing"){
            this.standing();
        }
        else if(msg.type === "jumpStart"){
            this.jumpStart();
        }
        else if(msg.type === "jumpEnd"){
            this.jumpEnd();
        }
        else if(msg.type === "rePosition"){
            this.rePosition(msg.data);
        }
        else if(msg.type === "delay"){
            this.delay(msg.data);
        }
        else if(msg.type === "eat"){
            this.eat();
        }
    }
    motionChange(img, state, frame){
        if(this.motionState !== state){
            this.sx = 0;
            this.maxSourceX = this.sw * frame;
            this.image = img;
            this.motionState = state;
        }  
    }

    update(){
        this.positionAdjust();
        this.draw();
    }

    draw(){
        canvas.context.drawImage(this.image,this.sx,this.sy,this.sw,this.sh,this.dx,this.dy,this.dw,this.dh);
        this.questState = this.game.questManager.npcQuestMarkState['0'];
        if(this.questState >= 0){
            let dx = this.dx + (this.dw/2 - 20);
            let dy = this.dy - 10;
            canvas.context.drawImage(this.questImg[this.questState],0,0,40,60,dx,dy,40,60);
        }
    }
}