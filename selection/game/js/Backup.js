class Backup{
    game;
    storage;
    information;
    myItem;
    stone;
    inventory;
    quest;
    npc;
    constructor(game){
        this.game = game;
        this.storage = new Storage();
        this.information = this.storage.getInformation();
        this.myItem = this.storage.getMyItem();
        this.stone = this.storage.getStone();
        this.inventory = this.storage.getInventory();
        this.quest = this.storage.getQuest();
        this.npc = this.storage.getNPC();
        this.backup();
    }

    backup(){
        //info
        let info = this.information;
        this.game.player.backup(info);
        this.game.player.stat.backup(info.stat);
        this.game.player.character.backup(info.position);
        this.game.mapManager.backup(info.position, info.mapData);
        this.game.player.skillManager.backup();
        this.game.player.keyBoard.backup(info.keyBoard)
        //inventory
        this.game.player.inventory.backup(this.inventory);
        this.game.UI.UIInteraction.inventoryInteraction.keyBackup();
        this.game.UI.UIInteraction.inventoryInteraction.update();
        this.game.UI.UIInteraction.skillInteraction.update();
        //quest
        this.game.questManager.backup(this.quest);
    }


    save(){
        this.game.player.save(this.information);
        this.game.player.stat.save(this.information.stat);
        this.game.player.character.save(this.information.position);
        this.game.mapManager.save(this.information.position, this.information.mapData);
        this.game.player.inventory.save(this.inventory);
        this.game.player.keyBoard.save(this.information.keyBoard)
        this.game.questManager.save(this.quest);
        this.storage.saveGameData();

    }
}