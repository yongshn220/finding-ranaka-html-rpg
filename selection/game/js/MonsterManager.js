class MonsterManager{
    game;
    monsterData;
    monsterList;
    maxNum;
    position;
    state;
    spawnTime;

    constructor(game){
        this.game = game;
        this.state = false;
        this.monsterList = [];
        this.monsterData = new MonsterData;
    }

    newMonsters(data){
        this.monsterList = [];
        if(data){
            this.state = true;
            this.spawnTime = 3;
            this.type = data.type;
            this.maxNum = data.maxNum;
            this.positions = data.pos;
            this.newMonsterData = this.monsterData.getMonster(data.code);
            this.spawnMonsters(this.maxNum);
        }
        else{
            this.state = false;
        }
    }

    spawnMonsters(numOfMonster){

        let mPos = 0;
        for(let i = 0; i < numOfMonster; i++){
            let newMonster = new Monster(this.game, this.newMonsterData, this.positions[mPos]);
            this.getDown(newMonster);
            this.monsterList.push(newMonster);
            if(mPos < this.positions.length - 1){
                mPos ++
            }
            else{
                mPos = 0;
            }
        }
        
    }

    getDown(monster){
        let gx = monster.gx;
        let gy = monster.gy;
        let sw = monster.sw;
        let sh = monster.sh;

        let state = true;
        while(state){
            gy++;
            if(!this.game.mapManager.curMap.isCollid(gx,gy,sw,sh, "wall")){
                
            }
            else{
                state = false;
                monster.gy = gy;
            }
        }
    }

    hitEvent(){
        if(!this.game.timeManager.getTimer("charAttacked").state){
            this.monsterList.forEach(monster => {
                let char = this.game.player.character;
                if(this.isCollid(monster, char)){
                    let msg = new Message(this, this.game.player.stat, "damaged", monster.AD);
                    this.game.messageQueue.add(msg);
                }
            });
        }
    }

    isCollid(a, b){
        if((a.gx <= (b.gx + b.sw) && (a.gx + a.sw) >= b.gx) && a.gy <= (b.gy + b.sh) && (a.gy + a.sh) >= b.gy){ 
            return true;
        }
        return false;
    }

    levelUpdate(){
        this.imageUpdate();
    }

    imageUpdate(){
        if(this.state){
            this.monsterList.forEach(monster => monster.imageUpdate());
            this.monsterList = this.monsterList.filter(monster => monster.activation);
            this.respawn();
        }
    }

    update(){
        if(this.state){
            this.monsterList.forEach(monster => { 
                monster.update();
            });
            this.hitEvent();
        }
    }

    onMessage(msg){
        if(msg.type === "newMonsters"){
            this.newMonsters(msg.data);
        }
    }

    respawn(){
        this.spawnTime -= 0.2;
        let respawnNum = this.maxNum - this.monsterList.length
        if(this.spawnTime <= 0 && respawnNum > 0){
            this.spawnMonsters(respawnNum);
            this.spawnTime = 3;
        }
    }
}