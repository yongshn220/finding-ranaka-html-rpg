class Entity{
    constructor(type){
        this.entitySetting(type)
    }
    entitySetting(type){
        if(type === undefined){
            this.sx = 0;
            this.sy = 0;
            this.sw = 0;
            this.sh = 0;
            this.dx = 0;
            this.dy = 0;
            this.dw = 0;
            this.dh = 0;
            this.gx = 0;
            this.gy = 0;
            this.RX = 0;
            this.RY = 0;
            this.RW = 0;
            this.RH = 0;
            this.velocity = {x : 0, y: 0};
        }
        else if(type instanceof Character){
            this.sx = type.sx;
            this.sy = type.sy;
            this.sw = type.sw;
            this.sh = type.sh;
            this.dx = type.dx;
            this.dy = type.dy;
            this.dw = type.dw;
            this.dh = type.dh;
            this.gx = type.gx;
            this.gy = type.gy;
            this.RX = type.RX;
            this.RY = type.RY;
            this.RW = type.RW;
            this.RH = type.RH;
            this.velocity = type.velocity;
        }
    }
}