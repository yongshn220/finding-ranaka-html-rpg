var KEY = {
    //player one
    LEFT : 37,
    UP : 38,
    RIGHT : 39,
    DOWN : 40,
    JUMP : 67, //
    EAT : 90,
    TALK: 32,
}

var keyMap = {
    [KEY.LEFT] : false,
    [KEY.UP] : false,
    [KEY.RIGHT] : false,
    [KEY.DOWN] : false,
    [KEY.JUMP] : false,
    [KEY.EAT] : false,
    [KEY.TALK] : false,
}
var consumpKEY = {
    delete: 49,
    end: 50,
}
var consumpKeyMap = {
    [consumpKEY.delete] : false,
    [consumpKEY.end] : false,
}
var valueToConsumpKey = {
    49 : "delete",
    50 : "end",
}

var skillKEY = {
    Q : 81,
    W : 87,
    E : 69,
    R : 82,
    A : 65,
    S : 83,
    D : 68,
    F : 70,
}

var skillKeyMap = {
    [skillKEY.Q] : false,
    [skillKEY.W] : false,
    [skillKEY.E] : false,
    [skillKEY.R] : false,
    [skillKEY.A] : false,
    [skillKEY.S] : false,
    [skillKEY.D] : false,
    [skillKEY.F] : false,
}

var valueToKey = {
    81 : 'Q',
    87 : 'W',
    69 : 'E',
    82 : 'R',
    65 : 'A',
    83 : 'S',
    68 : 'D',
    70 : 'F',
}

function standing(game){
    let msg = new Message("keyEvent", game.player.character, "standing", {});
    game.messageQueue.add(msg);
}

function moveCharacter(game){
    let tempCx = 0;
    let tempCy = 0;
    let moveSpeed =4;
    let keyStateLeft = keyMap[KEY.LEFT];
    let keyStateRight = keyMap[KEY.RIGHT];
    
    // if all key false
    if(!keyStateLeft && !keyStateRight){
        standing(game);
        return;
    }
    //left
    if(keyStateLeft && !keyStateRight){
        tempCx = moveSpeed * -1;
    }
    //right
    else if(!keyStateLeft && keyStateRight){
        tempCx = moveSpeed;
    }

    game.player.character.updateViewPoint(getViewpoint(keyStateLeft, keyStateRight));

    if(game.characterMove(tempCx, tempCy)){
        //nothing;
    }
    else{
        standing(game);
        return;
    }

    setTimeout(function(){
        moveCharacter(game);
    }, 10);
}

function getViewpoint(l, r){
    let viewpoint = ""; 
    if(l && !r){
        viewpoint = "left";
    }
    else if(!l && r){
        viewpoint = "right";
    }
    else{
        viewpoint = game.player.character.view;
    }
    return viewpoint;
}

function jump(game){
    let msg = new Message("keyEvent", game.player.character, "jumpStart", {});
    game.messageQueue.add(msg);
}
// getIn or getUp
function getIn(game){
    if(!keyMap[KEY.UP]){
        return;
    }
    let checkGetIn = game.mapManager.curMap.getIn();
    if(checkGetIn === "mapIn"){
        return;
    }
    setTimeout(function(){
        getIn(game);
    }, 7);
}
function getDown(game){
    if(!keyMap[KEY.DOWN]){
        return;
    }
    let msg = new Message(this, game.mapManager.curMap, "getDown", {});
    game.messageQueue.add(msg);
    setTimeout(function(){
        getDown(game);
    }, 7);
}

function skill(game, keyCode){
    if(!game.stateManager.getState("character", "isDelaying")){
        let key = valueToKey[keyCode]; // q, w, e, r .. 등등
        let skillCode = game.player.keyBoard[key];
        if(skillCode < 0){
            return;
        }
        let msg = new Message("keyEvent", game.player.skillManager, "skill", skillCode);
        game.messageQueue.add(msg);
    }
}
function consume(game, keyCode){
    let key = valueToConsumpKey[keyCode];
    let itemCode = game.player.keyBoard[key];
    if(itemCode < 0){
        return;
    }
    let msg = new Message("keyEvent", game.player.inventory, "consume", itemCode);
    game.messageQueue.add(msg);
}
function eat(game){
    if(!keyMap[KEY.EAT]){
        return;
    }
    let msg = new Message("keyEvent", game.player.character, "eat", {});
    game.messageQueue.add(msg);
    
    setTimeout(function(){
        eat(game);
    }, 100);
}

function talk(game){

    if(!game.stateManager.getState("talkManager", "isTalking")){
        let msg = new Message(this, game.mapManager.curMap, "getTalk", {});
        game.messageQueue.add(msg);
    }
    else{
        let msg = new Message(this, game.talkManager, "nextTalk", {});
        game.messageQueue.add(msg);
    }

}


function addEventListener(game){
    event.preventDefault();
    document.addEventListener('keydown', event => {
        console.log(event.keyCode);
        if(event.keyCode in keyMap){    
            if(event.keyCode === KEY.LEFT || event.keyCode === KEY.RIGHT){
                keyMap[event.keyCode] = true;
                if(!game.stateManager.getState("character", "isMoving")){
                    game.stateManager.setState("character", "isMoving", true);
                    moveCharacter(game);
                }
            }
            else if(event.keyCode === KEY.JUMP && !keyMap[event.keyCode]){
                jump(game);
            }
            else if(event.keyCode === KEY.UP && !keyMap[event.keyCode]){
                keyMap[event.keyCode] = true;
                getIn(game);
            }
            else if(event.keyCode === KEY.DOWN && !keyMap[event.keyCode]){
                keyMap[event.keyCode] = true;
                getDown(game);
            }
            else if(event.keyCode === KEY.EAT && !keyMap[event.keyCode]){
                keyMap[event.keyCode] = true;
                eat(game);
            }
            else if(event.keyCode === KEY.TALK && !keyMap[event.keyCode]){
                keyMap[event.keyCode] = true;
                talk(game);
            }
        }
        else if(event.keyCode in skillKeyMap && !skillKeyMap[event.keyCode]){
            skill(game, event.keyCode);
        }
        else if(event.keyCode in consumpKeyMap && !consumpKeyMap[event.keyCode]){
            consume(game, event.keyCode);
        }
    })
    document.addEventListener('keyup', event => {
        event.preventDefault();
        if(event.keyCode in keyMap){
            keyMap[event.keyCode] = false;
        }
        else if(event.keyCode in skillKeyMap){
            skillKeyMap[event.keyCode] = false;
        }
        else if(event.keyCode in consumpKeyMap){
            consumpKeyMap[event.keyCode] = false;
        }
    })
}

class KeyBoard{
    constructor(){
        this.skillCodeList = [];
        this.itemCodeList = [];
    }
    keyCodeReset(){
        this.skillCodeList = [this.Q,this.W,this.E,this.R,this.A,this.S,this.D,this.F];
        this.itemCodeList = [this.delete,this.end];
    }
    backup(keyBoard){
        this.Q = keyBoard.skillCode.Q;
        this.W = keyBoard.skillCode.W;
        this.E = keyBoard.skillCode.E;
        this.R = keyBoard.skillCode.R;
        this.A = keyBoard.skillCode.A;
        this.S = keyBoard.skillCode.S;
        this.D = keyBoard.skillCode.D;
        this.F = keyBoard.skillCode.F;
        this.delete = keyBoard.itemCode.delete;
        this.end = keyBoard.itemCode.end;
        this.keyCodeReset();
    }
    save(keyBoard){
        keyBoard.skillCode.Q = this.Q;
        keyBoard.skillCode.W = this.W;
        keyBoard.skillCode.E = this.E;
        keyBoard.skillCode.R = this.R;
        keyBoard.skillCode.A = this.A;
        keyBoard.skillCode.S = this.S;
        keyBoard.skillCode.D = this.D;
        keyBoard.skillCode.F = this.F; 
        keyBoard.itemCode.delete = this.delete;
        keyBoard.itemCode.end = this.end;
    }
}