class QuestManager{
    constructor(game){
        this.game = game;
        this.inprocessQuests = [];
        this.finishedQuests = [];
        this.questData = new QuestData(this.game);
        this.npcQuestMarkState = [];
    }
    getInprocessQuests(){
        return this.inprocessQuests;
    }
    getInprocessQuest(id){
        let quest = this.inprocessQuests.filter(quest => (quest.id === id));
        return quest[0];
    }
    backup(quest){
        quest.inprocess.forEach(storageData => {
            let qData = this.questData.questList[storageData.id];
            this.inprocessQuests.push(new Quest(this.game, storageData, qData));
        })
        this.finishedQuests = quest.finished;
        this.questData.setQuestState(quest.inprocess, quest.finished);
        this.newQuestCheck();
        this.UIUpdate();

    }
    UIUpdate(){
        let msg = new Message(this, this.game.UI.UIInteraction.questInteraction, "questUpdate", {});
        this.game.messageQueue.add(msg);
        
    }
    save(quest){
        quest.inprocess = [];
        this.inprocessQuests.forEach(inprocessQuest => {
            quest.inprocess.push(inprocessQuest.getStorageData());
        });
        quest.finished = this.finishedQuests;
    }
    newQuestCheck(){
        let newQuestDataList = this.questData.getNewQuest();
        newQuestDataList.forEach(data => {
            this.inprocessQuests.push(new Quest(this.game, false, data));
            this.questData.setInprocessQuest(data.id);
        })
    }
    questCheck(type, id){
        this.inprocessQuests.some((quest, i) => {
            quest.check(type, id);
            if(!quest.activation){
                this.finishedQuests.push(quest.id); // 완료 퀘스트 목록에 추가
                this.questData.setFinishedQuest(quest.id); // 퀘스트 데이터에 isCleared 체크
                this.inprocessQuests.splice(i, 1);  // 진행중 퀘스트 목록에서 삭제
                this.newQuestCheck(); // 다음 퀘스트 검색
                return true;
            }
            return false;
        })
    }
    update(){
        
    }
    onMessage(msg){
        if(msg.type === "questCheck"){
            this.questCheck(msg.data.type, msg.data.id);
        }
        else if(msg.type === "newQuestCheck"){
            this.newQuestCheck();
        }
    }
}

