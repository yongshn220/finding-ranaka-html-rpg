class MonsterData{
    monsters;
    constructor(){
        this.setting();
    }
    setting(){
        this.monsters = {
            1 : {
                id: 1,
                name: "쓰레기통",
                LV: 5, 
                sw: 80,
                sh: 80,
                HP: 800,
                AD: 400,
                DEF: 100,
                EXP: 10,
                frame: 5,
                img: this.getImg(1),
                infoImg : this.getInfoImg(1),
                isAttack: false,
                moveSpeed: 2,
                stands: false,
                drops: {
                    gold: {data: 15, percent: 50},
                    items: [{data: 2001, percent: 20},{data: 2002, percent: 20}]
                }
            },
            2: {
                id: 2,
                name: "깡통",
                LV: 5, 
                sw: 100,
                sh: 100,
                HP: 1000,
                AD: 500,
                DEF: 110,
                EXP: 15,
                frame: 5,
                img: this.getImg(2),
                infoImg : this.getInfoImg(2),
                isAttack: false,
                moveSpeed: 2,
                stands: false,
                drops: {
                    gold: {data: 15, percent: 50},
                    items: [{data: 3001, percent: 20},{data: 2001, percent: 20},{data: 2002, percent: 20}]
                }
            },
            3: {
                id: 3,
                name: "c등급표적",
                LV: 8, 
                sw: 80,
                sh: 80,
                HP: 3000,
                AD: 800,
                DEF: 200,
                EXP: 23,
                frame: 4,
                img: this.getImg(3),
                infoImg : this.getInfoImg(3),
                isAttack: false,
                moveSpeed: 2,
                stands: false,
                drops: {
                    gold: {data: 20, percent: 50},
                    items: [{data: 2001, percent: 20},{data: 2002, percent: 20}]
                }
            },
            4: {
                id: 4,
                name: "b등급표적",
                LV: 8, 
                sw: 80,
                sh: 80,
                HP: 3500,
                AD: 1000,
                DEF: 300,
                EXP: 25,
                frame: 4,
                img: this.getImg(4),
                infoImg : this.getInfoImg(4),
                isAttack: false,
                moveSpeed: 2,
                stands: false,
                drops: {
                    gold: {data: 20, percent: 50},
                    items: [{data: 2001, percent: 20},{data: 2002, percent: 20}]
                }
            },
            5: {
                id: 5,
                name: "마법빗자루",
                LV: 13, 
                sw: 80,
                sh: 120,
                HP: 400,//
                AD: 1200,
                DEF: 35,//
                EXP: 4500,
                frame: 6, 
                img: this.getImg(5),
                infoImg : this.getInfoImg(5),
                isAttack: false,
                moveSpeed: 3,
                stands: false,
                drops: {
                    gold: {data: 30, percent: 50},
                    items: [{data: 3002, percent: 5},{data: 2001, percent: 20},{data: 2002, percent: 20}]
                }
            },
            6: {
                id: 6,
                name: "마법빗자루",
                LV: 15, 
                sw: 80,
                sh: 120,
                HP: 6000,
                AD: 1700,
                DEF: 350,
                EXP: 130,
                frame: 6,
                img: this.getImg(6),
                infoImg : this.getInfoImg(6),
                isAttack: false,
                moveSpeed: 3,
                stands: false,
                drops: {
                    gold: {data: 100, percent: 50},
                    items: [{data: 2001, percent: 20},{data: 2002, percent: 20}]
                }
            },
            7: {
                id: 7,
                name: "a등급표적",
                LV: 15, 
                sw: 80,
                sh: 80,
                HP: 80000,
                AD: 2000,
                DEF: 500,
                EXP: 2000,
                frame: 4,
                img: this.getImg(7),
                infoImg : this.getInfoImg(7),
                isAttack: false,
                moveSpeed: 4,
                stands: true,
                drops: {
                    gold: {data: 2000, percent: 100},
                    items: [{data: 2001, percent: 20},{data: 2002, percent: 20}]
                }
            },
            8: {
                id: 8,
                name: "나무",
                LV: 15, 
                sw: 100,
                sh: 120,
                HP: 80000,
                AD: 2000,
                DEF: 500,
                EXP: 2000,
                frame: 4,
                img: this.getImg(8),
                infoImg : this.getInfoImg(8),
                isAttack: false,
                moveSpeed: 0.5,
                stands: false,
                drops: {
                    gold: {data: 100, percent: 50},
                    items: []
                }
            },
            9: {
                id: 9,
                name: "버섯",
                LV: 15, 
                sw: 80,
                sh: 80,
                HP: 80000,
                AD: 2000,
                DEF: 500,
                EXP: 2000,
                frame: 4,
                img: this.getImg(9),
                infoImg : this.getInfoImg(9),
                isAttack: false,
                moveSpeed: 2,
                stands: false,
                drops: {
                    gold: {data: 100, percent: 50},
                    items: []
                }
            },
            10: {
                id: 9,
                name: "씨앗",
                LV: 15, 
                sw: 100,
                sh: 100,
                HP: 80000,
                AD: 2000,
                DEF: 500,
                EXP: 2000,
                frame: 4,
                img: this.getImg(10),
                infoImg : this.getInfoImg(10),
                isAttack: false,
                moveSpeed: 0,
                stands: true,
                drops: {
                    gold: {data: 100, percent: 50},
                    items: []
                },
                touchEffect :{
                    moveSpeed : 4,
                    types : ["moveSpeed"],
                }
            },
        }
    }

    getMonster(code){
        return this.monsters[code];
    }

    getImg(num){
        let img = new Image();
        img.src = `./images/monsters/monster${num}.png`;
        return img;
    }
    getInfoImg(num){
        let img = new Image();
        img.src = `./images/monsters/info/monster${num}.png`;
        return img;
    }
}