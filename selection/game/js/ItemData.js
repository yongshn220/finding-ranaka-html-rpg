class ItemData{
    items
    constructor(game){
        this.game = game;
        this.items = {};
        this.setting();
    }

    getItem(code){
        let type = this.getType(code);
        return this.items[type][code];
    }

    getImage(code){
        let type = this.getType(code);
        let img = new Image();
        img.src = `./images/items/${type}/${code}.png`
        return img;
    }

    getType(code){
        let type = Math.floor(code / 1000);
        if(type === 1){
            return "equipment";
        }
        else if(type === 2){
            return "consumption";
        }
        else if(type === 3){
            return "general";
        }
    }

    setting(){
        this.items = {
            equipment: {
                1101: {
                    code: 1101,
                    type: "weapon",
                    name: "견습 검사의 검",
                    info: "스콜라스 학교에서 주로 새내기들이 사용했던 검이다.",
                    wearable: true,
                    useable: false,
                    sell: 100,
                    option: {
                        type: "무기",
                        LV: 1,
                        AD: 100,
                        CP: 5,
                        contains: ["type", "LV", "AD", "CP"],
                    },
                    img: this.getImage(1101),
                },
                1102: {
                    code: 1102,
                    type: "weapon",
                    name: "검",
                    info: "검이다.",
                    wearable: true,
                    useable: false,
                    sell: 100,
                    option: {
                        type: "무기",
                        LV: 1,
                        AD: 100,
                        CP: 5,
                        contains: ["type", "LV", "AD", "CP"],
                    },
                    img: this.getImage(1102),
                },
                1201: {
                    code: 1201,
                    type: "hat",
                    name: "새내기의 전투모",
                    info: "스콜라스 학교에서 새내기들이 훈련 시 쓰던 머리띠이다.",
                    wearable: true,
                    useable: false,
                    sell: 100,
                    option: {
                        type: "모자",
                        LV: 1,
                        DEF: 30,
                        HP: 500,
                        contains: ["type", "LV", "DEF", "HP"],
                    },
                    img: this.getImage(1201),
                },
                1202: {
                    code: 1202,
                    type: "hat",
                    name: "전투모",
                    info: "머리띠이다.",
                    wearable: true,
                    useable: false,
                    sell: 100,
                    option: {
                        type: "모자",
                        LV: 1,
                        DEF: 30,
                        HP: 500,
                        contains: ["type", "LV", "DEF", "HP"],
                    },
                    img: this.getImage(1202),
                },
                1301: {
                    code: 1301,
                    type: "cloth",
                    name: "새내기의 전투복",
                    info: "스콜라스 학교에서 새내기들이 훈련 시에 착용하는 옷이다.",
                    wearable: true,
                    useable: false,
                    sell: 100, 
                    option: {
                        type: "옷",
                        LV: 1,
                        DEF: 50,
                        HP: 500,
                        contains: ["type", "LV", "DEF", "HP"],
                    },
                    img: this.getImage(1301),
                },
                1302: {
                    code: 1302,
                    type: "cloth",
                    name: "전투복",
                    info: "옷이다.",
                    wearable: true,
                    useable: false,
                    sell: 100, 
                    option: {
                        type: "옷",
                        LV: 1,
                        DEF: 50,
                        HP: 500,
                        contains: ["type", "LV", "DEF", "HP"],
                    },
                    img: this.getImage(1302),
                },
                1401: {
                    code: 1401,
                    type: "gloves",
                    name: "새내기의 전투장갑",
                    info: "스콜라스 학교에서 새내기들이 훈련 시에 끼는 장갑이다.",
                    wearable: true,
                    useable: false,
                    sell: 100,
                    option: {
                        type: "장갑",
                        LV: 1,
                        AD: 50,
                        CP: 3,
                        contains: ["type", "LV", "AD", "CP"],
                    },
                    img: this.getImage(1401),
                },
                1402: {
                    code: 1402,
                    type: "gloves",
                    name: "전투장갑",
                    info: "장갑이다.",
                    wearable: true,
                    useable: false,
                    sell: 100,
                    option: {
                        type: "장갑",
                        LV: 1,
                        AD: 50,
                        CP: 3,
                        contains: ["type", "LV", "AD", "CP"],
                    },
                    img: this.getImage(1402),
                },
                1501: {
                    code: 1501,
                    type: "shoes",
                    name: "새내기의 전투화",
                    info: "스콜라스 학교에서 새내기들이 훈련 시에 시는 신발이다.",
                    wearable: true,
                    useable: false,
                    sell: 100,
                    option: {
                        type: "신발",
                        LV: 1,
                        AD: 10,
                        contains: ["type", "LV", "AD"],
                    },
                    img: this.getImage(1501),
                },
                1502: {
                    code: 1502,
                    type: "shoes",
                    name: "전투화",
                    info: "신발이다.",
                    wearable: true,
                    useable: false,
                    sell: 100,
                    option: {
                        type: "신발",
                        LV: 1,
                        AD: 10,
                        contains: ["type", "LV", "AD"],
                    },
                    img: this.getImage(1502),
                },
                1601: {
                    code: 1601,
                    type: "belt",
                    name: "새내기의 허리띠",
                    info: "스콜라스 학교 새내기들이 항상 차고 다니는 허리띠이다.",
                    wearable: true,
                    useable: false,
                    sell: 100,
                    option: {
                        type: "신발",
                        LV: 1,
                        HP: 300,
                        MP: 300,
                        contains: ["type", "LV", "HP", "MP"],
                    },
                    img: this.getImage(1601),
                },
                1602: {
                    code: 1602,
                    type: "belt",
                    name: "허리띠",
                    info: "허리띠이다.",
                    wearable: true,
                    useable: false,
                    sell: 100,
                    option: {
                        type: "신발",
                        LV: 1,
                        HP: 300,
                        MP: 300,
                        contains: ["type", "LV", "HP", "MP"],
                    },
                    img: this.getImage(1602),
                },
            },
            consumption: {
                2001: {
                    code: 2001,
                    type: "consumption",
                    name: "체력물약",
                    info: "체력물약이다.",
                    wearable: false,
                    useable: true,
                    sell: 1,
                    option: {
                        type: "소모품",
                        HP: 200,
                        MP: 0, 
                        contains: ["type", "HP"],
                    },
                    img: this.getImage(2001),
                },
                2002: {
                    code: 2002,
                    type: "consumption",
                    name: "마나물약",
                    info: "마나물약이다.",
                    wearable: false,
                    useable: true,
                    sell: 1,
                    option: {
                        type: "소모품",
                        HP: 0,
                        MP: 200, 
                        contains: ["type", "MP"],
                    },
                    img: this.getImage(2002),
                },
            },
            general: {
                3001: {
                    code: 3001,
                    type: "quest",
                    name: "빈 캔",
                    info: "비어있는 캔이다.",
                    wearable: false,
                    useable: false,
                    sell: 0,
                    option : false,
                    img: this.getImage(3001),
                },
                3002: {
                    code: 3002,
                    type: "quest",
                    name: "빗자루",
                    info: "평범한 빗자루이다.",
                    wearable: false,
                    useable: false,
                    sell: 0,
                    option : false,
                    img: this.getImage(3002),
                },
            },
        }
    }
}