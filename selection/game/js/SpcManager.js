class SpcManager{
    constructor(game){
        this.game = game;
    }

    specialization(id){
        if(id === 1){
            this.firstSpc();
        }
        else if(id === 2){
            this.secondSpc();
        }
        else if(id === 3){
            this.thirdSpc();
        }
        else{
            return;
        }
    }
    firstSpc(){
        let info = this.game.player.information
        this.spcNames = [];
        if(info.specialize !== 0){
            return;
        }
        if(info.job === 1){
            this.spcNames = ["달무사", "듀얼소드마스터"];
        }
        else if(info.job === 2){

        }
        else if(info.job === 3){

        }
        let msg = new Message(this, this.game.UI.UIInteraction.spcInteraction, "specialization", this.spcNames);
        this.game.messageQueue.add(msg);
    }
    //1차전직
    firstSpcSubmit(id){
        let spc;
        if(id === 0){
            spc = 10;

        }
        else{
            spc = 20;
        }
        this.game.player.information.code = this.game.player.information.job * 100 + spc;
        this.game.player.information.specialize = spc;
        this.game.player.character.charImageSetting();
        this.game.player.skillManager.skillListUpdate();
        backup.save();
        let msg = new Message(this, this.game.questManager, "questCheck", {type: "specialization", id: 1});
        this.game.messageQueue.add(msg);
        msg = new Message(this, this.game.textManager, "newInfo", {type: "전직성공:", value: this.spcNames[id]});
        this.game.messageQueue.add(msg);
    }
    onMessage(msg){
        if(msg.type === "specialization"){
            this.specialization(msg.data);
        }
        else if(msg.type === "spcSubmit"){
            this.firstSpcSubmit(msg.data);
        }
    }
}