class TextManager{
    constructor(game){
        this.game = game;
        this.infoList = [];
        this.questInfo = {};
        this.timer = 0;
        this.maxTime = 10000;
    }

    newInfo(type, value){
        this.timer = 0;
        let newInfoList = [{type:type, value:value}];
        this.infoList.some((info, i) => {
            newInfoList.push(info);
            return (i >= 3);
        })
        this.infoList = newInfoList;
        this.UIUpdate();
    }
    newQuestInfo(data){
        this.timer = 0;
        this.questInfo = data;
        this.questUIUpdate();
    }
    questUIUpdate(){
        let msg = new Message(this, this.game.UI.UIInteraction.questInteraction, "questInfoUpdate", this.questInfo);
        this.game.messageQueue.add(msg);
    }
    levelUpdate(){
        if(this.infoList.length > 0 || this.questInfo != ""){
            this.timer += 1000;
            this.timerCheck();
        }
    }
    timerCheck(){
        if(this.timer >= this.maxTime){
            this.timer = 0;
            this.infoList = [];
            this.questInfo = "";
            this.UIUpdate();
            this.questUIUpdate();
        }
    }
    UIUpdate(){
        let msg = new Message(this, this.game.UI.UIInteraction.textInteraction, "UIUpdate", this.infoList);
        this.game.messageQueue.add(msg);
    }
    onMessage(msg){
        if(msg.type === "newInfo"){
            this.newInfo(msg.data.type, msg.data.value);
        }
        else if(msg.type === "newQuestInfo"){
            this.newQuestInfo(msg.data);
        }
    }
}