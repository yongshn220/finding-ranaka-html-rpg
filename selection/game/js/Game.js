class Game{
    UI;
    mapManager;
    monsterManager;
    itemManager;
    statManager;
    player;
    entities;
    messageQueue;
    timeManager;
    constructor(){
        this.entities = [];
        this.messageQueue = new MessageQueue();
        this.mapManager = new MapManager(this);
        this.monsterManager = new MonsterManager(this);
        this.itemManager = new ItemManager(this);
        this.statManager = new StatManager(this);
        this.stateManager = new StateManager(this);
        this.questManager = new QuestManager(this);
        this.talkManager = new TalkManager(this);
        this.player = new Player(this);
        this.damageManager = new DamageManager(this);
        this.textManager = new TextManager(this);
        this.spcManager = new SpcManager(this);
        this.UI = new UI(this);
        this.timeManager = new TimeManager(this);
        
    }

    //update
    update(now){
        this.timeManager.levelUpdate(now);
    }

    levelUpdate(){
        this.gravity();
    }
    //character
    characterMove(addX, addY){
        let tempChar = new Entity(this.player.character);
        tempChar.gx += addX;
        tempChar.gy += addY;
        if(this.isCharInBckgrnd(tempChar) && !this.isStructureCollid(tempChar)){
            let msg = new Message(this, this.player.character, "move", {x: addX, y: addY});
            this.player.character.onMessage(msg);
            return true;
        }
    }

    isCharInBckgrnd(tempChar){
        let gx = tempChar.gx + tempChar.RX;
        let gy = tempChar.gy + tempChar.sh;
        let sw = tempChar.RW;
        let sh = tempChar.RH;
        return this.mapManager.curMap.mainBackground.isEntityInBackground(gx,gy,sw,sh)
    }

    isStructureCollid(tempChar){
        if(this.stateManager.getState("character", "isJumping")){
            return false;
        }
        let gx = tempChar.gx + tempChar.RX;
        let gy = tempChar.gy + tempChar.RY;
        let sw = tempChar.RW;
        let sh = tempChar.RH;
        return this.mapManager.curMap.isLineCollid(gx,gy+sh,sw,"wall");
    }

    backgroundCenterForceAdjust(){
        let center = this.backgroundCenterAdjust();
        while(!center.x || !center.y){
            center = this.backgroundCenterAdjust();
            this.player.character.positionAdjust();
        }
    }

    //canvas
    backgroundCenterAdjust(){
        let msg;
        let maxX = false;
        let maxY = false;
        //x position
        let diffX = this.isCharacterInCenterX();
        if(diffX !== 0){
            if(diffX > 0){
                if(this.isCanvasMoveValidX(4)){
                    msg = new Message(this, this.mapManager.curMap.mainBackground, "move", {x: 4, y: 0});
                    this.mapManager.curMap.mainBackground.onMessage(msg);
                }
                else{
                    maxX = true;
                }
            }
            else{
                if(this.isCanvasMoveValidX(-4)){
                    msg = new Message(this, this.mapManager.curMap.mainBackground, "move", {x: -4, y: 0});
                    this.mapManager.curMap.mainBackground.onMessage(msg);
                } 
                else{
                    maxX = true;
                }
            }
        }
        else{
            maxX = true;
        }


        // y position
        let diffY = this.isCharacterInCenterY();
        if(diffY !== 0 && !this.stateManager.getState("character", "isJumping")){
            if(diffY > 0){
                if(this.isCanvasMoveValidY(4)){
                    msg = new Message(this, this.mapManager.curMap.mainBackground, "move", {x: 0, y: 4});
                    this.mapManager.curMap.mainBackground.onMessage(msg);
                }
                else{
                    maxY = true;
                }
            }
            else{
                if(this.isCanvasMoveValidY(-4)){
                    msg = new Message(this, this.mapManager.curMap.mainBackground, "move", {x: 0, y: -4});
                    this.mapManager.curMap.mainBackground.onMessage(msg);
                }
                else{
                    maxY = true;
                }
            }
        }
        else{
            maxY = true;
        }

        return {x: maxX, y: maxY};
    }

    isCharacterInCenterX(){
        let diffX = this.player.character.dx + this.player.character.RX - 760;
        if(diffX < 5 && diffX > -5){
            return 0;
        }
        return diffX;
    }
    isCharacterInCenterY(){
        let diffY = this.player.character.dy + this.player.character.RY - 600;
        if(diffY < 5 && diffY > -5){
            return 0;
        }
        return diffY;
    }


    isCanvasMoveValidX(addX){
        let sx = this.mapManager.curMap.mainBackground.sx + addX;
        let bw = this.mapManager.curMap.mainBackground.width;
        let sw = screen_width;

        if(sx >= 0 && sx < bw - sw){
            return true;
        }
        return false;
    }
    isCanvasMoveValidY(addY){
        let sy = this.mapManager.curMap.mainBackground.sy + addY;
        let bh = this.mapManager.curMap.mainBackground.height;;
        let sh = screen_height;
        if(sy >= 0 && sy < bh - sh){
            return true;
        }
        return false;
    }

    

    //entities
    getEntities(){
        return this.entities;
    }
    
    addEntity(entity){
        this.entities.push(entity);
    }

    entityFilterling(){
        this.entities = this.entities.filter(entity => entity.gravityActivation);
    }

    getMessageQueue(){
        return this.messageQueue;
    }
    
    onMessage(msg){
        if(msg.type === "forceCenter"){
            this.backgroundCenterForceAdjust();
        }
    }

    //gravity effects 
    getYDir(num){
        if(num < 0){
            return 1;
        }
        else{
            return -1;
        }
    }

    getStructureValid(entity){
        if(entity instanceof Character){
            return this.stateManager.getState("character","checkStructure");
        }
        else {
            return true;
        }
    }
    setStructureValid(entity){
        if(entity instanceof Character){
            return this.stateManager.setState("character","checkStructure", true);
        }
    }
    gravityExceptionCheck(entity){
        if(entity instanceof Character){
            return this.stateManager.getState("character", "isLadderOn");
        }
        return false
    }
    adjustGravityMove(entity){
        if(this.gravityExceptionCheck(entity)){
            return false;
        }
        let yDir = this.getYDir(entity.velocity.y)
        let maxMoveY = Math.sqrt((entity.velocity.y**2));
        let curMoveY = 0;
        let gx = entity.gx + entity.RX;
        let gy = entity.gy + entity.RY;
        let sw = entity.RW;
        let sh = entity.RH;
           
        for(let i = 0; i < maxMoveY; i++){
            gy += (1 * yDir);
            let cs = this.getStructureValid(entity);
            let validation = !this.mapManager.curMap.isLineCollid(gx,gy + sh,sw, "wall");
            if(validation && !cs && yDir > 0){
                curMoveY++;
                this.setStructureValid(entity);
            }
            else if(validation || !cs){
                curMoveY++;
            }
            else{
                if(curMoveY === 0){
                    return -987654;
                }
                break;
            }
        }
        return curMoveY * yDir;     
    }
    
   
    gravity(){
        let entity;
        let val;
        for(let i = 0; i < this.entities.length; i++){
            entity = this.entities[i];
            entity.velocity.y = entity.velocity.y - 1;
            val = this.adjustGravityMove(entity);
            if(!val){
                entity.velocity.y = 0;
                return;
            }
            if(val !== -987654){
                let msg = new Message(this, entity, "move", {x:0, y: val});
                this.messageQueue.add(msg);
            }
            else{
                entity.velocity.y = 0;
                if(entity instanceof Character){
                    if(this.stateManager.getState("character", "isJumping") === true){
                        let msg = new Message(this, entity, "jumpEnd", {});
                        this.messageQueue.add(msg);
                    }
                }  
                else if(entity instanceof Item){
                    let msg = new Message(this, entity, "gravityDeactivate", {});
                    this.messageQueue.add(msg);
                }
            }   
        }
    }
}