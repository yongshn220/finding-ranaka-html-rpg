class Structure extends Entity{
    constructor(game, mapData){
        super();
        this.game = game;
        this.mapData = mapData;
        this.isDrawable = false;
        this.setting();
    }

    setting(){
        this.id = this.mapData.id;
        this.name = this.mapData.name;
        this.type = this.mapData.type;
        if(this.type === "store"){
            this.subType = this.mapData.subType;
            this.items = this.mapData.items;
        } 
        this.sw = this.mapData.sw;
        this.sh = this.mapData.sh;
        this.dw = this.mapData.sw;
        this.dh = this.mapData.sh;
        this.gx = this.mapData.gx;
        this.gy = this.mapData.gy;
        
        if(this.mapData.portalData){
            this.portalData = this.mapData.portalData;
        }
        if(this.mapData.img){
            this.isDrawable = true;
            this.img = this.mapData.img;
            this.questState = -1;
            this.newQuestImg = new Image();
            this.newQuestImg.src = './images/UI/quest/newQuestMark.png'
            this.finishedQuestImg = new Image();
            this.finishedQuestImg.src = './images/UI/quest/finishedQuestMark.png'
            this.questImg = [this.newQuestImg, this.finishedQuestImg];
        }
    }
    isCollid(gx,gy,sw,sh,type){
        if(this.type === type){
            if((gx <= (this.gx + this.sw) && (gx + sw) >= this.gx) && gy <= (this.gy + this.sh) && (gy + sh) >= this.gy){ 
                return true;
            }
        }
        return false;
    }
    isPointCollid(gx,gy,type){
        if(this.type === type){
            if( gx >= this.gx && gx <= (this.gx + this.sw) && gy >= this.gy && gy <= (this.gy + this.sh)){ 
                return true;
            }
        }
        return false;
    }
    isLineCollid(gx,gy,sw,type){
        if(this.type === type){
            if((gx <= (this.gx + this.sw) && (gx + sw) >= this.gx) &&  gy >= this.gy && gy <= (this.gy + this.sh)){ 
                return true;
            }
        }
        return false;
    }
    isUnderEntity(){
        
    }
    getIn(){
        let msg = new Message(this, this.game.mapManager, "mapChange", this.portalData)
        this.game.messageQueue.add(msg);
        
    }
    getTalk(){
        if(this.type === "npc"){
            let msg = new Message(this, this.game.talkManager, "talkEvent", {id: this.id, name: this.name, type: this.type});
            this.game.messageQueue.add(msg);
            msg = new Message(this, this.game.questManager, "questCheck", {type: "talk", id: this.id});
            this.game.messageQueue.add(msg);
        }
        else if(this.type === "store"){
            if(this.game.stateManager.getState("store", "isOpen") === false){
                this.game.stateManager.setState("store", "isOpen", true);
                let msg = new Message(this, this.game.UI.UIInteraction.storeInteraction, "storeIn", this.items);
                this.game.messageQueue.add(msg);
            }
            else{
                this.game.stateManager.setState("store", "isOpen", false);
                let msg = new Message(this, this.game.UI.UIInteraction.storeInteraction, "storeOut", this.items);
                this.game.messageQueue.add(msg); 
            }
        }
    }

    update(){
        this.draw();
    }

    positionAdjust(){
        let background = this.game.mapManager.curMap.mainBackground;
        this.dx = this.gx - background.sx;
        this.dy = this.gy - background.sy;
    }

    draw(){
        if(this.isDrawable){
            this.positionAdjust();
            canvas.context.drawImage(this.img,this.sx,this.sy,this.sw,this.sh,this.dx,this.dy,this.dw,this.dh);
        }
        this.questState = this.game.questManager.npcQuestMarkState[this.id];
        if(this.questState >= 0){
            let dx = this.dx + (this.dw/2 - 20);
            let dy = this.dy - 10;
            canvas.context.drawImage(this.questImg[this.questState],0,0,40,60,dx,dy,40,60);
        }
    }
}

