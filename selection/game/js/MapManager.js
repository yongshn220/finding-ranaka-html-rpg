class MapManager{
    game;
    curMap;
    mapCode;
    connection;
    mapData;
    constructor(game){
        this.game = game;
        this.mapData = new MapData(this.game);
        this.connection = [];
    }

    backup(position, mapData){
        this.mapCode = position.mapCode;
        this.mapAccess = mapData.mapAccess;
        this.newMapSetting(this.mapCode);
        this.monsterEvent();
    }

    save(position, mapData){
        position.mapCode = this.mapCode
        mapData.mapAccess = this.mapAccess;
    }
    
    newAccess(id){
        this.mapAccess[id] = true;
        let msg = new Message(this, this.game.textManager, "newQuestInfo", "다음 챕터로 가는 포탈이 열렸습니다.");
        this.game.messageQueue.add(msg);
        msg = new Message(this, this.game.textManager, "newInfo", {type: "포탈", value:"접근해제"});
        this.game.messageQueue.add(msg);
    }

    newMapSetting(code){
        this.curMap = new Map(this.game, code, this.mapData);
        let name = this.mapData.infoList[code].name;
        let msg = new Message(this, this.game.UI.UIInteraction.miniMapInteraction, "update", {name: name, id:code});
        this.game.messageQueue.add(msg);
    }

    mapChange(data){
        if(!data.access){
            if(!this.mapAccess[data.to]){
                let msg = new Message(this, this.game.textManager, "newInfo", {type: "진입불가", value:"퀘스트 미완료"});
                this.game.messageQueue.add(msg);
                return;
            }
        }
        this.mapCode = data.to;
        this.newMapSetting(data.to);
        let msg;
        msg = new Message(this, this.game.player.character, "rePosition", data.pos);
        this.game.getMessageQueue().add(msg);
        msg = new Message(this, this.game, "forceCenter", "");
        this.game.getMessageQueue().add(msg);
        this.monsterEvent();
        this.itemEvent();
    }

    monsterEvent(){
        let info = this.mapData.infoList[this.mapCode];
        let msg = new Message(this, this.game.monsterManager, "newMonsters", info.monster);
        this.game.messageQueue.add(msg);
    }

    itemEvent(){
        let msg = new Message(this, this.game.itemManager, "reset", {});
        this.game.messageQueue.add(msg);
    }

    onMessage(msg){
        if(msg.type === "mapChange"){
            this.mapChange(msg.data);
        }
        else if(msg.type === "newAccess"){
            this.newAccess(msg.data);
        }
        else{
        
        }
    }

}