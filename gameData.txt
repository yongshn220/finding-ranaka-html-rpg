game : {
    currentData : {
        account : 0,
        character : 0
    }

    accounts : [
        
        {
            information : {
                name : "용정",
                pw : 123456
            },
            characters [
                {
                    information : {
                        job : false,
                        specialize : 0,
                        level : 1,
                        stat : {
                            curHP : 10,
                            curMP : 10,
                            maxHP : 10,
                            maxMP : 10,
                            AD : 10,
                            defense: 10,
                            CP: 10,
                            avoidance : 10, 
                            moveSpeed : 1.5,
                            attackSpeed : 10,
                            stands : 10
                        },
                        position : {
                            chapter : 1,
                            map : 0,
                            x : 800,
                            y : 500
                        },
                        key : {
                            skill : {
                                q : 100,
                                w : 100,
                                e : 100,
                                r : 100,
                                a : 100,
                                s : 100,
                                d : 100,
                                f : 100,
                            },
                            item : {
                                delete: 100,
                                end: 100,
                            },
                        }
                    },
                    myItem : {
                        weapon : 0,
                        cloths : 0,
                        shoes : 0,
                        hat : 0,
                        gloves : 0,
                        belt : 0,
                    },
                    stone : [
                        0,0,0,0,0
                    ],
                    inventory : {
                        total : []
                    },
                    quest : {        
                    },
                    npc : {
                        chapter1 : {
                            npc#1 : {
                                position : {
                                    map : 
                                    x :
                                    y :
                                }
                                state : 0     //기본 상태 or 퀘스트 진행중인 상태
                            }
                        },
                        chapter2 : {

                        },
                        chapter3 : {

                        },
                        chapter4 : {

                        },
                        chapter5 : {

                        }
                    }
                },

                {
                    //char3
                }
            ]
        }
    ]

    mapData : {
        chapter1 : {
            map0 : {
                width : 1600,
                height : 800,
                structure : {

                }
            }
        }
    }
}