
let storage = new Storage();

$(document).ready(function(){
    
    showAccountInfo();
})

function accountSelected(value){

    storage.selectedAccount(value);
    startOn();
    deleteOn();
}

function startOn(){
    $("#start-button").addClass("on");
}
function deleteOn(){
    $("#delete-button").addClass("on");
}
function start(){
    storage.saveGameData();
}
function newAccount(){
    let newName = prompt("enter new account name");
    let newPassword = prompt("enter new password");
    storage.newAccount(newName, newPassword);
    showAccountInfo();
}
function deleteAccount(){
    storage.deleteAccount();
    window.location.reload();
}

function showAccountInfo(){
    let ctx;
    let gameData = storage.getGameData();
    for(let i = 0; i < gameData.accounts.length; i++){
        let accName = gameData.accounts[i].information.name;
        ctx = document.getElementById("account-button" + (i + 1));
        ctx.innerHTML = accName;
        $("#account-button" + (i + 1)).addClass("on");
    }
}

