class Storage{
    gameData;
    constructor(){
        //this.removeGameDataInStorage();
        this.setting();
        this.printGameData();
    }

    getGameData(){
        return this.gameData;
    }

    setting(){
        this.gameData = JSON.parse(localStorage.getItem("game"));
        if(this.gameData === null){
            this.gameData = {
                accounts : [
                ]
            };
            console.log("new Game data stored");
        }
        this.gameData.currentData = {};
    }

    selectedAccount(num){
        this.gameData.currentData.account = num;
        this.printGameData();
    }
    checkAccountValid(selAcc){
        if(localStorage.getItem(selAcc) === null){
            return false;
        }
        return true;
    }
    
    newAccount(name, pw){
        if(!this.isNameValid(name) || !this.isPasswordValid(pw)){
            alert("please enter name and pw again.");
            return;
        }
        if(this.isAccountFull()){
            this.noEmptyAccountEvent();
            return;
        }
        let account = 
        {
            information : {
                name : name,
                password : pw
            },

            characters: [
            ]
        }
        this.insetAccount(account);
        this.saveGameData();
        alert("newAccount created");
    }
    isNameValid(name){
        if(name.length > 0 && name.length < 15){
            return true;
        }
        return false;
    }
    isPasswordValid(pw){
        if(pw.length > 0 && pw.length < 20){
            return true;
        }
        return false;
    }

    deleteAccount(){
        let selectedAccountNumber = this.gameData.currentData.account;
        let enteredPassword = prompt("Enter password");
        if(enteredPassword === this.gameData.accounts[selectedAccountNumber].information.password){
            this.deleteAccountEvent(selectedAccountNumber);
            alert("account deleted")
        }
        else{
            alert("wrong password");
        }
    }
    deleteAccountEvent(num){
        this.gameData.accounts.splice(num, 1);
        this.saveGameData();
    }

    insetAccount(account){
        this.gameData.accounts.push(account);
    }
    isAccountFull(){
        if(this.gameData.accounts.length >= 4){
            return true;
        }
        return false;
    }

    noEmptyAccountEvent(){
        alert("no space for new Account");
    }

    saveGameData(){
        localStorage.setItem("game", JSON.stringify(this.gameData));
    }

    printGameData(){
        console.log(this.gameData);
    }

    removeGameDataInStorage(){
        localStorage.removeItem("game");
    }
}
